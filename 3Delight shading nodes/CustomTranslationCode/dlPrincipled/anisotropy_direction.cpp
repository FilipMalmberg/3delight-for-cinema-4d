	{
		v = data->GetVector(PARAM_ANISOTROPY_DIRECTION); //Don't linearize
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("anisotropy_direction", &v_data[0]));
	}
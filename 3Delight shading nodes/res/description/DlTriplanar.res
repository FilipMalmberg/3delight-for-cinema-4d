CONTAINER dlTriplanar
{
	NAME dlTriplanar;

	INCLUDE GVbase;
	GROUP ID_GVPROPERTIES {
	DEFAULT 1;
		GROUP GROUP_TEXTURES {
		DEFAULT 1;
			LONG PARAM_MODE{ CYCLE{PARAM_MODE_TRIPLANAR; PARAM_MODE_PLANAR; }; EDITPORT; INPORT;}
			FILENAME PARAM_COLOR_TEXTURE{EDITPORT; INPORT; }
			LONG PARAM_COLOR_TEXTURE_META_COLORSPACE{ CYCLE{PARAM_COLOR_TEXTURE_META_COLORSPACE_AUTO; PARAM_COLOR_TEXTURE_META_COLORSPACE_SRGB; PARAM_COLOR_TEXTURE_META_COLORSPACE_REC__709; PARAM_COLOR_TEXTURE_META_COLORSPACE_LINEAR; }; EDITPORT; INPORT;}
			FILENAME PARAM_FLOAT_TEXTURE{EDITPORT; INPORT; }
			LONG PARAM_FLOAT_TEXTURE_META_COLORSPACE{ CYCLE{PARAM_FLOAT_TEXTURE_META_COLORSPACE_AUTO; PARAM_FLOAT_TEXTURE_META_COLORSPACE_SRGB; PARAM_FLOAT_TEXTURE_META_COLORSPACE_REC__709; PARAM_FLOAT_TEXTURE_META_COLORSPACE_LINEAR; }; EDITPORT; INPORT;}
			FILENAME PARAM_HEIGHT_TEXTURE{EDITPORT; INPORT; }
			LONG PARAM_HEIGHT_TEXTURE_META_COLORSPACE{ CYCLE{PARAM_HEIGHT_TEXTURE_META_COLORSPACE_AUTO; PARAM_HEIGHT_TEXTURE_META_COLORSPACE_SRGB; PARAM_HEIGHT_TEXTURE_META_COLORSPACE_REC__709; PARAM_HEIGHT_TEXTURE_META_COLORSPACE_LINEAR; }; EDITPORT; INPORT;}
		}
		GROUP GROUP_TRANSFORMATION {
		DEFAULT 1;
			REAL PARAM_SCALE{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
			LONG PARAM_SPACE{ CYCLE{PARAM_SPACE_WORLD; PARAM_SPACE_OBJECT; }; EDITPORT; INPORT;}
		}
		GROUP GROUP_BLENDING {
		DEFAULT 1;
			REAL PARAM_BLEND_SOFTNESS{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; MAX 1; STEP 0.01; }
			REAL PARAM_BLEND_NOISE_INTENSITY{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
			REAL PARAM_BLEND_NOISE_SCALE{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 2; MIN 0; STEP 0.02; }
			BOOL PARAM_USE_HEIGHT_BLENDING{EDITPORT; INPORT;}
		}
		GROUP GROUP_VARIATION {
		DEFAULT 1;
			REAL PARAM_VARIATION_OFFSET{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
			REAL PARAM_VARIATION_AXIS_ROTATION{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
			REAL PARAM_VARIATION_SCALE{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
		}
		GROUP GROUP_TILE_REMOVAL {
		DEFAULT 1;
			BOOL PARAM_TILE_REMOVAL{EDITPORT; INPORT;}
			REAL PARAM_TILE_SOFTNESS{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; MAX 1; STEP 0.01; }
			REAL PARAM_TILE_OFFSET{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
			REAL PARAM_TILE_ROTATION{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
			REAL PARAM_TILE_SCALE{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
		}
		GROUP GROUP_IMAGE_SEQUENCE {
		DEFAULT 1;
			BOOL PARAM_USEIMAGESEQUENCE{EDITPORT; INPORT;}
			LONG PARAM_FRAME{EDITPORT; INPORT; }
			LONG PARAM_FRAMEOFFSET{EDITPORT; INPORT; }
		}
		MATRIX PARAM_PLACEMENTMATRIX{EDITPORT; PORTONLY; INPORT; }
		COLOR PARAM_OUTCOLOR{EDITPORT; OUTPORT;  CREATEPORT;}
		REAL PARAM_OUTALPHA{EDITPORT; OUTPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01;  CREATEPORT;}
		REAL PARAM_OUTFLOAT{EDITPORT; OUTPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01;  CREATEPORT;}
		REAL PARAM_OUTHEIGHT{EDITPORT; OUTPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01;  CREATEPORT;}
		COLOR PARAM_OUTMASK{EDITPORT; OUTPORT;  CREATEPORT;}
	}
}

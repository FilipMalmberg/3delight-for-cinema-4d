#pragma once

enum {
	PARAM_I_COLOR=1000,
	PARAM_INTENSITY,
	PARAM_EXPOSURE,
	PARAM_OPACITY,
	PARAM_OPACITY_CUTS_COLOR,
	PARAM_CASTS_SHADOWS,
	PARAM_DOUBLESIDED,
	PARAM_SPREAD,
	PARAM_DIFFUSE_CONTRIBUTION,
	PARAM_REFLECTION_CONTRIBUTION,
	PARAM_REFRACTION_CONTRIBUTION,
	PARAM_VOLUME_CONTRIBUTION,
	PARAM_INCANDESCENCE_MULTIPLIER,
	PARAM_AOVGROUP,
	PARAM_OUTCOLOR,
	GROUP_BASE,
	GROUP_CONTRIBUTIONS
};

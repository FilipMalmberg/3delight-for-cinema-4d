#pragma once

enum {
	PARAM_MODE=1000,
	PARAM_MODE_ABSOLUTE,
	PARAM_MODE_MODULO,
	PARAM_MODE_FLOOR,
	PARAM_MODE_CEIL,
	PARAM_MODE_ROUND,
	PARAM_MODE_TRUNCATE,
	PARAM_MODE_FRACTIONAL,
	PARAM_MODE_NEGATE,
	PARAM_MODE_COMPLEMENT,
	PARAM_MODE_RECIPROCAL,
	PARAM_MODE_EXPONENT,
	PARAM_MODE_POWER,
	PARAM_MODE_SQUARE_ROOT,
	PARAM_MODE_LOGARITHM,
	PARAM_MODE_HYPOTENUSE,
	PARAM_MODE_RADIANS,
	PARAM_MODE_DEGREES,
	PARAM_MODE_COSINE,
	PARAM_MODE_SINE,
	PARAM_MODE_TANGENT,
	PARAM_MODE_ARCCOSINE,
	PARAM_MODE_ARCSINE,
	PARAM_MODE_HYPERBOLIC_COSINE,
	PARAM_MODE_HYPERBOLIC_SINE,
	PARAM_MODE_HYPERBOLIC_TANGENT,
	PARAM_INPUT1,
	PARAM_INPUT2,
	PARAM_OUTFLOAT
};

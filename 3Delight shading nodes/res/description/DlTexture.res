CONTAINER dlTexture
{
	NAME dlTexture;

	INCLUDE GVbase;
	GROUP ID_GVPROPERTIES {
	DEFAULT 1;
		GROUP GROUP_TEXTURE_FILE {
		DEFAULT 1;
			FILENAME PARAM_TEXTUREFILE{EDITPORT; INPORT; }
			LONG PARAM_TEXTUREFILE_META_COLORSPACE{ CYCLE{PARAM_TEXTUREFILE_META_COLORSPACE_AUTO; PARAM_TEXTUREFILE_META_COLORSPACE_SRGB; PARAM_TEXTUREFILE_META_COLORSPACE_REC__709; PARAM_TEXTUREFILE_META_COLORSPACE_LINEAR; }; EDITPORT; INPORT;}
			COLOR PARAM_DEFAULTCOLOR{EDITPORT; INPORT; }
		}
		GROUP GROUP_TILE_REMOVAL {
		DEFAULT 1;
			BOOL PARAM_TILEREMOVAL{EDITPORT; INPORT;}
			REAL PARAM_TILESOFTNESS{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; MAX 1; STEP 0.01; }
			REAL PARAM_TILEOFFSET{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
			REAL PARAM_TILEROTATION{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
			REAL PARAM_TILESCALE{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; MIN 0; STEP 0.01; }
		}
		GROUP GROUP_COLOR_CORRECT {
		DEFAULT 1;
			LONG PARAM_TONALADJUST{ CYCLE{PARAM_TONALADJUST_OFF; PARAM_TONALADJUST_NORMALIZE__AUTO_LEVELS_; }; EDITPORT; INPORT;}
			COLOR PARAM_COLORGAIN{EDITPORT; INPORT; }
			COLOR PARAM_COLOROFFSET{EDITPORT; INPORT; }
			REAL PARAM_ALPHAGAIN{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01; }
			REAL PARAM_ALPHAOFFSET{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01; }
			BOOL PARAM_ALPHAISLUMINANCE{EDITPORT; INPORT;}
			BOOL PARAM_INVERT{EDITPORT; INPORT;}
		}
		GROUP GROUP_IMAGE_SEQUENCE {
		DEFAULT 1;
			BOOL PARAM_USEIMAGESEQUENCE{EDITPORT; INPORT;}
			LONG PARAM_FRAME{EDITPORT; INPORT; }
			LONG PARAM_FRAMEOFFSET{EDITPORT; INPORT; }
		}
		GROUP GROUP_FILTERING {
		DEFAULT 1;
			REAL PARAM_FILTER{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01; }
			REAL PARAM_BLUR{EDITPORT; INPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01; }
		}
		COLOR PARAM_OUTCOLOR{EDITPORT; OUTPORT;  CREATEPORT;}
		REAL PARAM_OUTCOLORR{EDITPORT; OUTPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01;  CREATEPORT;}
		REAL PARAM_OUTCOLORG{EDITPORT; OUTPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01;  CREATEPORT;}
		REAL PARAM_OUTCOLORB{EDITPORT; OUTPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01;  CREATEPORT;}
		REAL PARAM_OUTALPHA{EDITPORT; OUTPORT; CUSTOMGUI REALSLIDER; MINSLIDER 0; MAXSLIDER 1; STEP 0.01;  CREATEPORT;}
	}
}

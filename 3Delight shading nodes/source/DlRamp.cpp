//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlRamp.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlRamp : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlRamp); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlRamp::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetInt32(PARAM_MODE , PARAM_MODE_HORIZONTAL__U_);
	{
		AutoAlloc<Gradient> gradient;
		if (!gradient) return false;
		GradientKnot k;
		k.col = Vector(0, 0, 0);
		k.pos = 0;
		gradient->InsertKnot(k);
		k.col = Vector(1, 1, 1);
		k.pos = 1;
		gradient->InsertKnot(k);
		data->SetData(PARAM_COLORRAMP_COLORVALUE, GeData(CUSTOMDATATYPE_GRADIENT, *gradient));
	}
	data->SetInt32(PARAM_COLOR_MIX_MODE , PARAM_COLOR_MIX_MODE_RGB);
	data->SetFloat(PARAM_INPUT , 1);
	data->SetFloat(PARAM_REPEAT , 1);
	data->SetFloat(PARAM_OFFSET , 0);
	data->SetBool(PARAM_REVERSE , 0);
	data->SetFloat(PARAM_DISTORTION_INTENSITY , 0);
	data->SetVector(PARAM_DISTORTION , Vector(0, 0, 0));
	data->SetFloat(PARAM_NOISE_INTENSITY , 0);
	data->SetFloat(PARAM_NOISE_HUE_INTENSITY , 0.5);
	data->SetFloat(PARAM_NOISE_SATURATION_INTENSITY , 0.5);
	data->SetFloat(PARAM_NOISE_VALUE_INTENSITY , 0.5);
	data->SetFloat(PARAM_NOISE_SCALE , 0.5);
	data->SetFloat(PARAM_NOISE_OFFSET , 0);
	data->SetFloat(PARAM_NOISE_STEPPING , 0);
	data->SetFloat(PARAM_NOISE_TIME , 0);
	data->SetInt32(PARAM_LAYERS , 1);
	data->SetFloat(PARAM_LAYER_PERSISTENCE , 0.7);
	data->SetFloat(PARAM_LAYER_SCALE , 0.45);
	data->SetBool(PARAM_INVERT , 0);
	data->SetVector(PARAM_COLORGAIN , Vector(1, 1, 1));
	data->SetVector(PARAM_COLOROFFSET , Vector(0, 0, 0));
	data->SetFloat(PARAM_ALPHAGAIN , 1);
	data->SetFloat(PARAM_ALPHAOFFSET , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlRamp(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLRAMP, "Ramp"_s, 0, DlRamp::Alloc, "dlRamp"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

#include "DlColorBlendTranslator.h"
#include "DlColorBlend.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlColorBlendTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlColorBlend.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void DlColorBlendTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlColorBlendTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			Int32 option = data->GetInt32(PARAM_BLENDMODE);
			if(option == PARAM_BLENDMODE_OVER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 0));
			}
			else if(option == PARAM_BLENDMODE_MULTIPLY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 1));
			}
			else if(option == PARAM_BLENDMODE_DARKEN__MIN_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 2));
			}
			else if(option == PARAM_BLENDMODE_COLOR_BURN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 3));
			}
			else if(option == PARAM_BLENDMODE_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 4));
			}
			else if(option == PARAM_BLENDMODE_SCREEN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 5));
			}
			else if(option == PARAM_BLENDMODE_LIGHTEN__MAX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 6));
			}
			else if(option == PARAM_BLENDMODE_COLOR_DODGE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 7));
			}
			else if(option == PARAM_BLENDMODE_OVERLAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 8));
			}
			else if(option == PARAM_BLENDMODE_SOFT_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 9));
			}
			else if(option == PARAM_BLENDMODE_HARD_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 10));
			}
			else if(option == PARAM_BLENDMODE_ADD_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 11));
			}
			else if(option == PARAM_BLENDMODE_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 12));
			}
			else if(option == PARAM_BLENDMODE_DIFFERENCE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 13));
			}
			else if(option == PARAM_BLENDMODE_DIVIDE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 14));
			}
			else if(option == PARAM_BLENDMODE_HUE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 15));
			}
			else if(option == PARAM_BLENDMODE_SATURATION){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 16));
			}
			else if(option == PARAM_BLENDMODE_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 17));
			}
			else if(option == PARAM_BLENDMODE_LUMINOSITY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode", 18));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_FG), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("fg", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_BG), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("bg", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_BLEND);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend", f));

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTCOLORR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorR", f));

		f = (float) data->GetFloat(PARAM_OUTCOLORG);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorG", f));

		f = (float) data->GetFloat(PARAM_OUTCOLORB);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorB", f));

}

void  DlColorBlendTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLCOLORBLEND, PARAM_BLENDMODE, "blendMode");
	PM->RegisterAttributeName(ID_DLCOLORBLEND, PARAM_FG, "fg");
	PM->RegisterAttributeName(ID_DLCOLORBLEND, PARAM_BG, "bg");
	PM->RegisterAttributeName(ID_DLCOLORBLEND, PARAM_BLEND, "blend");
	PM->RegisterAttributeName(ID_DLCOLORBLEND, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DLCOLORBLEND, PARAM_OUTCOLORR, "outColorR");
	PM->RegisterAttributeName(ID_DLCOLORBLEND, PARAM_OUTCOLORG, "outColorG");
	PM->RegisterAttributeName(ID_DLCOLORBLEND, PARAM_OUTCOLORB, "outColorB");
}

//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlToonGlass.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlToonGlass : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlToonGlass); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlToonGlass::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_I_COLOR , Vector(1, 1, 1));
	data->SetFloat(PARAM_REFRACT_IOR , 1.3);
	data->SetBool(PARAM_FG_OUTLINE_OBJECTS_ENABLE , 0);
	data->SetVector(PARAM_FG_OUTLINE_OBJECTS_COLOR , Vector(0.5, 0.5, 0.5));
	data->SetFloat(PARAM_FG_OUTLINE_OBJECTS_OPACITY , 1);
	data->SetFloat(PARAM_FG_OUTLINE_OBJECTS_WIDTH , 0.01);
	data->SetInt32(PARAM_FG_OUTLINE_OBJECTS_PRIORITY , 0);
	data->SetFloat(PARAM_OUTLINE_FADE , 1);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlToonGlass(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLTOONGLASS, "Toon Glass"_s, 0, DlToonGlass::Alloc, "dlToonGlass"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlFloat.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlFloat : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlFloat); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlFloat::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetFloat(PARAM_INFLOAT , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlFloat(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLFLOAT, "Float"_s, 0, DlFloat::Alloc, "dlFloat"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

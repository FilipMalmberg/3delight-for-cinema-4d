#include "main.h"

#include "IDs.h"
#include "delightenvironment.h"
#include "dlTerminalTranslator.h"
#include "dlPrincipledTranslator.h"
#include "DlCarPaintTranslator.h"
#include "dlMetalTranslator.h"
#include "DlGlassTranslator.h"
#include "DlSkinTranslator.h"
#include "DlToonTranslator.h"
#include "DlToonGlassTranslator.h"
#include "DlHairAndFurTranslator.h"
#include "DlConstantTranslator.h"
#include "DlLayeredMaterialTranslator.h"
#include "DlThinTranslator.h"
#include "DlRandomMaterialTranslator.h"
#include "DlSubstanceTranslator.h"
#include "DlShadowMatteTranslator.h"
#include "DlTriplanarTranslator.h"
#include "dlTextureTranslator.h"
#include "ColorCorrectionTranslator.h"
#include "DlPrimitiveAttributeTranslator.h"
#include "DlRemapTranslator.h"
#include "DlFloatTranslator.h"
#include "DlFloatBlendTranslator.h"		
#include "DlFloatMathTranslator.h"
#include "DlColorTranslator.h"
#include "DlColorBlendTranslator.h"
#include "DlColorBlendMultiTranslator.h"
#include "DlDisplacementBlendTranslator.h"
#include "DlFacingRatioTranslator.h"
#include "DlRampTranslator.h"
#include "DlSolidRampTranslator.h"
#include "TextureAxisTranslator.h"
#include "DlNoiseTranslator.h"
#include "Place3DTextureTranslator.h"
#include "DlWorleyNoiseTranslator.h"
#include "DlBoxNoiseTranslator.h"
#include "DlFlakesTranslator.h"
#include "DlTilesTranslator.h"
#include "DecalTranslator.h"

#include <map>

Bool RegisterOperatorClass();
Bool RegisterShadingGroups();
Bool RegisterClosureDataType();
Bool RegisterDlTerminal(void);

Bool RegisterDlPrincipled(const char* icon, int node_group);
Bool RegisterDlMetal(const char* icon, int node_group);
Bool RegisterDlSkin(const char* icon, int node_group);
Bool RegisterDlGlass(const char* icon, int node_group);
Bool RegisterDlCarPaint(const char* icon, int node_group);
Bool RegisterDlToon(const char* icon, int node_group);
Bool RegisterDlToonGlass(const char* icon, int node_group);
Bool RegisterDlHairAndFur(const char* icon, int node_group);
Bool RegisterDlConstant(const char* icon, int node_group);
Bool RegisterDlLayeredMaterial(const char* icon, int node_group);
Bool RegisterDlSubstance(const char* icon, int node_group);
Bool RegisterDlRandomMaterial(const char* icon, int node_group);
Bool RegisterDlThin(const char* icon, int node_group);
Bool RegisterDlShadowMatte(const char* icon, int node_group);
Bool RegisterDlTexture(const char* icon, int node_group);
Bool RegisterDlTriplanar(const char* icon, int node_group);
Bool RegisterDlPrimitiveAttribute(const char* icon, int node_group);
Bool RegisterColorCorrection(const char* icon, int node_group);
Bool RegisterDlRemap(const char* icon, int node_group);
Bool RegisterDlFloat(const char* icon, int node_group);
Bool RegisterDlFloatBlend(const char* icon, int node_group);
Bool RegisterDlFloatMath(const char* icon, int node_group);
Bool RegisterDlColor(const char* icon, int node_group);
Bool RegisterDlColorBlend(const char* icon, int node_group);
Bool RegisterDlColorBlendMulti(const char* icon, int node_group);
Bool RegisterDlRamp(const char* icon, int node_group);
Bool RegisterDlSolidRamp(const char* icon, int node_group);
Bool RegisterLocator(const char* icon, int node_group);
Bool RegisterTextureAxis(const char* icon, int node_group);
Bool RegisterTextureAxis(const char* icon, int node_group);
Bool RegisterDlNoise(const char* icon, int node_group);
Bool RegisterPlace3DTextureWidget(void);
Bool RegisterPlace3DTexture(const char* icon, int node_group);
Bool RegisterDlTiles(const char* icon, int node_group);
Bool RegisterDlWorleyNoise(const char* icon, int node_group);
Bool RegisterDlFlakes(const char* icon, int node_group);
Bool RegisterDlBoxNoise(const char* icon , int node_group);
Bool RegisterDlDisplacementBlend(const char* icon, int node_group);
Bool RegisterDlFacingRatio(const char* icon, int node_group);
Bool RegisterDecal(const char* icon, int node_group);


Bool PluginStart(void){

	
	if (!RegisterOperatorClass()) { return false; }
	if (!RegisterShadingGroups()) { return false; }
	if (!RegisterClosureDataType()) { return false; }
	
	//Register all shader nodes
	if (!RegisterDlTerminal()) { return false; }

	//Materials
	if (!RegisterDlPrincipled("shelf_dlPrincipled_200.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlMetal("shelf_metal.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlSkin("shelf_skin.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlGlass("shelf_glass.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlCarPaint("shelf_car_paint.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlToon("shelf_toon.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlToonGlass("shelf_toon_glass.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlLayeredMaterial("shelf_layered.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlHairAndFur("shelf_hair_and_fur.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlSubstance("shelf_substance.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlThin("shelf_thin.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlRandomMaterial("shelf_random_material.png", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlConstant("dl_200.tif", DL_MATERIAL_GROUP)) { return false; }
	if (!RegisterDlShadowMatte("dl_200.tif", DL_MATERIAL_GROUP)) { return false; }

	//2D textures
	if (!RegisterDlTexture("dl_200.tif", DL_TWODTEXTURE_GROUP)) { return false; }
	if (!RegisterDlRamp("dl_200.tif", DL_TWODTEXTURE_GROUP)) { return false; }
	if (!RegisterDlTiles("dl_200.tif", DL_TWODTEXTURE_GROUP)) { return false; }

	//3D textures
	if (!RegisterDlTriplanar("dl_200.tif", DL_THREEDTEXTURE_GROUP)) { return false; }
	if (!RegisterDlNoise("dl_200.tif", DL_THREEDTEXTURE_GROUP)) { return false; }
	if (!RegisterDlSolidRamp("dl_200.tif", DL_THREEDTEXTURE_GROUP)) { return false; }
	if (!RegisterDlWorleyNoise("dl_200.tif", DL_THREEDTEXTURE_GROUP)) { return false; }
	if (!RegisterDlFlakes("dl_200.tif", DL_THREEDTEXTURE_GROUP)) { return false; }
	if (!RegisterDlBoxNoise("dl_200.tif", DL_THREEDTEXTURE_GROUP)) { return false; }
	if (!RegisterDecal("dl_200.tif", DL_THREEDTEXTURE_GROUP)) { return false; }

	//Utilities
	if (!RegisterDlPrimitiveAttribute("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterColorCorrection("dl_200.tif",DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterDlRemap("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterDlFloat("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterDlFloatBlend("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterDlFloatMath("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterDlColor("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterDlColorBlend("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterDlColorBlendMulti("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterDlFacingRatio("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterDlDisplacementBlend("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }

	//3dlfc4d-specific
	if (!RegisterTextureAxis("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
	if (!RegisterPlace3DTexture("dl_200.tif", DL_UTILITIES_GROUP)) { return false; }
		
	//GUI widgets
	if (!RegisterPlace3DTextureWidget()) { return false; }

	return true;
}

void PluginEnd(void)
{

}

Bool PluginMessage(Int32 id, void *data)
{
	switch (id)
	{
	case C4DPL_INIT_SYS:
		if (!g_resource.Init()) return FALSE; // don't start plugin without resource
		return TRUE;
		break;

	case DL_LOAD_PLUGINS:
		DL_PluginManager* pm = (DL_PluginManager*)data;

		pm->RegisterHook(AllocateHook<ShaderSettingsHook>);

		pm->RegisterHook(AllocateHook<DelightUVCoord>);
		
		pm->RegisterTranslator(DL_TERMINAL, AllocateTranslator<dlTerminalTranslator>);
		dlTerminalTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(Ttexture, AllocateTranslator<TextureTagTranslator>);

		pm->RegisterTranslator(ID_DLPRINCIPLED, AllocateTranslator<DlPrincipledTranslator>);
		DlPrincipledTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLMETAL, AllocateTranslator<DlMetalTranslator>);
		DlMetalTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLGLASS, AllocateTranslator<DlGlassTranslator>);
		DlGlassTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLSKIN, AllocateTranslator<DlSkinTranslator>);
		DlSkinTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLCARPAINT, AllocateTranslator<DlCarPaintTranslator>);
		DlCarPaintTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLTOON, AllocateTranslator<DlToonTranslator>);
		DlToonTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLTOONGLASS, AllocateTranslator<DlToonGlassTranslator>);
		DlToonGlassTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLHAIRANDFUR, AllocateTranslator<DlHairAndFurTranslator>);
		DlHairAndFurTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLCONSTANT, AllocateTranslator<DlConstantTranslator>);
		DlConstantTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLLAYEREDMATERIAL, AllocateTranslator<DlLayeredMaterialTranslator>);
		DlLayeredMaterialTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLTHIN, AllocateTranslator<DlThinTranslator>);
		DlThinTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLSUBSTANCE, AllocateTranslator<DlSubstanceTranslator>);
		DlSubstanceTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLRANDOMMATERIAL, AllocateTranslator<DlRandomMaterialTranslator>);
		DlRandomMaterialTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLSHADOWMATTE, AllocateTranslator<DlShadowMatteTranslator>);
		DlShadowMatteTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLTEXTURE, AllocateTranslator<DlTextureTranslator>);
		DlTextureTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLTRIPLANAR, AllocateTranslator<DlTriplanarTranslator>);
		DlTriplanarTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DECAL, AllocateTranslator<DecalTranslator>);
		DecalTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLNOISE, AllocateTranslator<DlNoiseTranslator>);
		DlNoiseTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLWORLEYNOISE, AllocateTranslator<DlWorleyNoiseTranslator>);
		DlWorleyNoiseTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLBOXNOISE, AllocateTranslator<DlBoxNoiseTranslator>);
		DlBoxNoiseTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLFLAKES, AllocateTranslator<DlFlakesTranslator>);
		DlFlakesTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLTILES, AllocateTranslator<DlTilesTranslator>);
		DlTilesTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLPRIMITIVEATTRIBUTE, AllocateTranslator<DlPrimitiveAttributeTranslator>);
		DlPrimitiveAttributeTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_COLORCORRECTION, AllocateTranslator<ColorCorrectionTranslator>);
		ColorCorrectionTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLREMAP, AllocateTranslator<DlRemapTranslator>);
		DlRemapTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLFLOATBLEND, AllocateTranslator<DlFloatBlendTranslator>);
		DlFloatBlendTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLFLOAT, AllocateTranslator<DlFloatTranslator>);
		DlFloatTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLFLOATMATH, AllocateTranslator<DlFloatMathTranslator>);
		DlFloatMathTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLCOLOR, AllocateTranslator<DlColorTranslator>);
		DlColorTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLCOLORBLEND, AllocateTranslator<DlColorBlendTranslator>);
		DlColorBlendTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLCOLORBLENDMULTI, AllocateTranslator<DlColorBlendMultiTranslator>);
		DlColorBlendMultiTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLDISPLACEMENTBLEND, AllocateTranslator<DlDisplacementBlendTranslator>);
		DlDisplacementBlendTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLFACINGRATIO, AllocateTranslator<DlFacingRatioTranslator>);
		DlFacingRatioTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(	ID_DLRAMP, AllocateTranslator<DlRampTranslator>);
		DlRampTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_DLSOLIDRAMP, AllocateTranslator<DlSolidRampTranslator>);
		DlSolidRampTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_TEXTUREAXIS, AllocateTranslator<TextureAxisTranslator>);
		TextureAxisTranslator::RegisterAttributeNames(pm);

		pm->RegisterTranslator(ID_PLACE3DTEXTURE, AllocateTranslator<Place3DTextureTranslator>);
		Place3DTextureTranslator::RegisterAttributeNames(pm);

	
		break;
	}
	return FALSE;
}

#include "DecalTranslator.h"
#include "Decal.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DecalTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/decal.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("3dlfc4d::default_placement_matrix", "out", shader_handle, "placementMatrix" );
}

void DecalTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DecalTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			Filename fn = data->GetFilename(PARAM_COLOR_TEXTURE);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("color_texture", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_COLOR_TEXTURE_META_COLORSPACE);
			if(option == PARAM_COLOR_TEXTURE_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("color_texture_meta_colorspace", "auto"));
			}
			else if(option == PARAM_COLOR_TEXTURE_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("color_texture_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_COLOR_TEXTURE_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("color_texture_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_COLOR_TEXTURE_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("color_texture_meta_colorspace", "linear"));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_BACKGROUND_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("background_color", &v_data[0]));
	}

		{
			Filename fn = data->GetFilename(PARAM_FLOAT_TEXTURE);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("float_texture", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_FLOAT_TEXTURE_META_COLORSPACE);
			if(option == PARAM_FLOAT_TEXTURE_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("float_texture_meta_colorspace", "auto"));
			}
			else if(option == PARAM_FLOAT_TEXTURE_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("float_texture_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_FLOAT_TEXTURE_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("float_texture_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_FLOAT_TEXTURE_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("float_texture_meta_colorspace", "linear"));
			}
		}

		f = (float) data->GetFloat(PARAM_BACKGROUND_VALUE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("background_value", f));

		{
			Int32 option = data->GetInt32(PARAM_SPACE);
			if(option == PARAM_SPACE_WORLD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 0));
			}
			else if(option == PARAM_SPACE_OBJECT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 1));
			}
		}

	{
		Matrix m = data->GetMatrix(PARAM_PLACEMENTMATRIX);
		std::vector<float> mdata = MatrixToNSIMatrix(m);
		NSI::Argument matrix_arg("placementMatrix");
		matrix_arg.SetType(NSITypeMatrix);
		matrix_arg.SetValuePointer((void*)&mdata[0]);
		ctx.SetAttribute(shader_handle, matrix_arg);
	}

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTALPHA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outAlpha", f));

		f = (float) data->GetFloat(PARAM_OUTFLOAT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outFloat", f));

}

void  DecalTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DECAL, PARAM_COLOR_TEXTURE, "color_texture");
	PM->RegisterAttributeName(ID_DECAL, PARAM_COLOR_TEXTURE_META_COLORSPACE, "color_texture_meta_colorspace");
	PM->RegisterAttributeName(ID_DECAL, PARAM_BACKGROUND_COLOR, "background_color");
	PM->RegisterAttributeName(ID_DECAL, PARAM_FLOAT_TEXTURE, "float_texture");
	PM->RegisterAttributeName(ID_DECAL, PARAM_FLOAT_TEXTURE_META_COLORSPACE, "float_texture_meta_colorspace");
	PM->RegisterAttributeName(ID_DECAL, PARAM_BACKGROUND_VALUE, "background_value");
	PM->RegisterAttributeName(ID_DECAL, PARAM_SPACE, "space");
	PM->RegisterAttributeName(ID_DECAL, PARAM_PLACEMENTMATRIX, "placementMatrix");
	PM->RegisterAttributeName(ID_DECAL, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DECAL, PARAM_OUTALPHA, "outAlpha");
	PM->RegisterAttributeName(ID_DECAL, PARAM_OUTFLOAT, "outFloat");
}

//XPRESSO nodes are divided into classes and groups, used in the GUI to sort nodes by category
//Here, we register custom classes and groups for XPRESSO nodes related to 3delight shaders

#include "c4d_graphview.h"
#include <c4d_graphview_def.h>
#include "IDs.h"

//GV class
static const String* GetGVClassName()
{
	static String mygroup("3Delight shading nodes");
	return &mygroup;
}

static BaseBitmap* GetGVClassIcon()
{
	return nullptr;
}

static GV_OPCLASS_HANDLER DLNodesClass;




Bool RegisterOperatorClass() {
	DLNodesClass.class_id = DL_NODES_CLASS;
	DLNodesClass.GetName = GetGVClassName;
	DLNodesClass.GetIcon = GetGVClassIcon;
	return GvRegisterOpClassType(&DLNodesClass, sizeof(DLNodesClass));

}

//Common for all groups
static BaseBitmap* GetGVGroupIcon(void)
{
	return nullptr;
}


//Material group
static const String* GetMaterialGroupName(void)
{
	static String mygroup("Materials");
	return &mygroup;
}

//2D texture group
static const String* Get2dTextureGroupName(void)
{
	static String mygroup("2D textures");
	return &mygroup;
}

//3D texture group
static const String* Get3dTextureGroupName(void)
{
	static String mygroup("3D textures");
	return &mygroup;
}

//Volume group
static const String* GetVolumeGroupName(void)
{
	static String mygroup("Volume");
	return &mygroup;
}

//Utilities group
static const String* GetUtilitiesGroupName(void)
{
	static String mygroup("Utilities");
	return &mygroup;
}



static GV_OPGROUP_HANDLER MaterialGroup;
static GV_OPGROUP_HANDLER Texture3DGroup;
static GV_OPGROUP_HANDLER Texture2DGroup;
static GV_OPGROUP_HANDLER VolumeGroup;
static GV_OPGROUP_HANDLER UtilitiesGroup;


//Add other groups here.
//Not yet supported in Shadergen

Bool RegisterShadingGroups() {
	bool result = true;

	MaterialGroup.group_id = DL_MATERIAL_GROUP;
	MaterialGroup.GetName = GetMaterialGroupName;
	MaterialGroup.GetIcon = GetGVGroupIcon;
	result=result && GvRegisterOpGroupType(&MaterialGroup, sizeof(MaterialGroup));

	Texture3DGroup.group_id = DL_THREEDTEXTURE_GROUP;
	Texture3DGroup.GetName = Get3dTextureGroupName;
	Texture3DGroup.GetIcon = GetGVGroupIcon;
	result = result && GvRegisterOpGroupType(&Texture3DGroup, sizeof(Texture3DGroup));

	Texture2DGroup.group_id = DL_TWODTEXTURE_GROUP;
	Texture2DGroup.GetName = Get2dTextureGroupName;
	Texture2DGroup.GetIcon = GetGVGroupIcon;
	result = result && GvRegisterOpGroupType(&Texture2DGroup, sizeof(Texture2DGroup));

	VolumeGroup.group_id = DL_VOLUME_GROUP;
	VolumeGroup.GetName = GetVolumeGroupName;
	VolumeGroup.GetIcon = GetGVGroupIcon;
	result = result && GvRegisterOpGroupType(&VolumeGroup, sizeof(VolumeGroup));


	UtilitiesGroup.group_id = DL_UTILITIES_GROUP;
	UtilitiesGroup.GetName = GetUtilitiesGroupName;
	UtilitiesGroup.GetIcon = GetGVGroupIcon;
	result = result && GvRegisterOpGroupType(&UtilitiesGroup, sizeof(UtilitiesGroup));
	
	return result;

}


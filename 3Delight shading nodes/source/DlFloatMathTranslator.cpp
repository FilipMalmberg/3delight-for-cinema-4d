#include "DlFloatMathTranslator.h"
#include "DlFloatMath.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlFloatMathTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlFloatMath.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void DlFloatMathTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlFloatMathTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			Int32 option = data->GetInt32(PARAM_MODE);
			if(option == PARAM_MODE_ABSOLUTE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 0));
			}
			else if(option == PARAM_MODE_MODULO){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 1));
			}
			else if(option == PARAM_MODE_FLOOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 2));
			}
			else if(option == PARAM_MODE_CEIL){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 3));
			}
			else if(option == PARAM_MODE_ROUND){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 4));
			}
			else if(option == PARAM_MODE_TRUNCATE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 5));
			}
			else if(option == PARAM_MODE_FRACTIONAL){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 6));
			}
			else if(option == PARAM_MODE_NEGATE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 7));
			}
			else if(option == PARAM_MODE_COMPLEMENT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 8));
			}
			else if(option == PARAM_MODE_RECIPROCAL){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 9));
			}
			else if(option == PARAM_MODE_EXPONENT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 10));
			}
			else if(option == PARAM_MODE_POWER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 11));
			}
			else if(option == PARAM_MODE_SQUARE_ROOT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 12));
			}
			else if(option == PARAM_MODE_LOGARITHM){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 13));
			}
			else if(option == PARAM_MODE_HYPOTENUSE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 14));
			}
			else if(option == PARAM_MODE_RADIANS){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 15));
			}
			else if(option == PARAM_MODE_DEGREES){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 16));
			}
			else if(option == PARAM_MODE_COSINE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 17));
			}
			else if(option == PARAM_MODE_SINE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 18));
			}
			else if(option == PARAM_MODE_TANGENT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 19));
			}
			else if(option == PARAM_MODE_ARCCOSINE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 20));
			}
			else if(option == PARAM_MODE_ARCSINE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 21));
			}
			else if(option == PARAM_MODE_HYPERBOLIC_COSINE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 22));
			}
			else if(option == PARAM_MODE_HYPERBOLIC_SINE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 23));
			}
			else if(option == PARAM_MODE_HYPERBOLIC_TANGENT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 24));
			}
		}

		f = (float) data->GetFloat(PARAM_INPUT1);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("input1", f));

		f = (float) data->GetFloat(PARAM_INPUT2);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("input2", f));

		f = (float) data->GetFloat(PARAM_OUTFLOAT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outFloat", f));

}

void  DlFloatMathTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLFLOATMATH, PARAM_MODE, "mode");
	PM->RegisterAttributeName(ID_DLFLOATMATH, PARAM_INPUT1, "input1");
	PM->RegisterAttributeName(ID_DLFLOATMATH, PARAM_INPUT2, "input2");
	PM->RegisterAttributeName(ID_DLFLOATMATH, PARAM_OUTFLOAT, "outFloat");
}

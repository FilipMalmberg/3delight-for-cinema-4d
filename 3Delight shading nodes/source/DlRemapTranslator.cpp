#include "DlRemapTranslator.h"
#include "DlRemap.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlRemapTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlRemap.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void DlRemapTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlRemapTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		f = (float) data->GetFloat(PARAM_INPUTVALUE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("inputValue", f));

		{
			SplineData* spline = (SplineData*)data->GetCustomDataType(PARAM_VALUE_FLOATVALUE, CUSTOMDATATYPE_SPLINE);

			int knot_count;
			std::vector<float> knots;
			std::vector<float> values;
			std::vector<int> interpolations;
			FillFloatRampNSIData(spline, doc, knots, values, interpolations, knot_count);

			NSI::Argument arg_knot_values("value_FloatValue");
			arg_knot_values.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_values.SetValuePointer((void*)&values[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_values));

			NSI::Argument arg_knot_position("value_Position");
			arg_knot_position.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_position.SetValuePointer((void*)&knots[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_position));

			NSI::Argument arg_knot_interpolation("value_Interp");
			arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);
			arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_interpolation));

		}

		{
			Gradient* gradient = (Gradient*)data->GetCustomDataType(PARAM_COLOR_COLOR, CUSTOMDATATYPE_GRADIENT );

			int knot_count;
			std::vector<float> knots;
			std::vector<float> colors;
			std::vector<int> interpolations;
			FillColorRampNSIData(gradient, doc, knots, colors, interpolations, knot_count);

			NSI::Argument arg_knot_color("color_Color");
			arg_knot_color.SetArrayType(NSITypeColor, knot_count);
			arg_knot_color.SetValuePointer((void*)&colors[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_color));

			NSI::Argument arg_knot_position("color_Position");
			arg_knot_position.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_position.SetValuePointer((void*)&knots[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_position));

			NSI::Argument arg_knot_interpolation("color_Interp");
			arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);
			arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_interpolation));

		}

		f = (float) data->GetFloat(PARAM_INPUTMIN);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("inputMin", f));

		f = (float) data->GetFloat(PARAM_INPUTMAX);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("inputMax", f));

		f = (float) data->GetFloat(PARAM_OUTPUTMIN);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outputMin", f));

		f = (float) data->GetFloat(PARAM_OUTPUTMAX);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outputMax", f));

		f = (float) data->GetFloat(PARAM_O_OUTVALUE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_outValue", f));

	{
		v = toLinear(data->GetVector(PARAM_O_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("o_outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_O_OUTCOLORR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_outColorR", f));

		f = (float) data->GetFloat(PARAM_O_OUTCOLORG);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_outColorG", f));

		f = (float) data->GetFloat(PARAM_O_OUTCOLORB);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_outColorB", f));

}

void  DlRemapTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_INPUTVALUE, "inputValue");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_VALUE_POSITION, "value_Position");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_VALUE_FLOATVALUE, "value_FloatValue");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_VALUE_INTERP, "value_Interp");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_COLOR_POSITION, "color_Position");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_COLOR_COLOR, "color_Color");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_COLOR_INTERP, "color_Interp");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_INPUTMIN, "inputMin");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_INPUTMAX, "inputMax");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_OUTPUTMIN, "outputMin");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_OUTPUTMAX, "outputMax");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_O_OUTVALUE, "o_outValue");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_O_OUTCOLOR, "o_outColor");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_O_OUTCOLORR, "o_outColorR");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_O_OUTCOLORG, "o_outColorG");
	PM->RegisterAttributeName(ID_DLREMAP, PARAM_O_OUTCOLORB, "o_outColorB");
}

//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlTexture.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlTexture : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlTexture); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlTexture::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetString(PARAM_TEXTUREFILE, String(""));
	data->SetInt32(PARAM_TEXTUREFILE_META_COLORSPACE, PARAM_TEXTUREFILE_META_COLORSPACE_AUTO);
	data->SetVector(PARAM_DEFAULTCOLOR , Vector(0, 0, 0));
	data->SetBool(PARAM_TILEREMOVAL , 0);
	data->SetFloat(PARAM_TILESOFTNESS , 0.75);
	data->SetFloat(PARAM_TILEOFFSET , 1);
	data->SetFloat(PARAM_TILEROTATION , 1);
	data->SetFloat(PARAM_TILESCALE , 0.5);
	data->SetInt32(PARAM_TONALADJUST , PARAM_TONALADJUST_OFF);
	data->SetVector(PARAM_COLORGAIN , Vector(1, 1, 1));
	data->SetVector(PARAM_COLOROFFSET , Vector(0, 0, 0));
	data->SetFloat(PARAM_ALPHAGAIN , 1);
	data->SetFloat(PARAM_ALPHAOFFSET , 0);
	data->SetBool(PARAM_ALPHAISLUMINANCE , 1);
	data->SetBool(PARAM_INVERT , 0);
	data->SetBool(PARAM_USEIMAGESEQUENCE , 0);
	data->SetInt32(PARAM_FRAME , 1);
	data->SetInt32(PARAM_FRAMEOFFSET , 0);
	data->SetFloat(PARAM_FILTER , 1);
	data->SetFloat(PARAM_BLUR , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlTexture(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLTEXTURE, "Texture"_s, 0, DlTexture::Alloc, "dlTexture"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

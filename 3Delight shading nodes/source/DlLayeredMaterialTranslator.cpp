#include "DlLayeredMaterialTranslator.h"
#include "DlLayeredMaterial.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlLayeredMaterialTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlLayeredMaterial.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void DlLayeredMaterialTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlLayeredMaterialTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		f = (float) data->GetFloat(PARAM_TOP_MASK);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("top_mask", f));

		f = (float) data->GetFloat(PARAM_MIDDLE_MASK);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("middle_mask", f));

		f = (float) data->GetFloat(PARAM_BOTTOM_MASK);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("bottom_mask", f));

}

void  DlLayeredMaterialTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLLAYEREDMATERIAL, PARAM_I_COLOR, "i_color");
	PM->RegisterAttributeName(ID_DLLAYEREDMATERIAL, PARAM_TOP_MASK, "top_mask");
	PM->RegisterAttributeName(ID_DLLAYEREDMATERIAL, PARAM_MIDDLE_LAYER, "middle_layer");
	PM->RegisterAttributeName(ID_DLLAYEREDMATERIAL, PARAM_MIDDLE_MASK, "middle_mask");
	PM->RegisterAttributeName(ID_DLLAYEREDMATERIAL, PARAM_BOTTOM_LAYER, "bottom_layer");
	PM->RegisterAttributeName(ID_DLLAYEREDMATERIAL, PARAM_BOTTOM_MASK, "bottom_mask");
	PM->RegisterAttributeName(ID_DLLAYEREDMATERIAL, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLLAYEREDMATERIAL, PARAM_OUTCOLOR, "outColor");
}

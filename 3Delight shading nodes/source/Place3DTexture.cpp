
#include "c4d.h"
#include "Place3DTexture.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class Place3DTexture : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(Place3DTexture);
	}

	virtual Bool Message(GeListNode *node, Int32 type, void *data);
	virtual Bool iCreateOperator(GvNode * 	bn);
private:
	UInt32 dirtycache;
};


Bool Place3DTexture::Message(GeListNode *node, Int32 type, void *data) {
	{
		GvNode* gvnode = (GvNode*)node;
		BaseContainer* nodedata = gvnode->GetOpContainerInstance();
		BaseObject* widget = (BaseObject*)nodedata->GetLink(WIDGET, node->GetDocument(), Obase);
		if (widget) {
			UInt32 widget_dirty = widget->GetDirty(DIRTYFLAGS::ALL);
			if (widget_dirty != dirtycache) {
				dirtycache = widget_dirty;
				node->SetDirty(DIRTYFLAGS::DATA);
			}

		}
	}
	if (type == MSG_DESCRIPTION_COMMAND) {
		DescriptionCommand* dc = (DescriptionCommand*)data;
		if (dc->_descId[0].id == CREATE_WIDGET) {
			BaseDocument* doc = node->GetDocument();
			if (doc) {
				BaseObject* const widget = BaseObject::Alloc(ID_PLACE3DTEXTUREWIDGET);
				if (widget) {
					doc->InsertObject(widget, nullptr, nullptr);
					GvNode* gvnode = (GvNode*)node;
					BaseContainer* nodedata = gvnode->GetOpContainerInstance();
					nodedata->SetLink(WIDGET, widget);
					EventAdd();
				}
			}
		}
	}
	return true;
}

Bool Place3DTexture::iCreateOperator(GvNode * 	bn) {

	BaseContainer* data = bn->GetOpContainerInstance();
	data->SetInt32(MODE, MODE_NORMAL);
	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterPlace3DTexture(const char* icon = "dl_200.tif", int node_group = ID_GV_OPGROUP_TYPE_GENERAL) {
	return GvRegisterOperatorPlugin(ID_PLACE3DTEXTURE, "Place3DTexture"_s, 0, Place3DTexture::Alloc, "Place3DTexture"_s, 0, DL_NODES_CLASS, node_group, 0, AutoBitmap(String(icon)));
}

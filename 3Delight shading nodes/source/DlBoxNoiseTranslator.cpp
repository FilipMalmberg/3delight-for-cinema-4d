#include "DlBoxNoiseTranslator.h"
#include "DlBoxNoise.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlBoxNoiseTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlBoxNoise.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("3dlfc4d::default_placement_matrix", "out", shader_handle, "placementMatrix" );
}

void DlBoxNoiseTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlBoxNoiseTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		f = (float) data->GetFloat(PARAM_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("scale", f));

		f = (float) data->GetFloat(PARAM_SMOOTHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("smoothness", f));

		f = (float) data->GetFloat(PARAM_I_TIME);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("i_time", f));

		{
			Int32 option = data->GetInt32(PARAM_SPACE);
			if(option == PARAM_SPACE_WORLD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 0));
			}
			else if(option == PARAM_SPACE_OBJECT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 1));
			}
		}

		{
			Gradient* gradient = (Gradient*)data->GetCustomDataType(PARAM_COLOR_COLORVALUE, CUSTOMDATATYPE_GRADIENT );

			int knot_count;
			std::vector<float> knots;
			std::vector<float> colors;
			std::vector<int> interpolations;
			FillColorRampNSIData(gradient, doc, knots, colors, interpolations, knot_count);

			NSI::Argument arg_knot_color("color_ColorValue");
			arg_knot_color.SetArrayType(NSITypeColor, knot_count);
			arg_knot_color.SetValuePointer((void*)&colors[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_color));

			NSI::Argument arg_knot_position("color_Position");
			arg_knot_position.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_position.SetValuePointer((void*)&knots[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_position));

			NSI::Argument arg_knot_interpolation("color_Interp");
			arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);
			arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_interpolation));

		}

		{
			Int32 option = data->GetInt32(PARAM_COLOR_BLEND_MODE);
			if(option == PARAM_COLOR_BLEND_MODE_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_blend_mode", 0));
			}
			else if(option == PARAM_COLOR_BLEND_MODE_LIGHTEN__MAX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_blend_mode", 1));
			}
			else if(option == PARAM_COLOR_BLEND_MODE_MULTIPLY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_blend_mode", 2));
			}
			else if(option == PARAM_COLOR_BLEND_MODE_DARKEN__MIN_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_blend_mode", 3));
			}
			else if(option == PARAM_COLOR_BLEND_MODE_OVERLAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_blend_mode", 4));
			}
			else if(option == PARAM_COLOR_BLEND_MODE_DIFFERENCE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_blend_mode", 5));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_DISTORTION), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("distortion", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DISTORTION_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("distortion_intensity", f));

		{
			int value = (int)data->GetInt32(PARAM_LAYERS);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("layers", value));
		}

		f = (float) data->GetFloat(PARAM_LAYER_PERSISTENCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layer_persistence", f));

		f = (float) data->GetFloat(PARAM_LAYER_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layer_scale", f));

		f = (float) data->GetFloat(PARAM_AMPLITUDE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("amplitude", f));

		f = (float) data->GetFloat(PARAM_CONTRAST);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("contrast", f));

		{
			int value = (int)data->GetBool(PARAM_INVERT);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("invert", value));
		}

	{
		Matrix m = data->GetMatrix(PARAM_PLACEMENTMATRIX);
		std::vector<float> mdata = MatrixToNSIMatrix(m);
		NSI::Argument matrix_arg("placementMatrix");
		matrix_arg.SetType(NSITypeMatrix);
		matrix_arg.SetValuePointer((void*)&mdata[0]);
		ctx.SetAttribute(shader_handle, matrix_arg);
	}

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTALPHA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outAlpha", f));

}

void  DlBoxNoiseTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_SCALE, "scale");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_SMOOTHNESS, "smoothness");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_I_TIME, "i_time");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_SPACE, "space");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_COLOR_POSITION, "color_Position");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_COLOR_COLORVALUE, "color_ColorValue");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_COLOR_INTERP, "color_Interp");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_COLOR_BLEND_MODE, "color_blend_mode");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_DISTORTION, "distortion");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_DISTORTION_INTENSITY, "distortion_intensity");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_LAYERS, "layers");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_LAYER_PERSISTENCE, "layer_persistence");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_LAYER_SCALE, "layer_scale");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_AMPLITUDE, "amplitude");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_CONTRAST, "contrast");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_INVERT, "invert");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_PLACEMENTMATRIX, "placementMatrix");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DLBOXNOISE, PARAM_OUTALPHA, "outAlpha");
}

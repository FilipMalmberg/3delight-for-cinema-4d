#include "DlConstantTranslator.h"
#include "DlConstant.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlConstantTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlConstant.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void DlConstantTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlConstantTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

	{
		v = toLinear(data->GetVector(PARAM_I_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("i_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("intensity", f));

		f = (float) data->GetFloat(PARAM_EXPOSURE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("exposure", f));

		f = (float) data->GetFloat(PARAM_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("opacity", f));

		{
			int value = (int)data->GetBool(PARAM_OPACITY_CUTS_COLOR);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("opacity_cuts_color", value));
		}

		{
			int value = (int)data->GetBool(PARAM_CASTS_SHADOWS);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("casts_shadows", value));
		}

		{
			int value = (int)data->GetBool(PARAM_DOUBLESIDED);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("doublesided", value));
		}

		f = (float) data->GetFloat(PARAM_SPREAD);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("spread", f));

		f = (float) data->GetFloat(PARAM_DIFFUSE_CONTRIBUTION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("diffuse_contribution", f));

		f = (float) data->GetFloat(PARAM_REFLECTION_CONTRIBUTION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("reflection_contribution", f));

		f = (float) data->GetFloat(PARAM_REFRACTION_CONTRIBUTION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("refraction_contribution", f));

		f = (float) data->GetFloat(PARAM_VOLUME_CONTRIBUTION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("volume_contribution", f));

	{
		v = toLinear(data->GetVector(PARAM_INCANDESCENCE_MULTIPLIER), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("incandescence_multiplier", &v_data[0]));
	}

}

void  DlConstantTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_I_COLOR, "i_color");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_INTENSITY, "intensity");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_EXPOSURE, "exposure");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_OPACITY, "opacity");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_OPACITY_CUTS_COLOR, "opacity_cuts_color");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_CASTS_SHADOWS, "casts_shadows");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_DOUBLESIDED, "doublesided");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_SPREAD, "spread");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_DIFFUSE_CONTRIBUTION, "diffuse_contribution");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_REFLECTION_CONTRIBUTION, "reflection_contribution");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_REFRACTION_CONTRIBUTION, "refraction_contribution");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_VOLUME_CONTRIBUTION, "volume_contribution");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_INCANDESCENCE_MULTIPLIER, "incandescence_multiplier");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLCONSTANT, PARAM_OUTCOLOR, "outColor");
}

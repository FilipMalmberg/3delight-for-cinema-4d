#include "DlGlassTranslator.h"
#include "DlGlass.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlGlassTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlGlass.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord" );
}

void DlGlassTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlGlassTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

	{
		v = toLinear(data->GetVector(PARAM_REFLECT_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("reflect_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_REFLECT_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("reflect_roughness", f));

		f = (float) data->GetFloat(PARAM_REFLECT_IOR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("reflect_ior", f));

		{
			int value = (int)data->GetBool(PARAM_THIN_FILM);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("thin_film", value));
		}

		f = (float) data->GetFloat(PARAM_FILM_THICKNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("film_thickness", f));

		f = (float) data->GetFloat(PARAM_FILM_IOR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("film_ior", f));

	{
		v = toLinear(data->GetVector(PARAM_I_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("i_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_REFRACT_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("refract_roughness", f));

		f = (float) data->GetFloat(PARAM_REFRACT_IOR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("refract_ior", f));

		{
			int value = (int)data->GetBool(PARAM_TRANSMIT_AOVS);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("transmit_aovs", value));
		}

		{
			int value = (int)data->GetBool(PARAM_VOLUMETRIC_ENABLE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("volumetric_enable", value));
		}

		f = (float) data->GetFloat(PARAM_VOLUMETRIC_DENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("volumetric_density", f));

	{
		v = toLinear(data->GetVector(PARAM_VOLUMETRIC_SCATTERING_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("volumetric_scattering_color", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_VOLUMETRIC_TRANSPARENCY_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("volumetric_transparency_color", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_INCANDESCENCE), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("incandescence", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_INCANDESCENCE_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("incandescence_intensity", f));

	{
		v = toLinear(data->GetVector(PARAM_INCANDESCENCE_MULTIPLIER), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("incandescence_multiplier", &v_data[0]));
	}

		{
			Int32 option = data->GetInt32(PARAM_DISP_NORMAL_BUMP_TYPE);
			if(option == PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 0));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_NORMAL_MAP__DIRECTX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 1));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_NORMAL_MAP__OPENGL_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 2));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_DISPLACEMENT__0_0_CENTERED_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 3));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_DISPLACEMENT__0_5_CENTERED_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 4));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_DISP_NORMAL_BUMP_VALUE), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("disp_normal_bump_value", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("disp_normal_bump_intensity", f));

		f = (float) data->GetFloat(PARAM_OCCLUSION_DISTANCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("occlusion_distance", f));

}

void  DlGlassTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_REFLECT_COLOR, "reflect_color");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_REFLECT_ROUGHNESS, "reflect_roughness");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_REFLECT_IOR, "reflect_ior");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_THIN_FILM, "thin_film");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_FILM_THICKNESS, "film_thickness");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_FILM_IOR, "film_ior");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_I_COLOR, "i_color");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_REFRACT_ROUGHNESS, "refract_roughness");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_REFRACT_IOR, "refract_ior");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_TRANSMIT_AOVS, "transmit_aovs");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_VOLUMETRIC_ENABLE, "volumetric_enable");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_VOLUMETRIC_DENSITY, "volumetric_density");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_VOLUMETRIC_SCATTERING_COLOR, "volumetric_scattering_color");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_VOLUMETRIC_TRANSPARENCY_COLOR, "volumetric_transparency_color");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_INCANDESCENCE, "incandescence");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_INCANDESCENCE_INTENSITY, "incandescence_intensity");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_INCANDESCENCE_MULTIPLIER, "incandescence_multiplier");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_DISP_NORMAL_BUMP_TYPE, "disp_normal_bump_type");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_DISP_NORMAL_BUMP_VALUE, "disp_normal_bump_value");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_DISP_NORMAL_BUMP_INTENSITY, "disp_normal_bump_intensity");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_OCCLUSION_DISTANCE, "occlusion_distance");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_UVCOORD, "uvCoord");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLGLASS, PARAM_OUTCOLOR, "outColor");
}

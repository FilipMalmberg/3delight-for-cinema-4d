#include "DlColorBlendMultiTranslator.h"
#include "DlColorBlendMulti.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlColorBlendMultiTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlColorBlendMulti.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void DlColorBlendMultiTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlColorBlendMultiTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			int value = (int)data->GetBool(PARAM_ENABLE_1);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("enable_1", value));
		}

		{
			String s = data->GetString(PARAM_NAME_1);
			ctx.SetAttribute(shader_handle, NSI::StringArg("name_1", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_BLENDMODE_1);
			if(option == PARAM_BLENDMODE_1_OVER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 0));
			}
			else if(option == PARAM_BLENDMODE_1_MULTIPLY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 1));
			}
			else if(option == PARAM_BLENDMODE_1_DARKEN__MIN_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 2));
			}
			else if(option == PARAM_BLENDMODE_1_COLOR_BURN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 3));
			}
			else if(option == PARAM_BLENDMODE_1_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 4));
			}
			else if(option == PARAM_BLENDMODE_1_SCREEN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 5));
			}
			else if(option == PARAM_BLENDMODE_1_LIGHTEN__MAX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 6));
			}
			else if(option == PARAM_BLENDMODE_1_COLOR_DODGE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 7));
			}
			else if(option == PARAM_BLENDMODE_1_OVERLAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 8));
			}
			else if(option == PARAM_BLENDMODE_1_SOFT_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 9));
			}
			else if(option == PARAM_BLENDMODE_1_HARD_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 10));
			}
			else if(option == PARAM_BLENDMODE_1_ADD_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 11));
			}
			else if(option == PARAM_BLENDMODE_1_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 12));
			}
			else if(option == PARAM_BLENDMODE_1_DIFFERENCE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 13));
			}
			else if(option == PARAM_BLENDMODE_1_DIVIDE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 14));
			}
			else if(option == PARAM_BLENDMODE_1_HUE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 15));
			}
			else if(option == PARAM_BLENDMODE_1_SATURATION){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 16));
			}
			else if(option == PARAM_BLENDMODE_1_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 17));
			}
			else if(option == PARAM_BLENDMODE_1_LUMINOSITY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_1", 18));
			}
		}

		f = (float) data->GetFloat(PARAM_BLEND_1);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_1", f));

	{
		v = toLinear(data->GetVector(PARAM_INPUT_1), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("input_1", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_ENABLE_2);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("enable_2", value));
		}

		{
			String s = data->GetString(PARAM_NAME_2);
			ctx.SetAttribute(shader_handle, NSI::StringArg("name_2", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_BLENDMODE_2);
			if(option == PARAM_BLENDMODE_2_OVER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 0));
			}
			else if(option == PARAM_BLENDMODE_2_MULTIPLY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 1));
			}
			else if(option == PARAM_BLENDMODE_2_DARKEN__MIN_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 2));
			}
			else if(option == PARAM_BLENDMODE_2_COLOR_BURN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 3));
			}
			else if(option == PARAM_BLENDMODE_2_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 4));
			}
			else if(option == PARAM_BLENDMODE_2_SCREEN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 5));
			}
			else if(option == PARAM_BLENDMODE_2_LIGHTEN__MAX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 6));
			}
			else if(option == PARAM_BLENDMODE_2_COLOR_DODGE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 7));
			}
			else if(option == PARAM_BLENDMODE_2_OVERLAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 8));
			}
			else if(option == PARAM_BLENDMODE_2_SOFT_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 9));
			}
			else if(option == PARAM_BLENDMODE_2_HARD_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 10));
			}
			else if(option == PARAM_BLENDMODE_2_ADD_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 11));
			}
			else if(option == PARAM_BLENDMODE_2_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 12));
			}
			else if(option == PARAM_BLENDMODE_2_DIFFERENCE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 13));
			}
			else if(option == PARAM_BLENDMODE_2_DIVIDE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 14));
			}
			else if(option == PARAM_BLENDMODE_2_HUE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 15));
			}
			else if(option == PARAM_BLENDMODE_2_SATURATION){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 16));
			}
			else if(option == PARAM_BLENDMODE_2_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 17));
			}
			else if(option == PARAM_BLENDMODE_2_LUMINOSITY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_2", 18));
			}
		}

		f = (float) data->GetFloat(PARAM_BLEND_2);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_2", f));

	{
		v = toLinear(data->GetVector(PARAM_INPUT_2), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("input_2", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_ENABLE_3);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("enable_3", value));
		}

		{
			String s = data->GetString(PARAM_NAME_3);
			ctx.SetAttribute(shader_handle, NSI::StringArg("name_3", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_BLENDMODE_3);
			if(option == PARAM_BLENDMODE_3_OVER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 0));
			}
			else if(option == PARAM_BLENDMODE_3_MULTIPLY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 1));
			}
			else if(option == PARAM_BLENDMODE_3_DARKEN__MIN_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 2));
			}
			else if(option == PARAM_BLENDMODE_3_COLOR_BURN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 3));
			}
			else if(option == PARAM_BLENDMODE_3_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 4));
			}
			else if(option == PARAM_BLENDMODE_3_SCREEN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 5));
			}
			else if(option == PARAM_BLENDMODE_3_LIGHTEN__MAX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 6));
			}
			else if(option == PARAM_BLENDMODE_3_COLOR_DODGE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 7));
			}
			else if(option == PARAM_BLENDMODE_3_OVERLAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 8));
			}
			else if(option == PARAM_BLENDMODE_3_SOFT_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 9));
			}
			else if(option == PARAM_BLENDMODE_3_HARD_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 10));
			}
			else if(option == PARAM_BLENDMODE_3_ADD_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 11));
			}
			else if(option == PARAM_BLENDMODE_3_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 12));
			}
			else if(option == PARAM_BLENDMODE_3_DIFFERENCE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 13));
			}
			else if(option == PARAM_BLENDMODE_3_DIVIDE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 14));
			}
			else if(option == PARAM_BLENDMODE_3_HUE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 15));
			}
			else if(option == PARAM_BLENDMODE_3_SATURATION){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 16));
			}
			else if(option == PARAM_BLENDMODE_3_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 17));
			}
			else if(option == PARAM_BLENDMODE_3_LUMINOSITY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_3", 18));
			}
		}

		f = (float) data->GetFloat(PARAM_BLEND_3);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_3", f));

	{
		v = toLinear(data->GetVector(PARAM_INPUT_3), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("input_3", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_ENABLE_4);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("enable_4", value));
		}

		{
			String s = data->GetString(PARAM_NAME_4);
			ctx.SetAttribute(shader_handle, NSI::StringArg("name_4", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_BLENDMODE_4);
			if(option == PARAM_BLENDMODE_4_OVER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 0));
			}
			else if(option == PARAM_BLENDMODE_4_MULTIPLY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 1));
			}
			else if(option == PARAM_BLENDMODE_4_DARKEN__MIN_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 2));
			}
			else if(option == PARAM_BLENDMODE_4_COLOR_BURN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 3));
			}
			else if(option == PARAM_BLENDMODE_4_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 4));
			}
			else if(option == PARAM_BLENDMODE_4_SCREEN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 5));
			}
			else if(option == PARAM_BLENDMODE_4_LIGHTEN__MAX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 6));
			}
			else if(option == PARAM_BLENDMODE_4_COLOR_DODGE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 7));
			}
			else if(option == PARAM_BLENDMODE_4_OVERLAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 8));
			}
			else if(option == PARAM_BLENDMODE_4_SOFT_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 9));
			}
			else if(option == PARAM_BLENDMODE_4_HARD_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 10));
			}
			else if(option == PARAM_BLENDMODE_4_ADD_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 11));
			}
			else if(option == PARAM_BLENDMODE_4_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 12));
			}
			else if(option == PARAM_BLENDMODE_4_DIFFERENCE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 13));
			}
			else if(option == PARAM_BLENDMODE_4_DIVIDE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 14));
			}
			else if(option == PARAM_BLENDMODE_4_HUE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 15));
			}
			else if(option == PARAM_BLENDMODE_4_SATURATION){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 16));
			}
			else if(option == PARAM_BLENDMODE_4_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 17));
			}
			else if(option == PARAM_BLENDMODE_4_LUMINOSITY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_4", 18));
			}
		}

		f = (float) data->GetFloat(PARAM_BLEND_4);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_4", f));

	{
		v = toLinear(data->GetVector(PARAM_INPUT_4), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("input_4", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_ENABLE_5);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("enable_5", value));
		}

		{
			String s = data->GetString(PARAM_NAME_5);
			ctx.SetAttribute(shader_handle, NSI::StringArg("name_5", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_BLENDMODE_5);
			if(option == PARAM_BLENDMODE_5_OVER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 0));
			}
			else if(option == PARAM_BLENDMODE_5_MULTIPLY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 1));
			}
			else if(option == PARAM_BLENDMODE_5_DARKEN__MIN_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 2));
			}
			else if(option == PARAM_BLENDMODE_5_COLOR_BURN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 3));
			}
			else if(option == PARAM_BLENDMODE_5_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 4));
			}
			else if(option == PARAM_BLENDMODE_5_SCREEN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 5));
			}
			else if(option == PARAM_BLENDMODE_5_LIGHTEN__MAX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 6));
			}
			else if(option == PARAM_BLENDMODE_5_COLOR_DODGE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 7));
			}
			else if(option == PARAM_BLENDMODE_5_OVERLAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 8));
			}
			else if(option == PARAM_BLENDMODE_5_SOFT_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 9));
			}
			else if(option == PARAM_BLENDMODE_5_HARD_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 10));
			}
			else if(option == PARAM_BLENDMODE_5_ADD_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 11));
			}
			else if(option == PARAM_BLENDMODE_5_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 12));
			}
			else if(option == PARAM_BLENDMODE_5_DIFFERENCE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 13));
			}
			else if(option == PARAM_BLENDMODE_5_DIVIDE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 14));
			}
			else if(option == PARAM_BLENDMODE_5_HUE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 15));
			}
			else if(option == PARAM_BLENDMODE_5_SATURATION){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 16));
			}
			else if(option == PARAM_BLENDMODE_5_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 17));
			}
			else if(option == PARAM_BLENDMODE_5_LUMINOSITY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_5", 18));
			}
		}

		f = (float) data->GetFloat(PARAM_BLEND_5);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_5", f));

	{
		v = toLinear(data->GetVector(PARAM_INPUT_5), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("input_5", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_ENABLE_6);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("enable_6", value));
		}

		{
			String s = data->GetString(PARAM_NAME_6);
			ctx.SetAttribute(shader_handle, NSI::StringArg("name_6", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_BLENDMODE_6);
			if(option == PARAM_BLENDMODE_6_OVER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 0));
			}
			else if(option == PARAM_BLENDMODE_6_MULTIPLY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 1));
			}
			else if(option == PARAM_BLENDMODE_6_DARKEN__MIN_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 2));
			}
			else if(option == PARAM_BLENDMODE_6_COLOR_BURN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 3));
			}
			else if(option == PARAM_BLENDMODE_6_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 4));
			}
			else if(option == PARAM_BLENDMODE_6_SCREEN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 5));
			}
			else if(option == PARAM_BLENDMODE_6_LIGHTEN__MAX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 6));
			}
			else if(option == PARAM_BLENDMODE_6_COLOR_DODGE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 7));
			}
			else if(option == PARAM_BLENDMODE_6_OVERLAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 8));
			}
			else if(option == PARAM_BLENDMODE_6_SOFT_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 9));
			}
			else if(option == PARAM_BLENDMODE_6_HARD_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 10));
			}
			else if(option == PARAM_BLENDMODE_6_ADD_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 11));
			}
			else if(option == PARAM_BLENDMODE_6_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 12));
			}
			else if(option == PARAM_BLENDMODE_6_DIFFERENCE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 13));
			}
			else if(option == PARAM_BLENDMODE_6_DIVIDE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 14));
			}
			else if(option == PARAM_BLENDMODE_6_HUE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 15));
			}
			else if(option == PARAM_BLENDMODE_6_SATURATION){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 16));
			}
			else if(option == PARAM_BLENDMODE_6_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 17));
			}
			else if(option == PARAM_BLENDMODE_6_LUMINOSITY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_6", 18));
			}
		}

		f = (float) data->GetFloat(PARAM_BLEND_6);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_6", f));

	{
		v = toLinear(data->GetVector(PARAM_INPUT_6), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("input_6", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_ENABLE_7);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("enable_7", value));
		}

		{
			String s = data->GetString(PARAM_NAME_7);
			ctx.SetAttribute(shader_handle, NSI::StringArg("name_7", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_BLENDMODE_7);
			if(option == PARAM_BLENDMODE_7_OVER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 0));
			}
			else if(option == PARAM_BLENDMODE_7_MULTIPLY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 1));
			}
			else if(option == PARAM_BLENDMODE_7_DARKEN__MIN_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 2));
			}
			else if(option == PARAM_BLENDMODE_7_COLOR_BURN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 3));
			}
			else if(option == PARAM_BLENDMODE_7_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 4));
			}
			else if(option == PARAM_BLENDMODE_7_SCREEN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 5));
			}
			else if(option == PARAM_BLENDMODE_7_LIGHTEN__MAX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 6));
			}
			else if(option == PARAM_BLENDMODE_7_COLOR_DODGE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 7));
			}
			else if(option == PARAM_BLENDMODE_7_OVERLAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 8));
			}
			else if(option == PARAM_BLENDMODE_7_SOFT_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 9));
			}
			else if(option == PARAM_BLENDMODE_7_HARD_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 10));
			}
			else if(option == PARAM_BLENDMODE_7_ADD_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 11));
			}
			else if(option == PARAM_BLENDMODE_7_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 12));
			}
			else if(option == PARAM_BLENDMODE_7_DIFFERENCE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 13));
			}
			else if(option == PARAM_BLENDMODE_7_DIVIDE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 14));
			}
			else if(option == PARAM_BLENDMODE_7_HUE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 15));
			}
			else if(option == PARAM_BLENDMODE_7_SATURATION){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 16));
			}
			else if(option == PARAM_BLENDMODE_7_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 17));
			}
			else if(option == PARAM_BLENDMODE_7_LUMINOSITY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_7", 18));
			}
		}

		f = (float) data->GetFloat(PARAM_BLEND_7);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_7", f));

	{
		v = toLinear(data->GetVector(PARAM_INPUT_7), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("input_7", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_ENABLE_8);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("enable_8", value));
		}

		{
			String s = data->GetString(PARAM_NAME_8);
			ctx.SetAttribute(shader_handle, NSI::StringArg("name_8", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_BLENDMODE_8);
			if(option == PARAM_BLENDMODE_8_OVER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 0));
			}
			else if(option == PARAM_BLENDMODE_8_MULTIPLY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 1));
			}
			else if(option == PARAM_BLENDMODE_8_DARKEN__MIN_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 2));
			}
			else if(option == PARAM_BLENDMODE_8_COLOR_BURN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 3));
			}
			else if(option == PARAM_BLENDMODE_8_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 4));
			}
			else if(option == PARAM_BLENDMODE_8_SCREEN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 5));
			}
			else if(option == PARAM_BLENDMODE_8_LIGHTEN__MAX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 6));
			}
			else if(option == PARAM_BLENDMODE_8_COLOR_DODGE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 7));
			}
			else if(option == PARAM_BLENDMODE_8_OVERLAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 8));
			}
			else if(option == PARAM_BLENDMODE_8_SOFT_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 9));
			}
			else if(option == PARAM_BLENDMODE_8_HARD_LIGHT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 10));
			}
			else if(option == PARAM_BLENDMODE_8_ADD_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 11));
			}
			else if(option == PARAM_BLENDMODE_8_SUBSTRACT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 12));
			}
			else if(option == PARAM_BLENDMODE_8_DIFFERENCE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 13));
			}
			else if(option == PARAM_BLENDMODE_8_DIVIDE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 14));
			}
			else if(option == PARAM_BLENDMODE_8_HUE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 15));
			}
			else if(option == PARAM_BLENDMODE_8_SATURATION){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 16));
			}
			else if(option == PARAM_BLENDMODE_8_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 17));
			}
			else if(option == PARAM_BLENDMODE_8_LUMINOSITY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("blendMode_8", 18));
			}
		}

		f = (float) data->GetFloat(PARAM_BLEND_8);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_8", f));

	{
		v = toLinear(data->GetVector(PARAM_INPUT_8), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("input_8", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM__CLAMP);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("_clamp", value));
		}

		{
			int value = (int)data->GetBool(PARAM_INVERT);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("invert", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTCOLORR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorR", f));

		f = (float) data->GetFloat(PARAM_OUTCOLORG);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorG", f));

		f = (float) data->GetFloat(PARAM_OUTCOLORB);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorB", f));

}

void  DlColorBlendMultiTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_ENABLE_1, "enable_1");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_NAME_1, "name_1");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLENDMODE_1, "blendMode_1");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLEND_1, "blend_1");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_INPUT_1, "input_1");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_ENABLE_2, "enable_2");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_NAME_2, "name_2");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLENDMODE_2, "blendMode_2");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLEND_2, "blend_2");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_INPUT_2, "input_2");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_ENABLE_3, "enable_3");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_NAME_3, "name_3");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLENDMODE_3, "blendMode_3");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLEND_3, "blend_3");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_INPUT_3, "input_3");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_ENABLE_4, "enable_4");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_NAME_4, "name_4");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLENDMODE_4, "blendMode_4");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLEND_4, "blend_4");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_INPUT_4, "input_4");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_ENABLE_5, "enable_5");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_NAME_5, "name_5");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLENDMODE_5, "blendMode_5");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLEND_5, "blend_5");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_INPUT_5, "input_5");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_ENABLE_6, "enable_6");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_NAME_6, "name_6");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLENDMODE_6, "blendMode_6");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLEND_6, "blend_6");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_INPUT_6, "input_6");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_ENABLE_7, "enable_7");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_NAME_7, "name_7");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLENDMODE_7, "blendMode_7");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLEND_7, "blend_7");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_INPUT_7, "input_7");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_ENABLE_8, "enable_8");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_NAME_8, "name_8");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLENDMODE_8, "blendMode_8");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_BLEND_8, "blend_8");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_INPUT_8, "input_8");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM__CLAMP, "_clamp");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_INVERT, "invert");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_OUTCOLORR, "outColorR");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_OUTCOLORG, "outColorG");
	PM->RegisterAttributeName(ID_DLCOLORBLENDMULTI, PARAM_OUTCOLORB, "outColorB");
}

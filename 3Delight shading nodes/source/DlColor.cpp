//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlColor.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlColor : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlColor); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlColor::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_INCOLOR , Vector(0, 0, 0));

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlColor(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLCOLOR, "Color"_s, 0, DlColor::Alloc, "dlColor"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

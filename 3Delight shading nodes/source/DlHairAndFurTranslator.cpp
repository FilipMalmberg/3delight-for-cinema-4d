#include "DlHairAndFurTranslator.h"
#include "DlHairAndFur.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlHairAndFurTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlHairAndFur.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void DlHairAndFurTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlHairAndFurTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		f = (float) data->GetFloat(PARAM_EUMELANINE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("eumelanine", f));

		f = (float) data->GetFloat(PARAM_PHENOMELANINE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("phenomelanine", f));

	{
		v = toLinear(data->GetVector(PARAM_I_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("i_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DYE_WEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("dye_weight", f));

		f = (float) data->GetFloat(PARAM_SPECULAR_LEVEL);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("specular_level", f));

		f = (float) data->GetFloat(PARAM_LONGITUDINAL_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("longitudinal_roughness", f));

		f = (float) data->GetFloat(PARAM_AZIMUTHAL_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("azimuthal_roughness", f));

		f = (float) data->GetFloat(PARAM_MEDULLA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("medulla", f));

		f = (float) data->GetFloat(PARAM_SYNTHETIC);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("synthetic", f));

		f = (float) data->GetFloat(PARAM_VARIATION_MELANIN);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_melanin", f));

		f = (float) data->GetFloat(PARAM_VARIATION_MELANIN_RED);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_melanin_red", f));

		f = (float) data->GetFloat(PARAM_VARIATION_WHITE_HAIR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_white_hair", f));

		f = (float) data->GetFloat(PARAM_VARIATION_DYE_HUE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_dye_hue", f));

		f = (float) data->GetFloat(PARAM_VARIATION_DYE_SATURATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_dye_saturation", f));

		f = (float) data->GetFloat(PARAM_VARIATION_DYE_VALUE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_dye_value", f));

		f = (float) data->GetFloat(PARAM_VARIATION_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_roughness", f));

		f = (float) data->GetFloat(PARAM_VARIATION_SPECULAR_LEVEL);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_specular_level", f));

		f = (float) data->GetFloat(PARAM_BOOST_GLOSSINESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("boost_glossiness", f));

		f = (float) data->GetFloat(PARAM_BOOST_SPECULAR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("boost_specular", f));

		f = (float) data->GetFloat(PARAM_BOOST_TRANSMISSION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("boost_transmission", f));

		f = (float) data->GetFloat(PARAM_OCCLUSION_DISTANCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("occlusion_distance", f));

}

void  DlHairAndFurTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_EUMELANINE, "eumelanine");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_PHENOMELANINE, "phenomelanine");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_I_COLOR, "i_color");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_DYE_WEIGHT, "dye_weight");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_SPECULAR_LEVEL, "specular_level");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_LONGITUDINAL_ROUGHNESS, "longitudinal_roughness");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_AZIMUTHAL_ROUGHNESS, "azimuthal_roughness");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_MEDULLA, "medulla");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_SYNTHETIC, "synthetic");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_VARIATION_MELANIN, "variation_melanin");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_VARIATION_MELANIN_RED, "variation_melanin_red");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_VARIATION_WHITE_HAIR, "variation_white_hair");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_VARIATION_DYE_HUE, "variation_dye_hue");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_VARIATION_DYE_SATURATION, "variation_dye_saturation");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_VARIATION_DYE_VALUE, "variation_dye_value");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_VARIATION_ROUGHNESS, "variation_roughness");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_VARIATION_SPECULAR_LEVEL, "variation_specular_level");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_BOOST_GLOSSINESS, "boost_glossiness");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_BOOST_SPECULAR, "boost_specular");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_BOOST_TRANSMISSION, "boost_transmission");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_OCCLUSION_DISTANCE, "occlusion_distance");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLHAIRANDFUR, PARAM_OUTCOLOR, "outColor");
}

//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlBoxNoise.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlBoxNoise : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlBoxNoise); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlBoxNoise::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetFloat(PARAM_SCALE , 1);
	data->SetFloat(PARAM_SMOOTHNESS , 0);
	data->SetFloat(PARAM_I_TIME , 0);
	data->SetInt32(PARAM_SPACE , PARAM_SPACE_WORLD);
	{
		AutoAlloc<Gradient> gradient;
		if (!gradient) return false;
		GradientKnot k;
		k.col = Vector(0, 0, 0);
		k.pos = 0;
		gradient->InsertKnot(k);
		k.col = Vector(1, 1, 1);
		k.pos = 1;
		gradient->InsertKnot(k);
		data->SetData(PARAM_COLOR_COLORVALUE, GeData(CUSTOMDATATYPE_GRADIENT, *gradient));
	}
	data->SetInt32(PARAM_COLOR_BLEND_MODE , PARAM_COLOR_BLEND_MODE_ADD);
	data->SetVector(PARAM_DISTORTION , Vector(0, 0, 0));
	data->SetFloat(PARAM_DISTORTION_INTENSITY , 1);
	data->SetInt32(PARAM_LAYERS , 1);
	data->SetFloat(PARAM_LAYER_PERSISTENCE , 0.7);
	data->SetFloat(PARAM_LAYER_SCALE , 0.45);
	data->SetFloat(PARAM_AMPLITUDE , 1);
	data->SetFloat(PARAM_CONTRAST , 1);
	data->SetBool(PARAM_INVERT , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlBoxNoise(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLBOXNOISE, "Box Noise"_s, 0, DlBoxNoise::Alloc, "dlBoxNoise"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

#include "DlToonTranslator.h"
#include "DlToon.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlToonTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlToon.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord" );
}

void DlToonTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlToonTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

	{
		v = toLinear(data->GetVector(PARAM_COLOR_TINT), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("color_tint", &v_data[0]));
	}

		{
			Gradient* gradient = (Gradient*)data->GetCustomDataType(PARAM_COLOR_CURVE_COLORS, CUSTOMDATATYPE_GRADIENT );

			int knot_count;
			std::vector<float> knots;
			std::vector<float> colors;
			std::vector<int> interpolations;
			FillColorRampNSIData(gradient, doc, knots, colors, interpolations, knot_count);

			NSI::Argument arg_knot_color("color_curve_Colors");
			arg_knot_color.SetArrayType(NSITypeColor, knot_count);
			arg_knot_color.SetValuePointer((void*)&colors[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_color));

			NSI::Argument arg_knot_position("color_curve_Knots");
			arg_knot_position.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_position.SetValuePointer((void*)&knots[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_position));

			NSI::Argument arg_knot_interpolation("color_curve_Interpolation");
			arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);
			arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_interpolation));

		}

		f = (float) data->GetFloat(PARAM_DIFFUSE_WEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("diffuse_weight", f));

		f = (float) data->GetFloat(PARAM_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("opacity", f));

		f = (float) data->GetFloat(PARAM_SPECULAR_WEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("specular_weight", f));

	{
		v = toLinear(data->GetVector(PARAM_SPECULAR_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("specular_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_SPECULAR_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("specular_roughness", f));

		f = (float) data->GetFloat(PARAM_REFRACT_WEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("refract_weight", f));

	{
		v = toLinear(data->GetVector(PARAM_REFRACT_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("refract_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_REFRACT_IOR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("refract_ior", f));

		{
			int value = (int)data->GetBool(PARAM_OUTLINE_SILHOUETTES_ENABLE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_silhouettes_enable", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_OUTLINE_SILHOUETTES_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outline_silhouettes_color", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_OUTLINE_SILHOUETTES_TINT_COLOR);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_silhouettes_tint_color", value));
		}

		f = (float) data->GetFloat(PARAM_OUTLINE_SILHOUETTES_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_silhouettes_opacity", f));

		f = (float) data->GetFloat(PARAM_OUTLINE_SILHOUETTES_WIDTH);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_silhouettes_width", f));

		f = (float) data->GetFloat(PARAM_OUTLINE_SILHOUETTES_SENSITIVITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_silhouettes_sensitivity", f));

		{
			int value = (int)data->GetInt32(PARAM_OUTLINE_SILOUHETTES_PRIORITY);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_silouhettes_priority", value));
		}

		{
			int value = (int)data->GetBool(PARAM_OUTLINE_FOLDS_ENABLE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_folds_enable", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_OUTLINE_FOLDS_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outline_folds_color", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_OUTLINE_FOLDS_TINT_COLOR);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_folds_tint_color", value));
		}

		f = (float) data->GetFloat(PARAM_OUTLINE_FOLDS_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_folds_opacity", f));

		f = (float) data->GetFloat(PARAM_OUTLINE_FOLDS_WIDTH);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_folds_width", f));

		f = (float) data->GetFloat(PARAM_OUTLINE_FOLDS_SENSITIVITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_folds_sensitivity", f));

		{
			int value = (int)data->GetInt32(PARAM_OUTLINE_FOLDS_PRIORITY);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_folds_priority", value));
		}

		{
			int value = (int)data->GetBool(PARAM_OUTLINE_CREASES_ENABLE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_creases_enable", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_OUTLINE_CREASES_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outline_creases_color", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_OUTLINE_CREASES_TINT_COLOR);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_creases_tint_color", value));
		}

		f = (float) data->GetFloat(PARAM_OUTLINE_CREASES_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_creases_opacity", f));

		f = (float) data->GetFloat(PARAM_OUTLINE_CREASES_WIDTH);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_creases_width", f));

		{
			int value = (int)data->GetInt32(PARAM_OUTLINE_CREASES_PRIORITY);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_creases_priority", value));
		}

		{
			int value = (int)data->GetBool(PARAM_OUTLINE_OBJECTS_ENABLE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_objects_enable", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_OUTLINE_OBJECTS_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outline_objects_color", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_OUTLINE_OBJECTS_TINT_COLOR);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_objects_tint_color", value));
		}

		f = (float) data->GetFloat(PARAM_OUTLINE_OBJECTS_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_objects_opacity", f));

		f = (float) data->GetFloat(PARAM_OUTLINE_OBJECTS_WIDTH);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_objects_width", f));

		{
			int value = (int)data->GetInt32(PARAM_OUTLINE_OBJECTS_PRIORITY);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_objects_priority", value));
		}

		{
			int value = (int)data->GetBool(PARAM_OUTLINE_TEXTURE_ENABLE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_texture_enable", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_OUTLINE_TEXTURE_SOURCE), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outline_texture_source", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_OUTLINE_TEXTURE_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outline_texture_color", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_OUTLINE_TEXTURE_TINT_COLOR);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_texture_tint_color", value));
		}

		f = (float) data->GetFloat(PARAM_OUTLINE_TEXTURE_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_texture_opacity", f));

		f = (float) data->GetFloat(PARAM_OUTLINE_TEXTURE_WIDTH);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_texture_width", f));

		f = (float) data->GetFloat(PARAM_OUTLINE_TEXTURE_SENSITIVITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_texture_sensitivity", f));

		{
			int value = (int)data->GetInt32(PARAM_OUTLINE_TEXTURE_PRIORITY);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_texture_priority", value));
		}

		{
			int value = (int)data->GetInt32(PARAM_OUTLINE_ALPHA_ENABLE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_alpha_enable", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_OUTLINE_ALPHA_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outline_alpha_color", &v_data[0]));
	}

		{
			int value = (int)data->GetInt32(PARAM_OUTLINE_ALPHA_TINT_COLOR);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_alpha_tint_color", value));
		}

		f = (float) data->GetFloat(PARAM_OUTLINE_ALPHA_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_alpha_opacity", f));

		f = (float) data->GetFloat(PARAM_OUTLINE_ALPHA_WIDTH);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_alpha_width", f));

		f = (float) data->GetFloat(PARAM_OUTLINE_ALPHA_SENSITIVITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_alpha_sensitivity", f));

		{
			int value = (int)data->GetInt32(PARAM_OUTLINE_ALPHA_PRIORITY);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outline_alpha_priority", value));
		}

		f = (float) data->GetFloat(PARAM_OUTLINE_FADE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_fade", f));

		{
			int value = (int)data->GetBool(PARAM_ENABLE_BACKFACING_OUTLINES);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("enable_backfacing_outlines", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_INCANDESCENCE), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("incandescence", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_INCANDESCENCE_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("incandescence_intensity", f));

	{
		v = toLinear(data->GetVector(PARAM_INCANDESCENCE_MULTIPLIER), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("incandescence_multiplier", &v_data[0]));
	}

		{
			Int32 option = data->GetInt32(PARAM_DISP_NORMAL_BUMP_TYPE);
			if(option == PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 0));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_NORMAL_MAP__DIRECTX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 1));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_NORMAL_MAP__OPENGL_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 2));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_DISPLACEMENT__0_0_CENTERED_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 3));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_DISPLACEMENT__0_5_CENTERED_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 4));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_DISP_NORMAL_BUMP_VALUE), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("disp_normal_bump_value", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("disp_normal_bump_intensity", f));

		f = (float) data->GetFloat(PARAM_OCCLUSION_DISTANCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("occlusion_distance", f));

}

void  DlToonTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLTOON, PARAM_COLOR_TINT, "color_tint");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_COLOR_CURVE_COLORS, "color_curve_Colors");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_COLOR_CURVE_KNOTS, "color_curve_Knots");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_COLOR_CURVE_INTERPOLATION, "color_curve_Interpolation");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_DIFFUSE_WEIGHT, "diffuse_weight");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OPACITY, "opacity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_SPECULAR_WEIGHT, "specular_weight");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_SPECULAR_COLOR, "specular_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_SPECULAR_ROUGHNESS, "specular_roughness");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_REFRACT_WEIGHT, "refract_weight");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_REFRACT_COLOR, "refract_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_REFRACT_IOR, "refract_ior");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_SILHOUETTES_ENABLE, "outline_silhouettes_enable");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_SILHOUETTES_COLOR, "outline_silhouettes_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_SILHOUETTES_TINT_COLOR, "outline_silhouettes_tint_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_SILHOUETTES_OPACITY, "outline_silhouettes_opacity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_SILHOUETTES_WIDTH, "outline_silhouettes_width");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_SILHOUETTES_SENSITIVITY, "outline_silhouettes_sensitivity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_SILOUHETTES_PRIORITY, "outline_silouhettes_priority");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_FOLDS_ENABLE, "outline_folds_enable");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_FOLDS_COLOR, "outline_folds_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_FOLDS_TINT_COLOR, "outline_folds_tint_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_FOLDS_OPACITY, "outline_folds_opacity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_FOLDS_WIDTH, "outline_folds_width");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_FOLDS_SENSITIVITY, "outline_folds_sensitivity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_FOLDS_PRIORITY, "outline_folds_priority");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_CREASES_ENABLE, "outline_creases_enable");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_CREASES_COLOR, "outline_creases_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_CREASES_TINT_COLOR, "outline_creases_tint_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_CREASES_OPACITY, "outline_creases_opacity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_CREASES_WIDTH, "outline_creases_width");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_CREASES_PRIORITY, "outline_creases_priority");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_OBJECTS_ENABLE, "outline_objects_enable");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_OBJECTS_COLOR, "outline_objects_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_OBJECTS_TINT_COLOR, "outline_objects_tint_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_OBJECTS_OPACITY, "outline_objects_opacity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_OBJECTS_WIDTH, "outline_objects_width");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_OBJECTS_PRIORITY, "outline_objects_priority");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_TEXTURE_ENABLE, "outline_texture_enable");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_TEXTURE_SOURCE, "outline_texture_source");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_TEXTURE_COLOR, "outline_texture_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_TEXTURE_TINT_COLOR, "outline_texture_tint_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_TEXTURE_OPACITY, "outline_texture_opacity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_TEXTURE_WIDTH, "outline_texture_width");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_TEXTURE_SENSITIVITY, "outline_texture_sensitivity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_TEXTURE_PRIORITY, "outline_texture_priority");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_ALPHA_ENABLE, "outline_alpha_enable");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_ALPHA_COLOR, "outline_alpha_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_ALPHA_TINT_COLOR, "outline_alpha_tint_color");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_ALPHA_OPACITY, "outline_alpha_opacity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_ALPHA_WIDTH, "outline_alpha_width");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_ALPHA_SENSITIVITY, "outline_alpha_sensitivity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_ALPHA_PRIORITY, "outline_alpha_priority");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTLINE_FADE, "outline_fade");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_ENABLE_BACKFACING_OUTLINES, "enable_backfacing_outlines");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_INCANDESCENCE, "incandescence");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_INCANDESCENCE_INTENSITY, "incandescence_intensity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_INCANDESCENCE_MULTIPLIER, "incandescence_multiplier");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_DISP_NORMAL_BUMP_TYPE, "disp_normal_bump_type");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_DISP_NORMAL_BUMP_VALUE, "disp_normal_bump_value");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_DISP_NORMAL_BUMP_INTENSITY, "disp_normal_bump_intensity");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OCCLUSION_DISTANCE, "occlusion_distance");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_UVCOORD, "uvCoord");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_PHYSICAL_LAYER, "physical_layer");
	PM->RegisterAttributeName(ID_DLTOON, PARAM_OUTCOLOR, "outColor");
}

#include "DlTriplanarTranslator.h"
#include "DlTriplanar.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlTriplanarTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlTriplanar.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("3dlfc4d::default_placement_matrix", "out", shader_handle, "placementMatrix" );
}

void DlTriplanarTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlTriplanarTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			Int32 option = data->GetInt32(PARAM_MODE);
			if(option == PARAM_MODE_TRIPLANAR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 0));
			}
			else if(option == PARAM_MODE_PLANAR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 1));
			}
		}

		{
			Filename fn = data->GetFilename(PARAM_COLOR_TEXTURE);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("color_texture", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_COLOR_TEXTURE_META_COLORSPACE);
			if(option == PARAM_COLOR_TEXTURE_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("color_texture_meta_colorspace", "auto"));
			}
			else if(option == PARAM_COLOR_TEXTURE_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("color_texture_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_COLOR_TEXTURE_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("color_texture_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_COLOR_TEXTURE_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("color_texture_meta_colorspace", "linear"));
			}
		}

		{
			Filename fn = data->GetFilename(PARAM_FLOAT_TEXTURE);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("float_texture", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_FLOAT_TEXTURE_META_COLORSPACE);
			if(option == PARAM_FLOAT_TEXTURE_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("float_texture_meta_colorspace", "auto"));
			}
			else if(option == PARAM_FLOAT_TEXTURE_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("float_texture_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_FLOAT_TEXTURE_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("float_texture_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_FLOAT_TEXTURE_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("float_texture_meta_colorspace", "linear"));
			}
		}

		{
			Filename fn = data->GetFilename(PARAM_HEIGHT_TEXTURE);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("height_texture", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_HEIGHT_TEXTURE_META_COLORSPACE);
			if(option == PARAM_HEIGHT_TEXTURE_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("height_texture_meta_colorspace", "auto"));
			}
			else if(option == PARAM_HEIGHT_TEXTURE_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("height_texture_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_HEIGHT_TEXTURE_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("height_texture_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_HEIGHT_TEXTURE_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("height_texture_meta_colorspace", "linear"));
			}
		}

		f = (float) data->GetFloat(PARAM_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("scale", f));

		{
			Int32 option = data->GetInt32(PARAM_SPACE);
			if(option == PARAM_SPACE_WORLD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 0));
			}
			else if(option == PARAM_SPACE_OBJECT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 1));
			}
		}

		f = (float) data->GetFloat(PARAM_BLEND_SOFTNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_softness", f));

		f = (float) data->GetFloat(PARAM_BLEND_NOISE_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_noise_intensity", f));

		f = (float) data->GetFloat(PARAM_BLEND_NOISE_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_noise_scale", f));

		{
			int value = (int)data->GetBool(PARAM_USE_HEIGHT_BLENDING);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("use_height_blending", value));
		}

		f = (float) data->GetFloat(PARAM_VARIATION_OFFSET);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_offset", f));

		f = (float) data->GetFloat(PARAM_VARIATION_AXIS_ROTATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_axis_rotation", f));

		f = (float) data->GetFloat(PARAM_VARIATION_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("variation_scale", f));

		{
			int value = (int)data->GetBool(PARAM_TILE_REMOVAL);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("tile_removal", value));
		}

		f = (float) data->GetFloat(PARAM_TILE_SOFTNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_softness", f));

		f = (float) data->GetFloat(PARAM_TILE_OFFSET);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_offset", f));

		f = (float) data->GetFloat(PARAM_TILE_ROTATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_rotation", f));

		f = (float) data->GetFloat(PARAM_TILE_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_scale", f));

		{
			int value = (int)data->GetBool(PARAM_USEIMAGESEQUENCE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("useImageSequence", value));
		}

		{
			int value = (int)data->GetInt32(PARAM_FRAME);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("frame", value));
		}

		{
			int value = (int)data->GetInt32(PARAM_FRAMEOFFSET);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("frameOffset", value));
		}

	{
		Matrix m = data->GetMatrix(PARAM_PLACEMENTMATRIX);
		std::vector<float> mdata = MatrixToNSIMatrix(m);
		NSI::Argument matrix_arg("placementMatrix");
		matrix_arg.SetType(NSITypeMatrix);
		matrix_arg.SetValuePointer((void*)&mdata[0]);
		ctx.SetAttribute(shader_handle, matrix_arg);
	}

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTALPHA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outAlpha", f));

		f = (float) data->GetFloat(PARAM_OUTFLOAT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outFloat", f));

		f = (float) data->GetFloat(PARAM_OUTHEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outHeight", f));

	{
		v = toLinear(data->GetVector(PARAM_OUTMASK), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outMask", &v_data[0]));
	}

}

void  DlTriplanarTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_MODE, "mode");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_COLOR_TEXTURE, "color_texture");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_COLOR_TEXTURE_META_COLORSPACE, "color_texture_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_FLOAT_TEXTURE, "float_texture");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_FLOAT_TEXTURE_META_COLORSPACE, "float_texture_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_HEIGHT_TEXTURE, "height_texture");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_HEIGHT_TEXTURE_META_COLORSPACE, "height_texture_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_SCALE, "scale");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_SPACE, "space");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_BLEND_SOFTNESS, "blend_softness");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_BLEND_NOISE_INTENSITY, "blend_noise_intensity");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_BLEND_NOISE_SCALE, "blend_noise_scale");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_USE_HEIGHT_BLENDING, "use_height_blending");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_VARIATION_OFFSET, "variation_offset");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_VARIATION_AXIS_ROTATION, "variation_axis_rotation");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_VARIATION_SCALE, "variation_scale");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_TILE_REMOVAL, "tile_removal");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_TILE_SOFTNESS, "tile_softness");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_TILE_OFFSET, "tile_offset");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_TILE_ROTATION, "tile_rotation");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_TILE_SCALE, "tile_scale");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_USEIMAGESEQUENCE, "useImageSequence");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_FRAME, "frame");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_FRAMEOFFSET, "frameOffset");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_PLACEMENTMATRIX, "placementMatrix");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_OUTALPHA, "outAlpha");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_OUTFLOAT, "outFloat");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_OUTHEIGHT, "outHeight");
	PM->RegisterAttributeName(ID_DLTRIPLANAR, PARAM_OUTMASK, "outMask");
}

//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlFlakes.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlFlakes : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlFlakes); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlFlakes::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetFloat(PARAM_DENSITY , 0.5);
	data->SetFloat(PARAM_SCALE , 1);
	data->SetFloat(PARAM_RANDOMNESS , 0.5);
	data->SetInt32(PARAM_LAYERS , 1);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlFlakes(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLFLAKES, "Flakes"_s, 0, DlFlakes::Alloc, "dlFlakes"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlSolidRamp.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlSolidRamp : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlSolidRamp); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlSolidRamp::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetInt32(PARAM_SHAPE , PARAM_SHAPE_SPHERE);
	{
		AutoAlloc<Gradient> gradient;
		if (!gradient) return false;
		GradientKnot k;
		k.col = Vector(0, 0, 0);
		k.pos = 0;
		gradient->InsertKnot(k);
		k.col = Vector(1, 1, 1);
		k.pos = 1;
		gradient->InsertKnot(k);
		data->SetData(PARAM_COLORRAMP_COLORVALUE, GeData(CUSTOMDATATYPE_GRADIENT, *gradient));
	}
	{
		AutoAlloc<SplineData> spline;
		spline->MakeLinearSplineLinear(2);
		CustomSplineKnot* knot0 = spline->GetKnot(0);
		knot0->vPos = Vector(0, 0, 0);
		CustomSplineKnot* knot1 = spline->GetKnot(1);
		knot1->vPos = Vector(1, 1, 0);
		data->SetData(PARAM_OPACITYRAMP_FLOATVALUE, GeData(CUSTOMDATATYPE_SPLINE, *spline));
	}
	data->SetInt32(PARAM_REVERSE_RAMPS , PARAM_REVERSE_RAMPS_NONE);
	data->SetVector(PARAM_COLOR_BG , Vector(0, 0, 0));
	data->SetInt32(PARAM_OUTSIDE , PARAM_OUTSIDE_ONLY_BACKGROUND);
	data->SetInt32(PARAM_SPACE , PARAM_SPACE_WORLD);
	data->SetFloat(PARAM_DISTORTION_INTENSITY , 0);
	data->SetVector(PARAM_DISTORTION , Vector(0, 0, 0));
	data->SetFloat(PARAM_NOISE_INTENSITY , 0);
	data->SetFloat(PARAM_NOISE_SCALE , 1);
	data->SetFloat(PARAM_NOISE_STEPPING , 0);
	data->SetFloat(PARAM_NOISE_TIME , 0);
	data->SetFloat(PARAM_NOISE_HUE_VARIATION , 0.5);
	data->SetFloat(PARAM_NOISE_SATURATION_VARIATION , 0.5);
	data->SetFloat(PARAM_NOISE_VALUE_VARIATION , 0.5);
	data->SetFloat(PARAM_NOISE_OPACITY_VARIATION , 0.5);
	data->SetInt32(PARAM_LAYERS , 1);
	data->SetFloat(PARAM_LAYER_PERSISTENCE , 0.7);
	data->SetFloat(PARAM_LAYER_SCALE , 0.45);
	data->SetBool(PARAM_INVERT , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlSolidRamp(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLSOLIDRAMP, "Solid Ramp"_s, 0, DlSolidRamp::Alloc, "dlSolidRamp"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

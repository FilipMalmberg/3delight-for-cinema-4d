//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlRemap.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlRemap : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlRemap); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlRemap::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetFloat(PARAM_INPUTVALUE , 0);
	{
		AutoAlloc<SplineData> spline;
		spline->MakeLinearSplineLinear(2);
		CustomSplineKnot* knot0 = spline->GetKnot(0);
		knot0->vPos = Vector(0, 0, 0);
		CustomSplineKnot* knot1 = spline->GetKnot(1);
		knot1->vPos = Vector(1, 1, 0);
		data->SetData(PARAM_VALUE_FLOATVALUE, GeData(CUSTOMDATATYPE_SPLINE, *spline));
	}
	{
		AutoAlloc<Gradient> gradient;
		if (!gradient) return false;
		GradientKnot k;
		k.col = Vector(0, 0, 0);
		k.pos = 0;
		gradient->InsertKnot(k);
		k.col = Vector(1, 1, 1);
		k.pos = 1;
		gradient->InsertKnot(k);
		data->SetData(PARAM_COLOR_COLOR, GeData(CUSTOMDATATYPE_GRADIENT, *gradient));
	}
	data->SetFloat(PARAM_INPUTMIN , 0);
	data->SetFloat(PARAM_INPUTMAX , 1);
	data->SetFloat(PARAM_OUTPUTMIN , 0);
	data->SetFloat(PARAM_OUTPUTMAX , 1);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlRemap(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLREMAP, "Remap"_s, 0, DlRemap::Alloc, "dlRemap"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

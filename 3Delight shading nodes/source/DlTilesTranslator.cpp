#include "DlTilesTranslator.h"
#include "DlTiles.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlTilesTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlTiles.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord" );
		ctx.Connect("3dlfc4d::default_placement_matrix", "out", shader_handle, "placementMatrix" );
}

void DlTilesTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlTilesTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			Int32 option = data->GetInt32(PARAM_MODE);
			if(option == PARAM_MODE_2D__UV_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 0));
			}
			else if(option == PARAM_MODE_2D_LAYERED__UV_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 1));
			}
			else if(option == PARAM_MODE_3D__PLANAR_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 2));
			}
			else if(option == PARAM_MODE_3D__TRIPLANAR_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 3));
			}
		}

		{
			Int32 option = data->GetInt32(PARAM_SHAPE);
			if(option == PARAM_SHAPE_SQUARE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("shape", 0));
			}
			else if(option == PARAM_SHAPE_TRIANGLE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("shape", 1));
			}
			else if(option == PARAM_SHAPE_HEXAGON){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("shape", 2));
			}
			else if(option == PARAM_SHAPE_VORONOI){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("shape", 3));
			}
		}

		f = (float) data->GetFloat(PARAM_TILE_U);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_u", f));

		f = (float) data->GetFloat(PARAM_TILE_V);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_v", f));

		f = (float) data->GetFloat(PARAM_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("scale", f));

		f = (float) data->GetFloat(PARAM_CORNER_ROUNDING);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("corner_rounding", f));

		f = (float) data->GetFloat(PARAM_INNER_ROUNDING);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("inner_rounding", f));

		{
			Int32 option = data->GetInt32(PARAM_BEVEL_SHAPE);
			if(option == PARAM_BEVEL_SHAPE_NONE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("bevel_shape", 0));
			}
			else if(option == PARAM_BEVEL_SHAPE_CUSTOM){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("bevel_shape", 1));
			}
			else if(option == PARAM_BEVEL_SHAPE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("bevel_shape", 2));
			}
			else if(option == PARAM_BEVEL_SHAPE_SMOOTH){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("bevel_shape", 3));
			}
			else if(option == PARAM_BEVEL_SHAPE_ROUND){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("bevel_shape", 4));
			}
		}

		f = (float) data->GetFloat(PARAM_BEVEL_WIDTH);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("bevel_width", f));

		{
			int value = (int)data->GetInt32(PARAM_BEVEL_REPEAT);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("bevel_repeat", value));
		}

		{
			SplineData* spline = (SplineData*)data->GetCustomDataType(PARAM_BEVEL_CUSTOM_VAL, CUSTOMDATATYPE_SPLINE);

			int knot_count;
			std::vector<float> knots;
			std::vector<float> values;
			std::vector<int> interpolations;
			FillFloatRampNSIData(spline, doc, knots, values, interpolations, knot_count);

			NSI::Argument arg_knot_values("bevel_custom_val");
			arg_knot_values.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_values.SetValuePointer((void*)&values[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_values));

			NSI::Argument arg_knot_position("bevel_custom_pos");
			arg_knot_position.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_position.SetValuePointer((void*)&knots[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_position));

			NSI::Argument arg_knot_interpolation("bevel_custom_inter");
			arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);
			arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_interpolation));

		}

		f = (float) data->GetFloat(PARAM_SLOPE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("slope", f));

		f = (float) data->GetFloat(PARAM_SLOPE_ROTATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("slope_rotation", f));

		f = (float) data->GetFloat(PARAM_PROBABILITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("probability", f));

		f = (float) data->GetFloat(PARAM_TILE_U_ALT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_u_alt", f));

		f = (float) data->GetFloat(PARAM_TILE_V_ALT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_v_alt", f));

		f = (float) data->GetFloat(PARAM_STAGGER);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("stagger", f));

		{
			Int32 option = data->GetInt32(PARAM_STAGGER_DIR);
			if(option == PARAM_STAGGER_DIR_U_DIR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("stagger_dir", 0));
			}
			else if(option == PARAM_STAGGER_DIR_V_DIR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("stagger_dir", 1));
			}
		}

		f = (float) data->GetFloat(PARAM_SUBDIVISION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("subdivision", f));

		f = (float) data->GetFloat(PARAM_RANDOM_STAGGER);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_stagger", f));

		f = (float) data->GetFloat(PARAM_RANDOM_U_ALT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_u_alt", f));

		f = (float) data->GetFloat(PARAM_RANDOM_V_ALT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_v_alt", f));

	{
		v = toLinear(data->GetVector(PARAM_TILE_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("tile_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_TILE_HEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_height", f));

		f = (float) data->GetFloat(PARAM_TILE_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_roughness", f));

		f = (float) data->GetFloat(PARAM_TILE_METALLIC);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tile_metallic", f));

		{
			Gradient* gradient = (Gradient*)data->GetCustomDataType(PARAM_COLOR_ACROSS_TILE_COLORVALUE, CUSTOMDATATYPE_GRADIENT );

			int knot_count;
			std::vector<float> knots;
			std::vector<float> colors;
			std::vector<int> interpolations;
			FillColorRampNSIData(gradient, doc, knots, colors, interpolations, knot_count);

			NSI::Argument arg_knot_color("color_across_tile_ColorValue");
			arg_knot_color.SetArrayType(NSITypeColor, knot_count);
			arg_knot_color.SetValuePointer((void*)&colors[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_color));

			NSI::Argument arg_knot_position("color_across_tile_Position");
			arg_knot_position.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_position.SetValuePointer((void*)&knots[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_position));

			NSI::Argument arg_knot_interpolation("color_across_tile_Interp");
			arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);
			arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_interpolation));

		}

		{
			Int32 option = data->GetInt32(PARAM_COLOR_INSIDE_MODE);
			if(option == PARAM_COLOR_INSIDE_MODE_SHAPE__ABSOLUTE_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_inside_mode", 0));
			}
			else if(option == PARAM_COLOR_INSIDE_MODE_SHAPE__RELATIVE_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_inside_mode", 1));
			}
			else if(option == PARAM_COLOR_INSIDE_MODE_SLOPE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_inside_mode", 2));
			}
			else if(option == PARAM_COLOR_INSIDE_MODE_SIZE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_inside_mode", 3));
			}
			else if(option == PARAM_COLOR_INSIDE_MODE_ALTERNATING){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_inside_mode", 4));
			}
		}

		{
			Gradient* gradient = (Gradient*)data->GetCustomDataType(PARAM_COLOR_INSIDE_TILE_COLORVALUE, CUSTOMDATATYPE_GRADIENT );

			int knot_count;
			std::vector<float> knots;
			std::vector<float> colors;
			std::vector<int> interpolations;
			FillColorRampNSIData(gradient, doc, knots, colors, interpolations, knot_count);

			NSI::Argument arg_knot_color("color_inside_tile_ColorValue");
			arg_knot_color.SetArrayType(NSITypeColor, knot_count);
			arg_knot_color.SetValuePointer((void*)&colors[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_color));

			NSI::Argument arg_knot_position("color_inside_tile_Position");
			arg_knot_position.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_position.SetValuePointer((void*)&knots[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_position));

			NSI::Argument arg_knot_interpolation("color_inside_tile_Interp");
			arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);
			arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_interpolation));

		}

		{
			Int32 option = data->GetInt32(PARAM_GAP_MODE);
			if(option == PARAM_GAP_MODE_HEIGHT_BLEND){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("gap_mode", 0));
			}
			else if(option == PARAM_GAP_MODE_SUNKEN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("gap_mode", 1));
			}
		}

		f = (float) data->GetFloat(PARAM_GAP_BLEND);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("gap_blend", f));

		{
			Int32 option = data->GetInt32(PARAM_GAP_SIZE_MODE);
			if(option == PARAM_GAP_SIZE_MODE_ABSOLUTE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("gap_size_mode", 0));
			}
			else if(option == PARAM_GAP_SIZE_MODE_RELATIVE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("gap_size_mode", 1));
			}
		}

		f = (float) data->GetFloat(PARAM_GAP_SIZE_U);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("gap_size_u", f));

		f = (float) data->GetFloat(PARAM_GAP_SIZE_V);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("gap_size_v", f));

	{
		v = toLinear(data->GetVector(PARAM_GAP_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("gap_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_GAP_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("gap_roughness", f));

		f = (float) data->GetFloat(PARAM_GAP_METALLIC);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("gap_metallic", f));

		f = (float) data->GetFloat(PARAM_GAP_HEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("gap_height", f));

	{
		v = toLinear(data->GetVector(PARAM_DISTORTION), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("distortion", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DISTORTION_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("distortion_intensity", f));

		{
			Int32 option = data->GetInt32(PARAM_BUILTIN_DISTORTION);
			if(option == PARAM_BUILTIN_DISTORTION_OFF){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 0));
			}
			else if(option == PARAM_BUILTIN_DISTORTION_SOFT_PERLIN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 1));
			}
			else if(option == PARAM_BUILTIN_DISTORTION_HARD_PERLIN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 2));
			}
			else if(option == PARAM_BUILTIN_DISTORTION_PERLIN_MIX_1){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 3));
			}
			else if(option == PARAM_BUILTIN_DISTORTION_PERLIN_MIX_2){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 4));
			}
			else if(option == PARAM_BUILTIN_DISTORTION_PERLIN_INFLECTION_1){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 5));
			}
			else if(option == PARAM_BUILTIN_DISTORTION_PERLIN_INFLECTION_2){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 6));
			}
			else if(option == PARAM_BUILTIN_DISTORTION_BOX){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 7));
			}
			else if(option == PARAM_BUILTIN_DISTORTION_VORONOI_1){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 8));
			}
			else if(option == PARAM_BUILTIN_DISTORTION_VORONOI_2){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 9));
			}
			else if(option == PARAM_BUILTIN_DISTORTION_VORONOI_3){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_distortion", 10));
			}
		}

		f = (float) data->GetFloat(PARAM_BUILTIN_DISTORTION_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("builtin_distortion_scale", f));

		f = (float) data->GetFloat(PARAM_BUILTIN_DISTORTION_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("builtin_distortion_intensity", f));

		{
			Filename fn = data->GetFilename(PARAM_TEXTURE_TILE_COLOR);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_color", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_TEXTURE_TILE_COLOR_META_COLORSPACE);
			if(option == PARAM_TEXTURE_TILE_COLOR_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_color_meta_colorspace", "auto"));
			}
			else if(option == PARAM_TEXTURE_TILE_COLOR_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_color_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_TEXTURE_TILE_COLOR_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_color_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_TEXTURE_TILE_COLOR_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_color_meta_colorspace", "linear"));
			}
		}

		{
			Filename fn = data->GetFilename(PARAM_TEXTURE_TILE_HEIGHT);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_height", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_TEXTURE_TILE_HEIGHT_META_COLORSPACE);
			if(option == PARAM_TEXTURE_TILE_HEIGHT_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_height_meta_colorspace", "auto"));
			}
			else if(option == PARAM_TEXTURE_TILE_HEIGHT_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_height_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_TEXTURE_TILE_HEIGHT_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_height_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_TEXTURE_TILE_HEIGHT_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_height_meta_colorspace", "linear"));
			}
		}

		{
			Filename fn = data->GetFilename(PARAM_TEXTURE_TILE_ROUGHNESS);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_roughness", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_TEXTURE_TILE_ROUGHNESS_META_COLORSPACE);
			if(option == PARAM_TEXTURE_TILE_ROUGHNESS_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_roughness_meta_colorspace", "auto"));
			}
			else if(option == PARAM_TEXTURE_TILE_ROUGHNESS_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_roughness_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_TEXTURE_TILE_ROUGHNESS_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_roughness_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_TEXTURE_TILE_ROUGHNESS_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_roughness_meta_colorspace", "linear"));
			}
		}

		{
			Filename fn = data->GetFilename(PARAM_TEXTURE_TILE_METALLIC);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_metallic", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_TEXTURE_TILE_METALLIC_META_COLORSPACE);
			if(option == PARAM_TEXTURE_TILE_METALLIC_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_metallic_meta_colorspace", "auto"));
			}
			else if(option == PARAM_TEXTURE_TILE_METALLIC_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_metallic_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_TEXTURE_TILE_METALLIC_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_metallic_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_TEXTURE_TILE_METALLIC_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_tile_metallic_meta_colorspace", "linear"));
			}
		}

		f = (float) data->GetFloat(PARAM_TEXTURE_TILE_COLOR_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("texture_tile_color_intensity", f));

		f = (float) data->GetFloat(PARAM_TEXTURE_TILE_HEIGHT_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("texture_tile_height_intensity", f));

		f = (float) data->GetFloat(PARAM_TEXTURE_TILE_ROUGHNESS_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("texture_tile_roughness_intensity", f));

		f = (float) data->GetFloat(PARAM_TEXTURE_TILE_METALLIC_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("texture_tile_metallic_intensity", f));

		f = (float) data->GetFloat(PARAM_TEXTURE_TILE_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("texture_tile_scale", f));

		f = (float) data->GetFloat(PARAM_TEXTURE_TILE_ROTATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("texture_tile_rotation", f));

		{
			Filename fn = data->GetFilename(PARAM_TEXTURE_UV_COLOR);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_color", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_TEXTURE_UV_COLOR_META_COLORSPACE);
			if(option == PARAM_TEXTURE_UV_COLOR_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_color_meta_colorspace", "auto"));
			}
			else if(option == PARAM_TEXTURE_UV_COLOR_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_color_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_TEXTURE_UV_COLOR_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_color_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_TEXTURE_UV_COLOR_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_color_meta_colorspace", "linear"));
			}
		}

		{
			Filename fn = data->GetFilename(PARAM_TEXTURE_UV_ROUGHNESS);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_roughness", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_TEXTURE_UV_ROUGHNESS_META_COLORSPACE);
			if(option == PARAM_TEXTURE_UV_ROUGHNESS_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_roughness_meta_colorspace", "auto"));
			}
			else if(option == PARAM_TEXTURE_UV_ROUGHNESS_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_roughness_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_TEXTURE_UV_ROUGHNESS_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_roughness_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_TEXTURE_UV_ROUGHNESS_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_roughness_meta_colorspace", "linear"));
			}
		}

		{
			Filename fn = data->GetFilename(PARAM_TEXTURE_UV_METALLIC);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_metallic", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_TEXTURE_UV_METALLIC_META_COLORSPACE);
			if(option == PARAM_TEXTURE_UV_METALLIC_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_metallic_meta_colorspace", "auto"));
			}
			else if(option == PARAM_TEXTURE_UV_METALLIC_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_metallic_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_TEXTURE_UV_METALLIC_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_metallic_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_TEXTURE_UV_METALLIC_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_metallic_meta_colorspace", "linear"));
			}
		}

		{
			Filename fn = data->GetFilename(PARAM_TEXTURE_UV_HEIGHT);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_height", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_TEXTURE_UV_HEIGHT_META_COLORSPACE);
			if(option == PARAM_TEXTURE_UV_HEIGHT_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_height_meta_colorspace", "auto"));
			}
			else if(option == PARAM_TEXTURE_UV_HEIGHT_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_height_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_TEXTURE_UV_HEIGHT_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_height_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_TEXTURE_UV_HEIGHT_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_height_meta_colorspace", "linear"));
			}
		}

		{
			Filename fn = data->GetFilename(PARAM_TEXTURE_UV_PROBABILITY);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_probability", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_TEXTURE_UV_PROBABILITY_META_COLORSPACE);
			if(option == PARAM_TEXTURE_UV_PROBABILITY_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_probability_meta_colorspace", "auto"));
			}
			else if(option == PARAM_TEXTURE_UV_PROBABILITY_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_probability_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_TEXTURE_UV_PROBABILITY_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_probability_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_TEXTURE_UV_PROBABILITY_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("texture_uv_probability_meta_colorspace", "linear"));
			}
		}

		f = (float) data->GetFloat(PARAM_TEXTURE_UV_HEIGHT_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("texture_uv_height_intensity", f));

		f = (float) data->GetFloat(PARAM_TEXTURE_UV_BLUR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("texture_uv_blur", f));

		{
			Int32 option = data->GetInt32(PARAM_BUILTIN_PATTERN);
			if(option == PARAM_BUILTIN_PATTERN_OFF){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 0));
			}
			else if(option == PARAM_BUILTIN_PATTERN_SOFT_PERLIN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 1));
			}
			else if(option == PARAM_BUILTIN_PATTERN_HARD_PERLIN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 2));
			}
			else if(option == PARAM_BUILTIN_PATTERN_PERLIN_MIX_1){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 3));
			}
			else if(option == PARAM_BUILTIN_PATTERN_PERLIN_MIX_2){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 4));
			}
			else if(option == PARAM_BUILTIN_PATTERN_PERLIN_INFLECTION_1){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 5));
			}
			else if(option == PARAM_BUILTIN_PATTERN_PERLIN_INFLECTION_2){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 6));
			}
			else if(option == PARAM_BUILTIN_PATTERN_BOX){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 7));
			}
			else if(option == PARAM_BUILTIN_PATTERN_VORONOI_1){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 8));
			}
			else if(option == PARAM_BUILTIN_PATTERN_VORONOI_2){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 9));
			}
			else if(option == PARAM_BUILTIN_PATTERN_VORONOI_3){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("builtin_pattern", 10));
			}
		}

		f = (float) data->GetFloat(PARAM_BUILTIN_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("builtin_scale", f));

		f = (float) data->GetFloat(PARAM_BUILTIN_ROTATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("builtin_rotation", f));

		f = (float) data->GetFloat(PARAM_BUILTIN_COLOR_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("builtin_color_intensity", f));

		f = (float) data->GetFloat(PARAM_BUILTIN_HEIGHT_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("builtin_height_intensity", f));

		f = (float) data->GetFloat(PARAM_BUILTIN_ROUGHNESS_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("builtin_roughness_intensity", f));

		f = (float) data->GetFloat(PARAM_BUILTIN_METALLIC_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("builtin_metallic_intensity", f));

		f = (float) data->GetFloat(PARAM_RANDOM_BRIGHTNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_brightness", f));

		f = (float) data->GetFloat(PARAM_RANDOM_HUE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_hue", f));

		f = (float) data->GetFloat(PARAM_RANDOM_HEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_height", f));

		f = (float) data->GetFloat(PARAM_RANDOM_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_roughness", f));

		f = (float) data->GetFloat(PARAM_RANDOM_METALLIC);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_metallic", f));

		f = (float) data->GetFloat(PARAM_RANDOM_OFFSET_X);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_offset_x", f));

		f = (float) data->GetFloat(PARAM_RANDOM_OFFSET_Y);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_offset_y", f));

		f = (float) data->GetFloat(PARAM_RANDOM_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_scale", f));

		f = (float) data->GetFloat(PARAM_RANDOM_ROTATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_rotation", f));

		{
			int value = (int)data->GetInt32(PARAM_RANDOM_ROTATION_STEPS);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("random_rotation_steps", value));
		}

		f = (float) data->GetFloat(PARAM_RANDOM_SLOPE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_slope", f));

		f = (float) data->GetFloat(PARAM_RANDOM_ROUNDING);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_rounding", f));

		f = (float) data->GetFloat(PARAM_RANDOM_TILE_TEXTURES);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_tile_textures", f));

		f = (float) data->GetFloat(PARAM_RANDOM_BUILTIN);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("random_builtin", f));

		f = (float) data->GetFloat(PARAM_SMOOTH_NOISE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("smooth_noise", f));

		f = (float) data->GetFloat(PARAM_SMOOTH_NOISE_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("smooth_noise_scale", f));

		f = (float) data->GetFloat(PARAM_SMOOTH_NOISE_DETAIL);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("smooth_noise_detail", f));

		f = (float) data->GetFloat(PARAM_DUST_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("dust_intensity", f));

	{
		v = toLinear(data->GetVector(PARAM_DUST_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("dust_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DUST_COVERAGE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("dust_coverage", f));

		f = (float) data->GetFloat(PARAM_DUST_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("dust_roughness", f));

		f = (float) data->GetFloat(PARAM_TRIPLANAR_SOFTNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("triplanar_softness", f));

		{
			Int32 option = data->GetInt32(PARAM_LAYERED_MODE);
			if(option == PARAM_LAYERED_MODE_HEIGHT_BLEND){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("layered_mode", 0));
			}
			else if(option == PARAM_LAYERED_MODE_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("layered_mode", 1));
			}
		}

		f = (float) data->GetFloat(PARAM_LAYERED_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layered_scale", f));

		f = (float) data->GetFloat(PARAM_LAYERED_PERSISTENCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layered_persistence", f));

		{
			Int32 option = data->GetInt32(PARAM_SPACE);
			if(option == PARAM_SPACE_WORLD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 0));
			}
			else if(option == PARAM_SPACE_OBJECT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 1));
			}
		}

		{
			int value = (int)data->GetInt32(PARAM_SEED);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("seed", value));
		}

	{
		Matrix m = data->GetMatrix(PARAM_PLACEMENTMATRIX);
		std::vector<float> mdata = MatrixToNSIMatrix(m);
		NSI::Argument matrix_arg("placementMatrix");
		matrix_arg.SetType(NSITypeMatrix);
		matrix_arg.SetValuePointer((void*)&mdata[0]);
		ctx.SetAttribute(shader_handle, matrix_arg);
	}

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTALPHA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outAlpha", f));

		f = (float) data->GetFloat(PARAM_OUTROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outRoughness", f));

		f = (float) data->GetFloat(PARAM_OUTMETALLIC);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outMetallic", f));

		f = (float) data->GetFloat(PARAM_OUTHEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outHeight", f));

	{
		v = toLinear(data->GetVector(PARAM_OUTTILEUV), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outTileUV", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_OUTTEXTUREUV), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outTextureUV", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_OUTMASK), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outMask", &v_data[0]));
	}

}

void  DlTilesTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLTILES, PARAM_MODE, "mode");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_SHAPE, "shape");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TILE_U, "tile_u");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TILE_V, "tile_v");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_SCALE, "scale");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_CORNER_ROUNDING, "corner_rounding");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_INNER_ROUNDING, "inner_rounding");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BEVEL_SHAPE, "bevel_shape");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BEVEL_WIDTH, "bevel_width");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BEVEL_REPEAT, "bevel_repeat");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BEVEL_CUSTOM_VAL, "bevel_custom_val");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BEVEL_CUSTOM_POS, "bevel_custom_pos");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BEVEL_CUSTOM_INTER, "bevel_custom_inter");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_SLOPE, "slope");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_SLOPE_ROTATION, "slope_rotation");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_PROBABILITY, "probability");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TILE_U_ALT, "tile_u_alt");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TILE_V_ALT, "tile_v_alt");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_STAGGER, "stagger");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_STAGGER_DIR, "stagger_dir");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_SUBDIVISION, "subdivision");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_STAGGER, "random_stagger");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_U_ALT, "random_u_alt");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_V_ALT, "random_v_alt");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TILE_COLOR, "tile_color");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TILE_HEIGHT, "tile_height");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TILE_ROUGHNESS, "tile_roughness");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TILE_METALLIC, "tile_metallic");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_COLOR_ACROSS_TILE_POSITION, "color_across_tile_Position");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_COLOR_ACROSS_TILE_COLORVALUE, "color_across_tile_ColorValue");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_COLOR_ACROSS_TILE_INTERP, "color_across_tile_Interp");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_COLOR_INSIDE_MODE, "color_inside_mode");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_COLOR_INSIDE_TILE_POSITION, "color_inside_tile_Position");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_COLOR_INSIDE_TILE_COLORVALUE, "color_inside_tile_ColorValue");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_COLOR_INSIDE_TILE_INTERP, "color_inside_tile_Interp");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_GAP_MODE, "gap_mode");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_GAP_BLEND, "gap_blend");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_GAP_SIZE_MODE, "gap_size_mode");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_GAP_SIZE_U, "gap_size_u");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_GAP_SIZE_V, "gap_size_v");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_GAP_COLOR, "gap_color");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_GAP_ROUGHNESS, "gap_roughness");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_GAP_METALLIC, "gap_metallic");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_GAP_HEIGHT, "gap_height");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_DISTORTION, "distortion");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_DISTORTION_INTENSITY, "distortion_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BUILTIN_DISTORTION, "builtin_distortion");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BUILTIN_DISTORTION_SCALE, "builtin_distortion_scale");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BUILTIN_DISTORTION_INTENSITY, "builtin_distortion_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_COLOR, "texture_tile_color");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_COLOR_META_COLORSPACE, "texture_tile_color_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_HEIGHT, "texture_tile_height");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_HEIGHT_META_COLORSPACE, "texture_tile_height_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_ROUGHNESS, "texture_tile_roughness");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_ROUGHNESS_META_COLORSPACE, "texture_tile_roughness_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_METALLIC, "texture_tile_metallic");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_METALLIC_META_COLORSPACE, "texture_tile_metallic_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_COLOR_INTENSITY, "texture_tile_color_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_HEIGHT_INTENSITY, "texture_tile_height_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_ROUGHNESS_INTENSITY, "texture_tile_roughness_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_METALLIC_INTENSITY, "texture_tile_metallic_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_SCALE, "texture_tile_scale");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_TILE_ROTATION, "texture_tile_rotation");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_COLOR, "texture_uv_color");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_COLOR_META_COLORSPACE, "texture_uv_color_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_ROUGHNESS, "texture_uv_roughness");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_ROUGHNESS_META_COLORSPACE, "texture_uv_roughness_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_METALLIC, "texture_uv_metallic");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_METALLIC_META_COLORSPACE, "texture_uv_metallic_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_HEIGHT, "texture_uv_height");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_HEIGHT_META_COLORSPACE, "texture_uv_height_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_PROBABILITY, "texture_uv_probability");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_PROBABILITY_META_COLORSPACE, "texture_uv_probability_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_HEIGHT_INTENSITY, "texture_uv_height_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TEXTURE_UV_BLUR, "texture_uv_blur");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BUILTIN_PATTERN, "builtin_pattern");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BUILTIN_SCALE, "builtin_scale");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BUILTIN_ROTATION, "builtin_rotation");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BUILTIN_COLOR_INTENSITY, "builtin_color_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BUILTIN_HEIGHT_INTENSITY, "builtin_height_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BUILTIN_ROUGHNESS_INTENSITY, "builtin_roughness_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_BUILTIN_METALLIC_INTENSITY, "builtin_metallic_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_BRIGHTNESS, "random_brightness");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_HUE, "random_hue");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_HEIGHT, "random_height");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_ROUGHNESS, "random_roughness");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_METALLIC, "random_metallic");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_OFFSET_X, "random_offset_x");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_OFFSET_Y, "random_offset_y");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_SCALE, "random_scale");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_ROTATION, "random_rotation");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_ROTATION_STEPS, "random_rotation_steps");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_SLOPE, "random_slope");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_ROUNDING, "random_rounding");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_TILE_TEXTURES, "random_tile_textures");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_RANDOM_BUILTIN, "random_builtin");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_SMOOTH_NOISE, "smooth_noise");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_SMOOTH_NOISE_SCALE, "smooth_noise_scale");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_SMOOTH_NOISE_DETAIL, "smooth_noise_detail");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_DUST_INTENSITY, "dust_intensity");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_DUST_COLOR, "dust_color");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_DUST_COVERAGE, "dust_coverage");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_DUST_ROUGHNESS, "dust_roughness");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_TRIPLANAR_SOFTNESS, "triplanar_softness");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_LAYERED_MODE, "layered_mode");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_LAYERED_SCALE, "layered_scale");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_LAYERED_PERSISTENCE, "layered_persistence");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_SPACE, "space");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_SEED, "seed");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_UVCOORD, "uvCoord");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_PLACEMENTMATRIX, "placementMatrix");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_OUTALPHA, "outAlpha");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_OUTROUGHNESS, "outRoughness");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_OUTMETALLIC, "outMetallic");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_OUTHEIGHT, "outHeight");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_OUTTILEUV, "outTileUV");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_OUTTEXTUREUV, "outTextureUV");
	PM->RegisterAttributeName(ID_DLTILES, PARAM_OUTMASK, "outMask");
}

#include "c4d.h"

//#include "NodeTest.h"
#include <c4d_operatordata.h>
#include "IDs.h"
#include "gvbase.h"

class dlTerminal : public GvOperatorData
{
public:
	//virtual Bool Init(GeListNode* node);
	static NodeData* Alloc()
	{
		return NewObjClear(dlTerminal);
	}

	//virtual Bool iCreateOperator(GvNode * 	node);

};

//Bool dlTerminal::iCreateOperator(GvNode * 	node){
	//BaseObject* op = (BaseObject*)node;
	//BaseContainer* data = op->GetDataInstance();
	//data->SetVector(COLORDATA, Vector(1, 1, 1));
	//return true;
//}


Bool RegisterDlTerminal(void)
{
	return GvRegisterOperatorPlugin(DL_TERMINAL,
		"dlTerminal"_s,
		0,
		dlTerminal::Alloc,
		"dlTerminal"_s,
		0,
		DL_NODES_CLASS,
		ID_GV_OPGROUP_TYPE_GENERAL,
		0,
		AutoBitmap("dl_200.tif"_s)
	);
}


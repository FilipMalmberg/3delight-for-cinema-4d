#include "DlFlakesTranslator.h"
#include "DlFlakes.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlFlakesTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlFlakes.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("3dlfc4d::default_placement_matrix", "out", shader_handle, "placementMatrix" );
}

void DlFlakesTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlFlakesTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		f = (float) data->GetFloat(PARAM_DENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("density", f));

		f = (float) data->GetFloat(PARAM_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("scale", f));

		f = (float) data->GetFloat(PARAM_RANDOMNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("randomness", f));

		{
			int value = (int)data->GetInt32(PARAM_LAYERS);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("layers", value));
		}

	{
		Matrix m = data->GetMatrix(PARAM_PLACEMENTMATRIX);
		std::vector<float> mdata = MatrixToNSIMatrix(m);
		NSI::Argument matrix_arg("placementMatrix");
		matrix_arg.SetType(NSITypeMatrix);
		matrix_arg.SetValuePointer((void*)&mdata[0]);
		ctx.SetAttribute(shader_handle, matrix_arg);
	}

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTCOLORR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorR", f));

		f = (float) data->GetFloat(PARAM_OUTCOLORG);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorG", f));

		f = (float) data->GetFloat(PARAM_OUTCOLORB);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorB", f));

		f = (float) data->GetFloat(PARAM_OUTALPHA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outAlpha", f));

}

void  DlFlakesTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLFLAKES, PARAM_DENSITY, "density");
	PM->RegisterAttributeName(ID_DLFLAKES, PARAM_SCALE, "scale");
	PM->RegisterAttributeName(ID_DLFLAKES, PARAM_RANDOMNESS, "randomness");
	PM->RegisterAttributeName(ID_DLFLAKES, PARAM_LAYERS, "layers");
	PM->RegisterAttributeName(ID_DLFLAKES, PARAM_PLACEMENTMATRIX, "placementMatrix");
	PM->RegisterAttributeName(ID_DLFLAKES, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DLFLAKES, PARAM_OUTCOLORR, "outColorR");
	PM->RegisterAttributeName(ID_DLFLAKES, PARAM_OUTCOLORG, "outColorG");
	PM->RegisterAttributeName(ID_DLFLAKES, PARAM_OUTCOLORB, "outColorB");
	PM->RegisterAttributeName(ID_DLFLAKES, PARAM_OUTALPHA, "outAlpha");
}

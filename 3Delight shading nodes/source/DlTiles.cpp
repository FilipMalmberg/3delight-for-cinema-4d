//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlTiles.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlTiles : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlTiles); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlTiles::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetInt32(PARAM_MODE , PARAM_MODE_2D__UV_);
	data->SetInt32(PARAM_SHAPE , PARAM_SHAPE_SQUARE);
	data->SetFloat(PARAM_TILE_U , 10);
	data->SetFloat(PARAM_TILE_V , 10);
	data->SetFloat(PARAM_SCALE , 1);
	data->SetFloat(PARAM_CORNER_ROUNDING , 0.05);
	data->SetFloat(PARAM_INNER_ROUNDING , 0.05);
	data->SetInt32(PARAM_BEVEL_SHAPE , PARAM_BEVEL_SHAPE_NONE);
	data->SetFloat(PARAM_BEVEL_WIDTH , 0.1);
	data->SetInt32(PARAM_BEVEL_REPEAT , 1);
	{
		AutoAlloc<SplineData> spline;
		spline->MakeLinearSplineLinear(2);
		CustomSplineKnot* knot0 = spline->GetKnot(0);
		knot0->vPos = Vector(0, 0, 0);
		CustomSplineKnot* knot1 = spline->GetKnot(1);
		knot1->vPos = Vector(1, 1, 0);
		data->SetData(PARAM_BEVEL_CUSTOM_VAL, GeData(CUSTOMDATATYPE_SPLINE, *spline));
	}
	data->SetFloat(PARAM_SLOPE , 0);
	data->SetFloat(PARAM_SLOPE_ROTATION , 0);
	data->SetFloat(PARAM_PROBABILITY , 1);
	data->SetFloat(PARAM_TILE_U_ALT , 0);
	data->SetFloat(PARAM_TILE_V_ALT , 0);
	data->SetFloat(PARAM_STAGGER , 0);
	data->SetInt32(PARAM_STAGGER_DIR , PARAM_STAGGER_DIR_U_DIR);
	data->SetFloat(PARAM_SUBDIVISION , 0);
	data->SetFloat(PARAM_RANDOM_STAGGER , 0);
	data->SetFloat(PARAM_RANDOM_U_ALT , 0);
	data->SetFloat(PARAM_RANDOM_V_ALT , 0);
	data->SetVector(PARAM_TILE_COLOR , Vector(1, 1, 1));
	data->SetFloat(PARAM_TILE_HEIGHT , 1);
	data->SetFloat(PARAM_TILE_ROUGHNESS , 0.1);
	data->SetFloat(PARAM_TILE_METALLIC , 0);
	{
		AutoAlloc<Gradient> gradient;
		if (!gradient) return false;
		GradientKnot k;
		k.col = Vector(0, 0, 0);
		k.pos = 0;
		gradient->InsertKnot(k);
		k.col = Vector(1, 1, 1);
		k.pos = 1;
		gradient->InsertKnot(k);
		data->SetData(PARAM_COLOR_ACROSS_TILE_COLORVALUE, GeData(CUSTOMDATATYPE_GRADIENT, *gradient));
	}
	data->SetInt32(PARAM_COLOR_INSIDE_MODE , PARAM_COLOR_INSIDE_MODE_SHAPE__ABSOLUTE_);
	{
		AutoAlloc<Gradient> gradient;
		if (!gradient) return false;
		GradientKnot k;
		k.col = Vector(0, 0, 0);
		k.pos = 0;
		gradient->InsertKnot(k);
		k.col = Vector(1, 1, 1);
		k.pos = 1;
		gradient->InsertKnot(k);
		data->SetData(PARAM_COLOR_INSIDE_TILE_COLORVALUE, GeData(CUSTOMDATATYPE_GRADIENT, *gradient));
	}
	data->SetInt32(PARAM_GAP_MODE , PARAM_GAP_MODE_HEIGHT_BLEND);
	data->SetFloat(PARAM_GAP_BLEND , 0.1);
	data->SetInt32(PARAM_GAP_SIZE_MODE , PARAM_GAP_SIZE_MODE_ABSOLUTE);
	data->SetFloat(PARAM_GAP_SIZE_U , 0.05);
	data->SetFloat(PARAM_GAP_SIZE_V , 0.05);
	data->SetVector(PARAM_GAP_COLOR , Vector(0.05, 0.05, 0.05));
	data->SetFloat(PARAM_GAP_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_GAP_METALLIC , 0);
	data->SetFloat(PARAM_GAP_HEIGHT , 0.05);
	data->SetVector(PARAM_DISTORTION , Vector(0.5, 0.5, 0.5));
	data->SetFloat(PARAM_DISTORTION_INTENSITY , 1);
	data->SetInt32(PARAM_BUILTIN_DISTORTION , PARAM_BUILTIN_DISTORTION_OFF);
	data->SetFloat(PARAM_BUILTIN_DISTORTION_SCALE , 1);
	data->SetFloat(PARAM_BUILTIN_DISTORTION_INTENSITY , 1);
	data->SetString(PARAM_TEXTURE_TILE_COLOR, String(""));
	data->SetInt32(PARAM_TEXTURE_TILE_COLOR_META_COLORSPACE, PARAM_TEXTURE_TILE_COLOR_META_COLORSPACE_AUTO);
	data->SetString(PARAM_TEXTURE_TILE_HEIGHT, String(""));
	data->SetInt32(PARAM_TEXTURE_TILE_HEIGHT_META_COLORSPACE, PARAM_TEXTURE_TILE_HEIGHT_META_COLORSPACE_LINEAR);
	data->SetString(PARAM_TEXTURE_TILE_ROUGHNESS, String(""));
	data->SetInt32(PARAM_TEXTURE_TILE_ROUGHNESS_META_COLORSPACE, PARAM_TEXTURE_TILE_ROUGHNESS_META_COLORSPACE_LINEAR);
	data->SetString(PARAM_TEXTURE_TILE_METALLIC, String(""));
	data->SetInt32(PARAM_TEXTURE_TILE_METALLIC_META_COLORSPACE, PARAM_TEXTURE_TILE_METALLIC_META_COLORSPACE_LINEAR);
	data->SetFloat(PARAM_TEXTURE_TILE_COLOR_INTENSITY , 1);
	data->SetFloat(PARAM_TEXTURE_TILE_HEIGHT_INTENSITY , 1);
	data->SetFloat(PARAM_TEXTURE_TILE_ROUGHNESS_INTENSITY , 1);
	data->SetFloat(PARAM_TEXTURE_TILE_METALLIC_INTENSITY , 1);
	data->SetFloat(PARAM_TEXTURE_TILE_SCALE , 1);
	data->SetFloat(PARAM_TEXTURE_TILE_ROTATION , 0);
	data->SetString(PARAM_TEXTURE_UV_COLOR, String(""));
	data->SetInt32(PARAM_TEXTURE_UV_COLOR_META_COLORSPACE, PARAM_TEXTURE_UV_COLOR_META_COLORSPACE_AUTO);
	data->SetString(PARAM_TEXTURE_UV_ROUGHNESS, String(""));
	data->SetInt32(PARAM_TEXTURE_UV_ROUGHNESS_META_COLORSPACE, PARAM_TEXTURE_UV_ROUGHNESS_META_COLORSPACE_LINEAR);
	data->SetString(PARAM_TEXTURE_UV_METALLIC, String(""));
	data->SetInt32(PARAM_TEXTURE_UV_METALLIC_META_COLORSPACE, PARAM_TEXTURE_UV_METALLIC_META_COLORSPACE_LINEAR);
	data->SetString(PARAM_TEXTURE_UV_HEIGHT, String(""));
	data->SetInt32(PARAM_TEXTURE_UV_HEIGHT_META_COLORSPACE, PARAM_TEXTURE_UV_HEIGHT_META_COLORSPACE_LINEAR);
	data->SetString(PARAM_TEXTURE_UV_PROBABILITY, String(""));
	data->SetInt32(PARAM_TEXTURE_UV_PROBABILITY_META_COLORSPACE, PARAM_TEXTURE_UV_PROBABILITY_META_COLORSPACE_LINEAR);
	data->SetFloat(PARAM_TEXTURE_UV_HEIGHT_INTENSITY , 1);
	data->SetFloat(PARAM_TEXTURE_UV_BLUR , 0);
	data->SetInt32(PARAM_BUILTIN_PATTERN , PARAM_BUILTIN_PATTERN_OFF);
	data->SetFloat(PARAM_BUILTIN_SCALE , 1);
	data->SetFloat(PARAM_BUILTIN_ROTATION , 0);
	data->SetFloat(PARAM_BUILTIN_COLOR_INTENSITY , 1);
	data->SetFloat(PARAM_BUILTIN_HEIGHT_INTENSITY , 1);
	data->SetFloat(PARAM_BUILTIN_ROUGHNESS_INTENSITY , 1);
	data->SetFloat(PARAM_BUILTIN_METALLIC_INTENSITY , 1);
	data->SetFloat(PARAM_RANDOM_BRIGHTNESS , 0);
	data->SetFloat(PARAM_RANDOM_HUE , 0);
	data->SetFloat(PARAM_RANDOM_HEIGHT , 0);
	data->SetFloat(PARAM_RANDOM_ROUGHNESS , 0);
	data->SetFloat(PARAM_RANDOM_METALLIC , 0);
	data->SetFloat(PARAM_RANDOM_OFFSET_X , 0);
	data->SetFloat(PARAM_RANDOM_OFFSET_Y , 0);
	data->SetFloat(PARAM_RANDOM_SCALE , 0);
	data->SetFloat(PARAM_RANDOM_ROTATION , 0);
	data->SetInt32(PARAM_RANDOM_ROTATION_STEPS , 1);
	data->SetFloat(PARAM_RANDOM_SLOPE , 0);
	data->SetFloat(PARAM_RANDOM_ROUNDING , 0);
	data->SetFloat(PARAM_RANDOM_TILE_TEXTURES , 0);
	data->SetFloat(PARAM_RANDOM_BUILTIN , 0);
	data->SetFloat(PARAM_SMOOTH_NOISE , 0);
	data->SetFloat(PARAM_SMOOTH_NOISE_SCALE , 1);
	data->SetFloat(PARAM_SMOOTH_NOISE_DETAIL , 0);
	data->SetFloat(PARAM_DUST_INTENSITY , 0);
	data->SetVector(PARAM_DUST_COLOR , Vector(0.9, 0.9, 0.9));
	data->SetFloat(PARAM_DUST_COVERAGE , 1);
	data->SetFloat(PARAM_DUST_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_TRIPLANAR_SOFTNESS , 0.5);
	data->SetInt32(PARAM_LAYERED_MODE , PARAM_LAYERED_MODE_HEIGHT_BLEND);
	data->SetFloat(PARAM_LAYERED_SCALE , 0.5);
	data->SetFloat(PARAM_LAYERED_PERSISTENCE , 0.5);
	data->SetInt32(PARAM_SPACE , PARAM_SPACE_WORLD);
	data->SetInt32(PARAM_SEED , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlTiles(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLTILES, "Tiles"_s, 0, DlTiles::Alloc, "dlTiles"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

#include "c4d.h"
#include "IDs.h"
#include "Oplace3dtexturewidget.h"

class Place3DTextureWidget : public ObjectData
{
public:
	static NodeData* Alloc(void)
	{
		return NewObjClear(Place3DTextureWidget);
	}

	virtual Bool Init(GeListNode* node);

	virtual Int32 GetHandleCount(BaseObject* op);
	virtual void GetHandle(BaseObject* op, Int32 i, HandleInfo& info);
	virtual void SetHandle(BaseObject* op, Int32 i, Vector p, const HandleInfo& info);



	virtual DRAWRESULT Draw(BaseObject* op,
		DRAWPASS drawpass,
		BaseDraw* bd,
		BaseDrawHelp* bh);
};



Bool Place3DTextureWidget::Init(GeListNode* node) {
	BaseObject* op = (BaseObject*)node;
	BaseContainer* data = op->GetDataInstance();
	data->SetFloat(SIZE_X, 200.0);
	data->SetFloat(SIZE_Y, 200.0);
	data->SetFloat(SIZE_Z, 200.0);
	return true;
}




DRAWRESULT Place3DTextureWidget::Draw(BaseObject* op,
	DRAWPASS drawpass,
	BaseDraw* bd,
	BaseDrawHelp* bh) {
	
	
	Bool isActive = op->GetDeformMode();

	if (isActive) {
		BaseContainer* data = op->GetDataInstance();

		float W = (float)data->GetFloat(SIZE_X);
		float H = (float)data->GetFloat(SIZE_Y);
		float D = (float)data->GetFloat(SIZE_Z);

		Vector offset(W / 2, H / 2, D / 2);

		Vector color(1, 1, 1);
		Matrix m = bh->GetMg();
		bd->SetMatrix_Matrix(NULL, m);
		bd->SetPen(color);
		bd->SetDrawParam(DRAW_PARAMETER_SETZ, DRAW_Z_LOWEREQUAL);

		
		Int32 drawflag = 0;

		// Front
		bd->DrawLine(Vector(0, 0, 0) - offset, Vector(W, 0, 0) - offset, drawflag);
		bd->DrawLine(Vector(W, 0, 0) - offset, Vector(W, H, 0) - offset, drawflag);
		bd->DrawLine(Vector(W, H, 0) - offset, Vector(0, H, 0) - offset, drawflag);
		bd->DrawLine(Vector(0, H, 0) - offset, Vector(0, 0, 0) - offset, drawflag);

		//Connect
		bd->DrawLine(Vector(0, 0, 0) - offset, Vector(0, 0, D) - offset, drawflag);
		bd->DrawLine(Vector(W, 0, 0) - offset, Vector(W, 0, D) - offset, drawflag);
		bd->DrawLine(Vector(W, H, 0) - offset, Vector(W, H, D) - offset, drawflag);
		bd->DrawLine(Vector(0, H, 0) - offset, Vector(0, H, D) - offset, drawflag);

		//Back
		bd->DrawLine(Vector(0, 0, D) - offset, Vector(W, 0, D) - offset, drawflag);
		bd->DrawLine(Vector(W, 0, D) - offset, Vector(W, H, D) - offset, drawflag);
		bd->DrawLine(Vector(W, H, D) - offset, Vector(0, H, D) - offset, drawflag);
		bd->DrawLine(Vector(0, H, D) - offset, Vector(0, 0, D) - offset, drawflag);

		int gridlines = 4;
		float t;
		for (int i = 1; i < gridlines; i++) {
			t = float(i) / float(gridlines);
			bd->DrawLine(Vector(0, t*H, 0) - offset, Vector(W, t*H, 0) - offset, drawflag);
			bd->DrawLine(Vector(t*W, 0, 0) - offset, Vector(t*W, H, 0) - offset, drawflag);
		}
	}

	return ObjectData::Draw(op, drawpass, bd, bh);
}


Int32 Place3DTextureWidget::GetHandleCount(BaseObject* op) {
	return 3;
}

void Place3DTextureWidget::GetHandle(BaseObject* op, Int32 i, HandleInfo& info) {
	BaseContainer* data = op->GetDataInstance();
	float W = (float)data->GetFloat(SIZE_X);
	float H = (float)data->GetFloat(SIZE_Y);
	float D = (float)data->GetFloat(SIZE_Z);

	Vector offset(W / 2, H / 2, D / 2);

	if (i == 0) {
		info.position = Vector(W, H/2, D/2)-offset;
		info.direction = Vector(1.0, 0, 0);
		info.type = HANDLECONSTRAINTTYPE::LINEAR;
	}
	else if (i == 1) {
		info.position = Vector(W/2, H, D/2)-offset;
		info.direction = Vector(0.0, 1.0, 0);
		info.type = HANDLECONSTRAINTTYPE::LINEAR;

	}
	else if (i == 2) {
		info.position = Vector(W/2, H/2, D)-offset;
		info.direction = Vector(0.0, 0.0, 1.0);
		info.type = HANDLECONSTRAINTTYPE::LINEAR;
	}
}

void Place3DTextureWidget::SetHandle(BaseObject* op, Int32 i, Vector p, const HandleInfo& info) {
	BaseContainer* data = op->GetDataInstance();

	if (i == 0) {
		data->SetFloat(SIZE_X, 2*abs(p.x));
	}
	else if (i == 1) {
		data->SetFloat(SIZE_Y, 2*abs(p.y));
	}
	else if (i == 2) {
		data->SetFloat(SIZE_Z, 2*abs(p.z));
	}
}

Bool RegisterPlace3DTextureWidget(void) {
	return RegisterObjectPlugin(ID_PLACE3DTEXTUREWIDGET,
		"Place 3D Texture widget"_s,
		OBJECT_GENERATOR| PLUGINFLAG_HIDEPLUGINMENU,
		Place3DTextureWidget::Alloc,
		"Oplace3dtexturewidget"_s,
		AutoBitmap("Place3dTexture.png"_s),
		0);

	
}

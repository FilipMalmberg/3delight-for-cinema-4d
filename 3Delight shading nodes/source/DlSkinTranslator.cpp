#include "DlSkinTranslator.h"
#include "DlSkin.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlSkinTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlSkin.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord" );
}

void DlSkinTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlSkinTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

	{
		v = toLinear(data->GetVector(PARAM_SKIN_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("skin_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("roughness", f));

		f = (float) data->GetFloat(PARAM_SPECULAR_LEVEL);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("specular_level", f));

	{
		v = toLinear(data->GetVector(PARAM_SSS_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("sss_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_SSS_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("sss_scale", f));

		f = (float) data->GetFloat(PARAM_SSS_IOR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("sss_ior", f));

		f = (float) data->GetFloat(PARAM_SSS_ANISOTROPY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("sss_anisotropy", f));

		{
			String s = data->GetString(PARAM_SSS_MERGE_SET);
			ctx.SetAttribute(shader_handle, NSI::StringArg("sss_merge_set", StringToStdString(s)));
		}

		{
			int value = (int)data->GetBool(PARAM_SSS_DOMINANT_MATERIAL);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("sss_dominant_material", value));
		}

		{
			int value = (int)data->GetBool(PARAM_SSS_DOUBLE_SIDED);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("sss_double_sided", value));
		}

		f = (float) data->GetFloat(PARAM_SHEEN_LEVEL);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("sheen_level", f));

		f = (float) data->GetFloat(PARAM_SHEEN_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("sheen_roughness", f));

	{
		v = toLinear(data->GetVector(PARAM_SHEEN_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("sheen_color", &v_data[0]));
	}

		{
			Int32 option = data->GetInt32(PARAM_DISP_NORMAL_BUMP_TYPE);
			if(option == PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 0));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_NORMAL_MAP__DIRECTX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 1));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_NORMAL_MAP__OPENGL_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 2));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_DISPLACEMENT__0_0_CENTERED_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 3));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_DISPLACEMENT__0_5_CENTERED_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 4));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_DISP_NORMAL_BUMP_VALUE), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("disp_normal_bump_value", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("disp_normal_bump_intensity", f));

		f = (float) data->GetFloat(PARAM_OCCLUSION_DISTANCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("occlusion_distance", f));

}

void  DlSkinTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SKIN_COLOR, "skin_color");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_ROUGHNESS, "roughness");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SPECULAR_LEVEL, "specular_level");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SSS_COLOR, "sss_color");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SSS_SCALE, "sss_scale");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SSS_IOR, "sss_ior");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SSS_ANISOTROPY, "sss_anisotropy");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SSS_MERGE_SET, "sss_merge_set");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SSS_DOMINANT_MATERIAL, "sss_dominant_material");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SSS_DOUBLE_SIDED, "sss_double_sided");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SHEEN_LEVEL, "sheen_level");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SHEEN_ROUGHNESS, "sheen_roughness");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_SHEEN_COLOR, "sheen_color");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_DISP_NORMAL_BUMP_TYPE, "disp_normal_bump_type");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_DISP_NORMAL_BUMP_VALUE, "disp_normal_bump_value");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_DISP_NORMAL_BUMP_INTENSITY, "disp_normal_bump_intensity");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_OCCLUSION_DISTANCE, "occlusion_distance");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_UVCOORD, "uvCoord");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLSKIN, PARAM_OUTCOLOR, "outColor");
}

#include "DlSolidRampTranslator.h"
#include "DlSolidRamp.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlSolidRampTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlSolidRamp.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("3dlfc4d::default_placement_matrix", "out", shader_handle, "placementMatrix" );
}

void DlSolidRampTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlSolidRampTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			Int32 option = data->GetInt32(PARAM_SHAPE);
			if(option == PARAM_SHAPE_SPHERE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("shape", 0));
			}
			else if(option == PARAM_SHAPE_CUBE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("shape", 1));
			}
			else if(option == PARAM_SHAPE_CYLINDER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("shape", 2));
			}
			else if(option == PARAM_SHAPE_CONE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("shape", 3));
			}
			else if(option == PARAM_SHAPE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("shape", 4));
			}
		}

		{
			Gradient* gradient = (Gradient*)data->GetCustomDataType(PARAM_COLORRAMP_COLORVALUE, CUSTOMDATATYPE_GRADIENT );

			int knot_count;
			std::vector<float> knots;
			std::vector<float> colors;
			std::vector<int> interpolations;
			FillColorRampNSIData(gradient, doc, knots, colors, interpolations, knot_count);

			NSI::Argument arg_knot_color("colorRamp_ColorValue");
			arg_knot_color.SetArrayType(NSITypeColor, knot_count);
			arg_knot_color.SetValuePointer((void*)&colors[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_color));

			NSI::Argument arg_knot_position("colorRamp_Position");
			arg_knot_position.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_position.SetValuePointer((void*)&knots[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_position));

			NSI::Argument arg_knot_interpolation("colorRamp_Interp");
			arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);
			arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_interpolation));

		}

		{
			SplineData* spline = (SplineData*)data->GetCustomDataType(PARAM_OPACITYRAMP_FLOATVALUE, CUSTOMDATATYPE_SPLINE);

			int knot_count;
			std::vector<float> knots;
			std::vector<float> values;
			std::vector<int> interpolations;
			FillFloatRampNSIData(spline, doc, knots, values, interpolations, knot_count);

			NSI::Argument arg_knot_values("opacityRamp_FloatValue");
			arg_knot_values.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_values.SetValuePointer((void*)&values[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_values));

			NSI::Argument arg_knot_position("opacityRamp_Position");
			arg_knot_position.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_position.SetValuePointer((void*)&knots[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_position));

			NSI::Argument arg_knot_interpolation("opacityRamp_Interp");
			arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);
			arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_interpolation));

		}

		{
			Int32 option = data->GetInt32(PARAM_REVERSE_RAMPS);
			if(option == PARAM_REVERSE_RAMPS_NONE){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("reverse_ramps", 0));
			}
			else if(option == PARAM_REVERSE_RAMPS_BOTH){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("reverse_ramps", 1));
			}
			else if(option == PARAM_REVERSE_RAMPS_COLOR_RAMP){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("reverse_ramps", 2));
			}
			else if(option == PARAM_REVERSE_RAMPS_OPACITY_RAMP){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("reverse_ramps", 3));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_COLOR_BG), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("color_bg", &v_data[0]));
	}

		{
			Int32 option = data->GetInt32(PARAM_OUTSIDE);
			if(option == PARAM_OUTSIDE_ONLY_BACKGROUND){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("outside", 0));
			}
			else if(option == PARAM_OUTSIDE_REPEAT_RAMP){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("outside", 1));
			}
		}

		{
			Int32 option = data->GetInt32(PARAM_SPACE);
			if(option == PARAM_SPACE_WORLD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 0));
			}
			else if(option == PARAM_SPACE_OBJECT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 1));
			}
		}

		f = (float) data->GetFloat(PARAM_DISTORTION_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("distortion_intensity", f));

	{
		v = toLinear(data->GetVector(PARAM_DISTORTION), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("distortion", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_NOISE_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_intensity", f));

		f = (float) data->GetFloat(PARAM_NOISE_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_scale", f));

		f = (float) data->GetFloat(PARAM_NOISE_STEPPING);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_stepping", f));

		f = (float) data->GetFloat(PARAM_NOISE_TIME);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_time", f));

		f = (float) data->GetFloat(PARAM_NOISE_HUE_VARIATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_hue_variation", f));

		f = (float) data->GetFloat(PARAM_NOISE_SATURATION_VARIATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_saturation_variation", f));

		f = (float) data->GetFloat(PARAM_NOISE_VALUE_VARIATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_value_variation", f));

		f = (float) data->GetFloat(PARAM_NOISE_OPACITY_VARIATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_opacity_variation", f));

		{
			int value = (int)data->GetInt32(PARAM_LAYERS);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("layers", value));
		}

		f = (float) data->GetFloat(PARAM_LAYER_PERSISTENCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layer_persistence", f));

		f = (float) data->GetFloat(PARAM_LAYER_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layer_scale", f));

		{
			int value = (int)data->GetBool(PARAM_INVERT);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("invert", value));
		}

	{
		Matrix m = data->GetMatrix(PARAM_PLACEMENTMATRIX);
		std::vector<float> mdata = MatrixToNSIMatrix(m);
		NSI::Argument matrix_arg("placementMatrix");
		matrix_arg.SetType(NSITypeMatrix);
		matrix_arg.SetValuePointer((void*)&mdata[0]);
		ctx.SetAttribute(shader_handle, matrix_arg);
	}

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTALPHA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outAlpha", f));

}

void  DlSolidRampTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_SHAPE, "shape");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_COLORRAMP_POSITION, "colorRamp_Position");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_COLORRAMP_COLORVALUE, "colorRamp_ColorValue");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_COLORRAMP_INTERP, "colorRamp_Interp");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_OPACITYRAMP_POSITION, "opacityRamp_Position");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_OPACITYRAMP_FLOATVALUE, "opacityRamp_FloatValue");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_OPACITYRAMP_INTERP, "opacityRamp_Interp");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_REVERSE_RAMPS, "reverse_ramps");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_COLOR_BG, "color_bg");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_OUTSIDE, "outside");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_SPACE, "space");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_DISTORTION_INTENSITY, "distortion_intensity");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_DISTORTION, "distortion");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_NOISE_INTENSITY, "noise_intensity");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_NOISE_SCALE, "noise_scale");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_NOISE_STEPPING, "noise_stepping");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_NOISE_TIME, "noise_time");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_NOISE_HUE_VARIATION, "noise_hue_variation");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_NOISE_SATURATION_VARIATION, "noise_saturation_variation");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_NOISE_VALUE_VARIATION, "noise_value_variation");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_NOISE_OPACITY_VARIATION, "noise_opacity_variation");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_LAYERS, "layers");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_LAYER_PERSISTENCE, "layer_persistence");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_LAYER_SCALE, "layer_scale");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_INVERT, "invert");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_PLACEMENTMATRIX, "placementMatrix");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DLSOLIDRAMP, PARAM_OUTALPHA, "outAlpha");
}

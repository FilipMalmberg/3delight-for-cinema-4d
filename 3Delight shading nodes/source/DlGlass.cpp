//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlGlass.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlGlass : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlGlass); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlGlass::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_REFLECT_COLOR , Vector(0.6, 0.6, 0.6));
	data->SetFloat(PARAM_REFLECT_ROUGHNESS , 0);
	data->SetFloat(PARAM_REFLECT_IOR , 1.3);
	data->SetBool(PARAM_THIN_FILM , 0);
	data->SetFloat(PARAM_FILM_THICKNESS , 0.25);
	data->SetFloat(PARAM_FILM_IOR , 1.5);
	data->SetVector(PARAM_I_COLOR , Vector(1, 1, 1));
	data->SetFloat(PARAM_REFRACT_ROUGHNESS , 0);
	data->SetFloat(PARAM_REFRACT_IOR , 1.3);
	data->SetBool(PARAM_TRANSMIT_AOVS , 0);
	data->SetBool(PARAM_VOLUMETRIC_ENABLE , 0);
	data->SetFloat(PARAM_VOLUMETRIC_DENSITY , 1);
	data->SetVector(PARAM_VOLUMETRIC_SCATTERING_COLOR , Vector(0, 0, 0));
	data->SetVector(PARAM_VOLUMETRIC_TRANSPARENCY_COLOR , Vector(1, 1, 1));
	data->SetVector(PARAM_INCANDESCENCE , Vector(0, 0, 0));
	data->SetFloat(PARAM_INCANDESCENCE_INTENSITY , 1);
	data->SetVector(PARAM_INCANDESCENCE_MULTIPLIER , Vector(1, 1, 1));
	data->SetInt32(PARAM_DISP_NORMAL_BUMP_TYPE , PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP);
	data->SetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY , 1);
	data->SetFloat(PARAM_OCCLUSION_DISTANCE , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlGlass(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLGLASS, "Glass"_s, 0, DlGlass::Alloc, "dlGlass"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

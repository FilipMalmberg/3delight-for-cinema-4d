#include "DlRandomMaterialTranslator.h"
#include "DlRandomMaterial.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlRandomMaterialTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlRandomMaterial.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord" );
}

void DlRandomMaterialTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlRandomMaterialTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		f = (float) data->GetFloat(PARAM_IMPORTANCE_1);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("importance_1", f));

		f = (float) data->GetFloat(PARAM_IMPORTANCE_2);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("importance_2", f));

		f = (float) data->GetFloat(PARAM_IMPORTANCE_3);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("importance_3", f));

		f = (float) data->GetFloat(PARAM_IMPORTANCE_4);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("importance_4", f));

		f = (float) data->GetFloat(PARAM_IMPORTANCE_5);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("importance_5", f));

		{
			int value = (int)data->GetInt32(PARAM_SEED);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("seed", value));
		}

}

void  DlRandomMaterialTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_MATERIAL_1, "material_1");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_IMPORTANCE_1, "importance_1");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_MATERIAL_2, "material_2");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_IMPORTANCE_2, "importance_2");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_MATERIAL_3, "material_3");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_IMPORTANCE_3, "importance_3");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_MATERIAL_4, "material_4");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_IMPORTANCE_4, "importance_4");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_MATERIAL_5, "material_5");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_IMPORTANCE_5, "importance_5");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_SEED, "seed");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_UVCOORD, "uvCoord");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLRANDOMMATERIAL, PARAM_OUTCOLOR, "outColor");
}

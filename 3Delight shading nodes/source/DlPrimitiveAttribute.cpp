//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlPrimitiveAttribute.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlPrimitiveAttribute : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlPrimitiveAttribute); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlPrimitiveAttribute::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetString(PARAM_ATTRIBUTE_NAME, String(""));
	data->SetInt32(PARAM_ATTRIBUTE_TYPE , PARAM_ATTRIBUTE_TYPE_FLOAT);
	data->SetVector(PARAM_FALLBACK_COLOR , Vector(0, 0, 0));
	data->SetVector(PARAM_FALLBACK_VALUE , Vector(0, 0, 0));

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlPrimitiveAttribute(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLPRIMITIVEATTRIBUTE, "dlPrimitiveAttribute"_s, 0, DlPrimitiveAttribute::Alloc, "dlPrimitiveAttribute"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

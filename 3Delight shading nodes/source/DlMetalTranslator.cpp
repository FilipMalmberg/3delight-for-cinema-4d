#include "DlMetalTranslator.h"
#include "DlMetal.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlMetalTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlMetal.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord" );
}

void DlMetalTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlMetalTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		f = (float) data->GetFloat(PARAM_COATING_THICKNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("coating_thickness", f));

	{
		v = toLinear(data->GetVector(PARAM_COATING_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("coating_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_COATING_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("coating_roughness", f));

		f = (float) data->GetFloat(PARAM_COATING_SPECULAR_LEVEL);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("coating_specular_level", f));

	{
		v = toLinear(data->GetVector(PARAM_I_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("i_color", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_EDGE_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("edge_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("roughness", f));

		f = (float) data->GetFloat(PARAM_ANISOTROPY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("anisotropy", f));

	{
		v = toLinear(data->GetVector(PARAM_ANISOTROPY_DIRECTION), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("anisotropy_direction", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("opacity", f));

		f = (float) data->GetFloat(PARAM_THIN_FILM_THICKNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("thin_film_thickness", f));

		f = (float) data->GetFloat(PARAM_THIN_FILM_IOR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("thin_film_ior", f));

		{
			Int32 option = data->GetInt32(PARAM_DISP_NORMAL_BUMP_TYPE);
			if(option == PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 0));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_NORMAL_MAP__DIRECTX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 1));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_NORMAL_MAP__OPENGL_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 2));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_DISPLACEMENT__0_0_CENTERED_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 3));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_DISPLACEMENT__0_5_CENTERED_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 4));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_DISP_NORMAL_BUMP_VALUE), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("disp_normal_bump_value", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("disp_normal_bump_intensity", f));

		{
			Int32 option = data->GetInt32(PARAM_NORMAL_BUMP_AFFECT_LAYER);
			if(option == PARAM_NORMAL_BUMP_AFFECT_LAYER_BOTH_LAYERS){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("normal_bump_affect_layer", 0));
			}
			else if(option == PARAM_NORMAL_BUMP_AFFECT_LAYER_COATING_LAYER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("normal_bump_affect_layer", 1));
			}
			else if(option == PARAM_NORMAL_BUMP_AFFECT_LAYER_BASE_LAYER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("normal_bump_affect_layer", 2));
			}
		}

		f = (float) data->GetFloat(PARAM_OCCLUSION_DISTANCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("occlusion_distance", f));

}

void  DlMetalTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_COATING_THICKNESS, "coating_thickness");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_COATING_COLOR, "coating_color");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_COATING_ROUGHNESS, "coating_roughness");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_COATING_SPECULAR_LEVEL, "coating_specular_level");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_I_COLOR, "i_color");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_EDGE_COLOR, "edge_color");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_ROUGHNESS, "roughness");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_ANISOTROPY, "anisotropy");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_ANISOTROPY_DIRECTION, "anisotropy_direction");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_OPACITY, "opacity");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_THIN_FILM_THICKNESS, "thin_film_thickness");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_THIN_FILM_IOR, "thin_film_ior");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_DISP_NORMAL_BUMP_TYPE, "disp_normal_bump_type");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_DISP_NORMAL_BUMP_VALUE, "disp_normal_bump_value");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_DISP_NORMAL_BUMP_INTENSITY, "disp_normal_bump_intensity");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_NORMAL_BUMP_AFFECT_LAYER, "normal_bump_affect_layer");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_OCCLUSION_DISTANCE, "occlusion_distance");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_UVCOORD, "uvCoord");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLMETAL, PARAM_OUTCOLOR, "outColor");
}

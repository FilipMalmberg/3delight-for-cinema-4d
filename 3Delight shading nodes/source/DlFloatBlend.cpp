//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlFloatBlend.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlFloatBlend : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlFloatBlend); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlFloatBlend::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetInt32(PARAM_BLENDMODE , PARAM_BLENDMODE_OVER);
	data->SetFloat(PARAM_BLEND , 1);
	data->SetFloat(PARAM_FG , 1);
	data->SetFloat(PARAM_BG , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlFloatBlend(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLFLOATBLEND, "Float Blend"_s, 0, DlFloatBlend::Alloc, "dlFloatBlend"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

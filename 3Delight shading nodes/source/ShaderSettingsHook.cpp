#include "ShaderSettingsHook.h"
#include "delightenvironment.h"
#include "nsi.hpp"
using namespace std;

void ShaderSettingsHook::CreateNSINodes(BaseDocument* doc, DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	// Create an attributes node, and connect it to the mesh
	std::string attributes_handle = std::string("3dlfc4d::root_attributes");
	ctx.Create(attributes_handle, "attributes");
	ctx.Connect(attributes_handle, "", ".root", "geometryattributes");
	// Create a shader for the mesh and connect it to the geometry attributes of
	// the mesh
	std::string shader_handle("3dlfc4d::default_material");
	ctx.Create(shader_handle, "shader");
	ctx.Connect(shader_handle, "", attributes_handle, "surfaceshader");
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlPrincipled.oso";
	ctx.SetAttribute(shader_handle,
					 (NSI::StringArg("shaderfilename", shaderpath)));

	
	std::string default_placement_handle("3dlfc4d::default_placement_matrix");
	ctx.Create(default_placement_handle, "shader");
	shaderpath = delightpath + (std::string) "/cinema4d/osl" + (std::string) "/defaultPlacementMatrix.oso";
	ctx.SetAttribute(default_placement_handle, (NSI::StringArg("shaderfilename", shaderpath)));
}

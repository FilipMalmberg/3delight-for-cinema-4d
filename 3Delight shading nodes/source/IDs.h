#pragma once

//Obtained from plugincafe:
#define DL_NODES_CLASS	1061282
#define DL_MATERIAL_GROUP 1062126
#define DL_THREEDTEXTURE_GROUP 1063206
#define DL_TWODTEXTURE_GROUP 1063207
#define DL_VOLUME_GROUP 1063208
#define DL_UTILITIES_GROUP 1063209

#define DL_CLOSURE_TYPE 1056055

#define DL_TERMINAL 1062098

#define ID_DLPRINCIPLED 1063179
#define ID_DLTEXTURE 1063180
#define ID_DLSKIN 1063181
#define ID_DLGLASS 1063182
#define ID_DLMETAL 1063183
#define ID_DLCARPAINT 1063184
#define ID_DLTOON 1063197
#define ID_DLTOONGLASS 1063198
#define ID_DLLAYERED 1063199
#define ID_DLHAIRANDFUR 1063200
#define ID_DLCONSTANT 1063201
#define ID_DLLAYEREDMATERIAL 1063202
#define ID_DLTHIN 1064462
#define ID_DLRANDOMMATERIAL 1064461
#define ID_DLSUBSTANCE 1064460

#define ID_DLTRIPLANAR 1063185
#define ID_DLSHADOWMATTE 1063186
#define ID_DLPRIMITIVEATTRIBUTE 1063187
#define ID_COLORCORRECTION 1063188
#define ID_DLREMAP 1063189
#define ID_DLRAMP 1064194
#define ID_DLSOLIDRAMP 1064538
#define ID_DLNOISE 1064465
#define ID_DLWORLEYNOISE 1064582
#define ID_DLBOXNOISE 1064583
#define ID_DLFLAKES 1064584
#define ID_DLTILES 1064585

#define ID_DLFLOATBLEND 1063234
#define ID_DLFLOATMATH 1063235	
#define ID_DLCOLORBLEND 1063236
#define ID_DLCOLORBLENDMULTI 1063237
#define ID_DLFLOAT 1064562
#define ID_DLCOLOR 	1064561
#define ID_DLFACINGRATIO 1064707
#define ID_DLDISPLACEMENTBLEND 1064708

#define ID_TEXTUREAXIS	1064204
#define ID_LOCATOR		1064215
#define ID_PLACE3DTEXTUREWIDGET 1064567
#define ID_PLACE3DTEXTURE	1064577
#define ID_DECAL 1064835
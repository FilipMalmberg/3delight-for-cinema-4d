#include "DlTextureTranslator.h"
#include "DlTexture.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlTextureTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlTexture.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord" );
}

void DlTextureTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlTextureTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			Filename fn = data->GetFilename(PARAM_TEXTUREFILE);
			String s = fn.GetString();
			ctx.SetAttribute(shader_handle, NSI::StringArg("textureFile", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_TEXTUREFILE_META_COLORSPACE);
			if(option == PARAM_TEXTUREFILE_META_COLORSPACE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::StringArg("textureFile_meta_colorspace", "auto"));
			}
			else if(option == PARAM_TEXTUREFILE_META_COLORSPACE_SRGB){
				ctx.SetAttribute(shader_handle, NSI::StringArg("textureFile_meta_colorspace", "sRGB"));
			}
			else if(option == PARAM_TEXTUREFILE_META_COLORSPACE_REC__709){
				ctx.SetAttribute(shader_handle, NSI::StringArg("textureFile_meta_colorspace", "Rec. 709"));
			}
			else if(option == PARAM_TEXTUREFILE_META_COLORSPACE_LINEAR){
				ctx.SetAttribute(shader_handle, NSI::StringArg("textureFile_meta_colorspace", "linear"));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_DEFAULTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("defaultColor", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_TILEREMOVAL);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("tileRemoval", value));
		}

		f = (float) data->GetFloat(PARAM_TILESOFTNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tileSoftness", f));

		f = (float) data->GetFloat(PARAM_TILEOFFSET);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tileOffset", f));

		f = (float) data->GetFloat(PARAM_TILEROTATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tileRotation", f));

		f = (float) data->GetFloat(PARAM_TILESCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("tileScale", f));

		{
			Int32 option = data->GetInt32(PARAM_TONALADJUST);
			if(option == PARAM_TONALADJUST_OFF){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("tonalAdjust", 0));
			}
			else if(option == PARAM_TONALADJUST_NORMALIZE__AUTO_LEVELS_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("tonalAdjust", 1));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_COLORGAIN), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("colorGain", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_COLOROFFSET), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("colorOffset", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_ALPHAGAIN);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("alphaGain", f));

		f = (float) data->GetFloat(PARAM_ALPHAOFFSET);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("alphaOffset", f));

		{
			int value = (int)data->GetBool(PARAM_ALPHAISLUMINANCE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("alphaIsLuminance", value));
		}

		{
			int value = (int)data->GetBool(PARAM_INVERT);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("invert", value));
		}

		{
			int value = (int)data->GetBool(PARAM_USEIMAGESEQUENCE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("useImageSequence", value));
		}

		{
			int value = (int)data->GetInt32(PARAM_FRAME);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("frame", value));
		}

		{
			int value = (int)data->GetInt32(PARAM_FRAMEOFFSET);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("frameOffset", value));
		}

		f = (float) data->GetFloat(PARAM_FILTER);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("filter", f));

		f = (float) data->GetFloat(PARAM_BLUR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blur", f));

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTCOLORR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorR", f));

		f = (float) data->GetFloat(PARAM_OUTCOLORG);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorG", f));

		f = (float) data->GetFloat(PARAM_OUTCOLORB);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorB", f));

		f = (float) data->GetFloat(PARAM_OUTALPHA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outAlpha", f));

}

void  DlTextureTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_TEXTUREFILE, "textureFile");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_TEXTUREFILE_META_COLORSPACE, "textureFile_meta_colorspace");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_DEFAULTCOLOR, "defaultColor");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_TILEREMOVAL, "tileRemoval");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_TILESOFTNESS, "tileSoftness");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_TILEOFFSET, "tileOffset");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_TILEROTATION, "tileRotation");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_TILESCALE, "tileScale");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_TONALADJUST, "tonalAdjust");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_COLORGAIN, "colorGain");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_COLOROFFSET, "colorOffset");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_ALPHAGAIN, "alphaGain");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_ALPHAOFFSET, "alphaOffset");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_ALPHAISLUMINANCE, "alphaIsLuminance");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_INVERT, "invert");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_USEIMAGESEQUENCE, "useImageSequence");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_FRAME, "frame");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_FRAMEOFFSET, "frameOffset");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_FILTER, "filter");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_BLUR, "blur");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_UVCOORD, "uvCoord");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_OUTCOLORR, "outColorR");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_OUTCOLORG, "outColorG");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_OUTCOLORB, "outColorB");
	PM->RegisterAttributeName(ID_DLTEXTURE, PARAM_OUTALPHA, "outAlpha");
}

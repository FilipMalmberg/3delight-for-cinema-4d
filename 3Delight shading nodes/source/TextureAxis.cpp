//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "TextureAxis.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class TextureAxis : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(TextureAxis); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool TextureAxis::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();


	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterTextureAxis(const char* icon="dl_200.tif", int node_group= ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_TEXTUREAXIS, "TextureAxis"_s, 0, TextureAxis::Alloc, "TextureAxis"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

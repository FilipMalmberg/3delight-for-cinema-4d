//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlNoise.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlNoise : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlNoise); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlNoise::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetInt32(PARAM_NOISE_TYPE , PARAM_NOISE_TYPE_PERLIN);
	data->SetInt32(PARAM_NOISE_DIMENSION , PARAM_NOISE_DIMENSION_3D);
	data->SetFloat(PARAM_SCALE , 1);
	data->SetFloat(PARAM_I_TIME , 0);
	data->SetInt32(PARAM_SPACE , PARAM_SPACE_WORLD);
	data->SetInt32(PARAM_GABOR_TYPE , PARAM_GABOR_TYPE_ISOTROPIC);
	data->SetVector(PARAM_GABOR_DIRECTION , Vector(0.5, 0.5, 0.5));
	data->SetFloat(PARAM_GABOR_STRIPES , 0.5);
	data->SetInt32(PARAM_LAYERS , 5);
	data->SetFloat(PARAM_LAYER_PERSISTENCE , 0.6);
	data->SetFloat(PARAM_LAYER_SCALE , 0.5);
	data->SetFloat(PARAM_LAYER_CONTRAST , 1);
	data->SetInt32(PARAM_LAYER_INFLECTION , PARAM_LAYER_INFLECTION_OFF);
	data->SetInt32(PARAM_LAYER_BLEND_MODE , PARAM_LAYER_BLEND_MODE_ADD);
	data->SetVector(PARAM_DISTORTION , Vector(0.5, 0.5, 0.5));
	data->SetFloat(PARAM_DISTORTION_INTENSITY , 0);
	data->SetFloat(PARAM_SELF_DISTORTION , 0);
	data->SetFloat(PARAM_SELF_DISTORTION_SCALE , 1);
	data->SetFloat(PARAM_SELF_DISTORTION_ACCUM , 0);
	data->SetInt32(PARAM_NOISE_CHANNELS , PARAM_NOISE_CHANNELS_GRAY);
	data->SetInt32(PARAM_NOISE_RANGE , PARAM_NOISE_RANGE_0_TO_1);
	data->SetFloat(PARAM_FILTERING , 1);
	data->SetFloat(PARAM_AMPLITUDE , 1);
	data->SetFloat(PARAM_OFFSET , 0);
	data->SetFloat(PARAM_CLAMP_SOFTNESS , 0.2);
	data->SetFloat(PARAM_CONTRAST , 1);
	data->SetBool(PARAM_INVERT , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlNoise(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLNOISE, "Noise"_s, 0, DlNoise::Alloc, "dlNoise"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

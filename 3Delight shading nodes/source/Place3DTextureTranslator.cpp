#include "Place3DTextureTranslator.h"
#include "Place3DTexture.h"
#include "Oplace3dtexturewidget.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void Place3DTextureTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser) {
	NSI::Context& ctx(parser->GetContext());
	std::string shader_handle = (std::string) Handle;
	ctx.Create(shader_handle, "shader");
	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string  shaderpath = std::string(delightpath) + (std::string)"/cinema4d/osl/Place3DTexture.oso";
	ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void Place3DTextureTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser) {

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void Place3DTextureTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser) {

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	Int32 mode = data->GetInt32(MODE, MODE_NORMAL);

	std::string shader_handle = (std::string) Handle;

	BaseObject* widget = (BaseObject*)data->GetLink(WIDGET, doc, Obase);
	if (!widget) { return; }

	BaseContainer* widgetdata = widget->GetDataInstance();
	float W = (float)widgetdata->GetFloat(SIZE_X);
	float H = (float)widgetdata->GetFloat(SIZE_Y);
	float D = (float)widgetdata->GetFloat(SIZE_Z);
	

	ApplicationOutput(String::IntToString(mode));
	Matrix m = widget->GetMgn();
	m = ~m; //Invert matrix

	if(mode==MODE_NORMAL){
		Matrix scaling = Matrix();
		scaling.sqmat.v1.x = 1 / W;
		scaling.sqmat.v2.y = 1 / H;
		scaling.sqmat.v3.z = 1 / D;
		
		Matrix offset = Matrix();
		offset.off.x = W / 2;
		offset.off.y = H / 2;
		offset.off.z = D / 2;

		m = scaling * offset* m;
	
	}
	else if (mode == MODE_CENTERED) {
		Matrix scaling = Matrix();
		scaling.sqmat.v1.x = 2 / W;
		scaling.sqmat.v2.y = 2 / H;
		scaling.sqmat.v3.z = 2 / D;
		m = scaling *  m;

	}


	std::vector<float> v = MatrixToNSIMatrix(m);
	NSI::Argument matrix_arg("in");
	matrix_arg.SetType(NSITypeMatrix);
	matrix_arg.SetValuePointer((void*)&v[0]);
	ctx.SetAttribute(shader_handle, matrix_arg);

}

void  Place3DTextureTranslator::RegisterAttributeNames(DL_PluginManager* PM) {
	PM->RegisterAttributeName(ID_PLACE3DTEXTURE, OUT, "out");
}

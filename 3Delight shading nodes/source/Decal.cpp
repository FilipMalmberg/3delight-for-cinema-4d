//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "Decal.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class Decal : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(Decal); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool Decal::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetString(PARAM_COLOR_TEXTURE, String(""));
	data->SetInt32(PARAM_COLOR_TEXTURE_META_COLORSPACE, PARAM_COLOR_TEXTURE_META_COLORSPACE_AUTO);
	data->SetVector(PARAM_BACKGROUND_COLOR , Vector(0, 0, 0));
	data->SetString(PARAM_FLOAT_TEXTURE, String(""));
	data->SetInt32(PARAM_FLOAT_TEXTURE_META_COLORSPACE, PARAM_FLOAT_TEXTURE_META_COLORSPACE_LINEAR);
	data->SetFloat(PARAM_BACKGROUND_VALUE , 0);
	data->SetInt32(PARAM_SPACE , PARAM_SPACE_WORLD);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDecal(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DECAL, "Decal"_s, 0, Decal::Alloc, "Decal"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

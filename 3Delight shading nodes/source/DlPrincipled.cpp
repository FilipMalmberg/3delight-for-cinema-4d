//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlPrincipled.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlPrincipled : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlPrincipled); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlPrincipled::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_I_COLOR , Vector(0.8, 0.8, 0.8));
	data->SetFloat(PARAM_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_SPECULAR_LEVEL , 0.5);
	data->SetFloat(PARAM_METALLIC , 0);
	data->SetBool(PARAM_REFLECT_AOVS , 0);
	data->SetFloat(PARAM_ANISOTROPY , 0);
	data->SetVector(PARAM_ANISOTROPY_DIRECTION , Vector(0.5, 1, 0));
	data->SetFloat(PARAM_OPACITY , 1);
	data->SetFloat(PARAM_COATING_THICKNESS , 0);
	data->SetVector(PARAM_COATING_COLOR , Vector(1, 0.5, 0.1));
	data->SetFloat(PARAM_COATING_ROUGHNESS , 0);
	data->SetFloat(PARAM_COATING_SPECULAR_LEVEL , 0.5);
	data->SetFloat(PARAM_SSS_WEIGHT , 0);
	data->SetVector(PARAM_SSS_COLOR , Vector(0.5, 0.5, 0.5));
	data->SetFloat(PARAM_SSS_ANISOTROPY , 0);
	data->SetFloat(PARAM_SSS_SCALE , 0.1);
	data->SetBool(PARAM_SSS_INTERNAL_LIGHTING , 0);
	data->SetBool(PARAM_SSS_SIMPLE_COLOR_MODEL , 0);
	data->SetString(PARAM_SSS_MERGE_SET, String(""));
	data->SetBool(PARAM_SSS_DOMINANT_MATERIAL , 0);
	data->SetBool(PARAM_SSS_DOUBLE_SIDED , 0);
	data->SetFloat(PARAM_SHEEN_LEVEL , 0);
	data->SetFloat(PARAM_SHEEN_ROUGHNESS , 0.5);
	data->SetVector(PARAM_SHEEN_COLOR , Vector(1, 1, 1));
	data->SetFloat(PARAM_REFRACT_WEIGHT , 0);
	data->SetVector(PARAM_REFRACT_COLOR , Vector(1, 1, 1));
	data->SetFloat(PARAM_REFRACT_IOR , 1.3);
	data->SetFloat(PARAM_REFRACT_ROUGHNESS , 0);
	data->SetBool(PARAM_TRANSMIT_AOVS , 0);
	data->SetInt32(PARAM_VOLUMETRIC_ENABLE , 1);
	data->SetFloat(PARAM_VOLUMETRIC_DENSITY , 1);
	data->SetVector(PARAM_VOLUMETRIC_SCATTERING_COLOR , Vector(0, 0, 0));
	data->SetVector(PARAM_VOLUMETRIC_TRANSPARENCY_COLOR , Vector(1, 1, 1));
	data->SetVector(PARAM_INCANDESCENCE , Vector(0, 0, 0));
	data->SetFloat(PARAM_INCANDESCENCE_INTENSITY , 1);
	data->SetVector(PARAM_INCANDESCENCE_MULTIPLIER , Vector(1, 1, 1));
	data->SetInt32(PARAM_DISP_NORMAL_BUMP_TYPE , PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP);
	data->SetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY , 1);
	data->SetInt32(PARAM_NORMAL_BUMP_AFFECT_LAYER , PARAM_NORMAL_BUMP_AFFECT_LAYER_BOTH_LAYERS);
	data->SetFloat(PARAM_OCCLUSION_DISTANCE , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlPrincipled(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLPRINCIPLED, "Principled"_s, 0, DlPrincipled::Alloc, "dlPrincipled"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

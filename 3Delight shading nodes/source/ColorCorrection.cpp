//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "ColorCorrection.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class ColorCorrection : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(ColorCorrection); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool ColorCorrection::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_INPUT , Vector(1, 1, 1));
	data->SetFloat(PARAM_MASK , 1);
	data->SetFloat(PARAM_GAMMA , 1);
	data->SetFloat(PARAM_HUESHIFT , 0);
	data->SetFloat(PARAM_SATURATION , 1);
	data->SetFloat(PARAM_VIBRANCE , 1);
	data->SetFloat(PARAM_CONTRAST , 1);
	data->SetFloat(PARAM_CONTRASTPIVOT , 0.18);
	data->SetFloat(PARAM_EXPOSURE , 0);
	data->SetVector(PARAM_GAIN , Vector(1, 1, 1));
	data->SetVector(PARAM_OFFSET , Vector(0, 0, 0));
	data->SetBool(PARAM_INVERT , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterColorCorrection(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_COLORCORRECTION, "colorCorrection"_s, 0, ColorCorrection::Alloc, "colorCorrection"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlHairAndFur.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlHairAndFur : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlHairAndFur); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlHairAndFur::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetFloat(PARAM_EUMELANINE , 0.6);
	data->SetFloat(PARAM_PHENOMELANINE , 0.1);
	data->SetVector(PARAM_I_COLOR , Vector(0.8, 0.3, 0.3));
	data->SetFloat(PARAM_DYE_WEIGHT , 0);
	data->SetFloat(PARAM_SPECULAR_LEVEL , 0.5);
	data->SetFloat(PARAM_LONGITUDINAL_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_AZIMUTHAL_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_MEDULLA , 0);
	data->SetFloat(PARAM_SYNTHETIC , 0);
	data->SetFloat(PARAM_VARIATION_MELANIN , 0);
	data->SetFloat(PARAM_VARIATION_MELANIN_RED , 0);
	data->SetFloat(PARAM_VARIATION_WHITE_HAIR , 0);
	data->SetFloat(PARAM_VARIATION_DYE_HUE , 0);
	data->SetFloat(PARAM_VARIATION_DYE_SATURATION , 0);
	data->SetFloat(PARAM_VARIATION_DYE_VALUE , 0);
	data->SetFloat(PARAM_VARIATION_ROUGHNESS , 0);
	data->SetFloat(PARAM_VARIATION_SPECULAR_LEVEL , 0);
	data->SetFloat(PARAM_BOOST_GLOSSINESS , 0);
	data->SetFloat(PARAM_BOOST_SPECULAR , 0);
	data->SetFloat(PARAM_BOOST_TRANSMISSION , 0);
	data->SetFloat(PARAM_OCCLUSION_DISTANCE , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlHairAndFur(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLHAIRANDFUR, "Hair & Fur"_s, 0, DlHairAndFur::Alloc, "dlHairAndFur"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

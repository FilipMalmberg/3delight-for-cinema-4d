//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlConstant.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlConstant : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlConstant); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlConstant::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_I_COLOR , Vector(1, 1, 1));
	data->SetFloat(PARAM_INTENSITY , 1);
	data->SetFloat(PARAM_EXPOSURE , 0);
	data->SetFloat(PARAM_OPACITY , 1);
	data->SetBool(PARAM_OPACITY_CUTS_COLOR , 1);
	data->SetBool(PARAM_CASTS_SHADOWS , 1);
	data->SetBool(PARAM_DOUBLESIDED , 0);
	data->SetFloat(PARAM_SPREAD , 1);
	data->SetFloat(PARAM_DIFFUSE_CONTRIBUTION , 1);
	data->SetFloat(PARAM_REFLECTION_CONTRIBUTION , 1);
	data->SetFloat(PARAM_REFRACTION_CONTRIBUTION , 1);
	data->SetFloat(PARAM_VOLUME_CONTRIBUTION , 1);
	data->SetVector(PARAM_INCANDESCENCE_MULTIPLIER , Vector(1, 1, 1));

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlConstant(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLCONSTANT, "Constant"_s, 0, DlConstant::Alloc, "dlConstant"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

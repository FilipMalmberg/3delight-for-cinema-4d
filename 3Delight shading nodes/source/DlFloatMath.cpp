//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlFloatMath.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlFloatMath : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlFloatMath); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlFloatMath::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetInt32(PARAM_MODE , PARAM_MODE_ABSOLUTE);
	data->SetFloat(PARAM_INPUT1 , 0);
	data->SetFloat(PARAM_INPUT2 , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlFloatMath(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLFLOATMATH, "Float Math"_s, 0, DlFloatMath::Alloc, "dlFloatMath"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

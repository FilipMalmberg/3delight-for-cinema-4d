//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlColorBlendMulti.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlColorBlendMulti : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlColorBlendMulti); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlColorBlendMulti::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetBool(PARAM_ENABLE_1 , 1);
	data->SetString(PARAM_NAME_1, String(""));
	data->SetInt32(PARAM_BLENDMODE_1 , PARAM_BLENDMODE_1_OVER);
	data->SetFloat(PARAM_BLEND_1 , 1);
	data->SetVector(PARAM_INPUT_1 , Vector(1, 1, 1));
	data->SetBool(PARAM_ENABLE_2 , 0);
	data->SetString(PARAM_NAME_2, String(""));
	data->SetInt32(PARAM_BLENDMODE_2 , PARAM_BLENDMODE_2_OVER);
	data->SetFloat(PARAM_BLEND_2 , 1);
	data->SetVector(PARAM_INPUT_2 , Vector(1, 1, 1));
	data->SetBool(PARAM_ENABLE_3 , 0);
	data->SetString(PARAM_NAME_3, String(""));
	data->SetInt32(PARAM_BLENDMODE_3 , PARAM_BLENDMODE_3_OVER);
	data->SetFloat(PARAM_BLEND_3 , 1);
	data->SetVector(PARAM_INPUT_3 , Vector(1, 1, 1));
	data->SetBool(PARAM_ENABLE_4 , 0);
	data->SetString(PARAM_NAME_4, String(""));
	data->SetInt32(PARAM_BLENDMODE_4 , PARAM_BLENDMODE_4_OVER);
	data->SetFloat(PARAM_BLEND_4 , 1);
	data->SetVector(PARAM_INPUT_4 , Vector(1, 1, 1));
	data->SetBool(PARAM_ENABLE_5 , 0);
	data->SetString(PARAM_NAME_5, String(""));
	data->SetInt32(PARAM_BLENDMODE_5 , PARAM_BLENDMODE_5_OVER);
	data->SetFloat(PARAM_BLEND_5 , 1);
	data->SetVector(PARAM_INPUT_5 , Vector(1, 1, 1));
	data->SetBool(PARAM_ENABLE_6 , 0);
	data->SetString(PARAM_NAME_6, String(""));
	data->SetInt32(PARAM_BLENDMODE_6 , PARAM_BLENDMODE_6_OVER);
	data->SetFloat(PARAM_BLEND_6 , 1);
	data->SetVector(PARAM_INPUT_6 , Vector(1, 1, 1));
	data->SetBool(PARAM_ENABLE_7 , 0);
	data->SetString(PARAM_NAME_7, String(""));
	data->SetInt32(PARAM_BLENDMODE_7 , PARAM_BLENDMODE_7_OVER);
	data->SetFloat(PARAM_BLEND_7 , 1);
	data->SetVector(PARAM_INPUT_7 , Vector(1, 1, 1));
	data->SetBool(PARAM_ENABLE_8 , 0);
	data->SetString(PARAM_NAME_8, String(""));
	data->SetInt32(PARAM_BLENDMODE_8 , PARAM_BLENDMODE_8_OVER);
	data->SetFloat(PARAM_BLEND_8 , 1);
	data->SetVector(PARAM_INPUT_8 , Vector(1, 1, 1));
	data->SetBool(PARAM__CLAMP , 0);
	data->SetBool(PARAM_INVERT , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlColorBlendMulti(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLCOLORBLENDMULTI, "Color Blend Multi"_s, 0, DlColorBlendMulti::Alloc, "dlColorBlendMulti"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

#include "DlShadowMatteTranslator.h"
#include "DlShadowMatte.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlShadowMatteTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlShadowMatte.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void DlShadowMatteTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlShadowMatteTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		f = (float) data->GetFloat(PARAM_SHADOW_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("shadow_opacity", f));

	{
		v = toLinear(data->GetVector(PARAM_SHADOW_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("shadow_color", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_DIFFUSE_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("diffuse_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DIFFUSE_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("diffuse_intensity", f));

		f = (float) data->GetFloat(PARAM_DIFFUSE_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("diffuse_roughness", f));

		{
			int value = (int)data->GetBool(PARAM_SHOW_BACKGROUND);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("show_background", value));
		}

}

void  DlShadowMatteTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLSHADOWMATTE, PARAM_SHADOW_OPACITY, "shadow_opacity");
	PM->RegisterAttributeName(ID_DLSHADOWMATTE, PARAM_SHADOW_COLOR, "shadow_color");
	PM->RegisterAttributeName(ID_DLSHADOWMATTE, PARAM_DIFFUSE_COLOR, "diffuse_color");
	PM->RegisterAttributeName(ID_DLSHADOWMATTE, PARAM_DIFFUSE_INTENSITY, "diffuse_intensity");
	PM->RegisterAttributeName(ID_DLSHADOWMATTE, PARAM_DIFFUSE_ROUGHNESS, "diffuse_roughness");
	PM->RegisterAttributeName(ID_DLSHADOWMATTE, PARAM_SHOW_BACKGROUND, "show_background");
	PM->RegisterAttributeName(ID_DLSHADOWMATTE, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLSHADOWMATTE, PARAM_OUTCOLOR, "outColor");
}

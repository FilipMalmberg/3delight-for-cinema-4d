//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlSubstance.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlSubstance : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlSubstance); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlSubstance::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_I_COLOR , Vector(0.5, 0.5, 0.5));
	data->SetFloat(PARAM_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_SPECULAR_LEVEL , 0.5);
	data->SetFloat(PARAM_METALLIC , 0);
	data->SetFloat(PARAM_OPACITY , 1);
	data->SetVector(PARAM_EMISSIVE , Vector(0, 0, 0));
	data->SetFloat(PARAM_EMISSIVE_INTENSITY , 1);
	data->SetVector(PARAM_INCANDESCENCE_MULTIPLIER , Vector(1, 1, 1));
	data->SetInt32(PARAM_DISP_NORMAL_BUMP_TYPE , PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP);
	data->SetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY , 1);
	data->SetFloat(PARAM_OCCLUSION_DISTANCE , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlSubstance(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLSUBSTANCE, "Substance"_s, 0, DlSubstance::Alloc, "dlSubstance"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

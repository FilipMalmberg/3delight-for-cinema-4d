//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlSkin.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlSkin : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlSkin); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlSkin::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_SKIN_COLOR , Vector(0.788, 0.597, 0.491));
	data->SetFloat(PARAM_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_SPECULAR_LEVEL , 0.5);
	data->SetVector(PARAM_SSS_COLOR , Vector(0.9642, 0.3388, 0.216));
	data->SetFloat(PARAM_SSS_SCALE , 0.5);
	data->SetFloat(PARAM_SSS_IOR , 1.3);
	data->SetFloat(PARAM_SSS_ANISOTROPY , 0);
	data->SetString(PARAM_SSS_MERGE_SET, String(""));
	data->SetBool(PARAM_SSS_DOMINANT_MATERIAL , 0);
	data->SetBool(PARAM_SSS_DOUBLE_SIDED , 0);
	data->SetFloat(PARAM_SHEEN_LEVEL , 0);
	data->SetFloat(PARAM_SHEEN_ROUGHNESS , 0.5);
	data->SetVector(PARAM_SHEEN_COLOR , Vector(1, 1, 1));
	data->SetInt32(PARAM_DISP_NORMAL_BUMP_TYPE , PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP);
	data->SetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY , 1);
	data->SetFloat(PARAM_OCCLUSION_DISTANCE , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlSkin(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLSKIN, "Skin"_s, 0, DlSkin::Alloc, "dlSkin"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

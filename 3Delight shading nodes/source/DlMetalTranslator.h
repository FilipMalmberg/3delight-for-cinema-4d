//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#pragma once
#include "c4d.h"
#include "DL_TranslatorPlugin.h"
#include "DL_PluginManager.h"

class DlMetalTranslator : public DL_TranslatorPlugin {
public:
	virtual void CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser);

	virtual void ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser);

	virtual void SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser);

	static void RegisterAttributeNames(DL_PluginManager* PM);
};

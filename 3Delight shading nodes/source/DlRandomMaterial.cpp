//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlRandomMaterial.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlRandomMaterial : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlRandomMaterial); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlRandomMaterial::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetFloat(PARAM_IMPORTANCE_1 , 1);
	data->SetFloat(PARAM_IMPORTANCE_2 , 1);
	data->SetFloat(PARAM_IMPORTANCE_3 , 1);
	data->SetFloat(PARAM_IMPORTANCE_4 , 1);
	data->SetFloat(PARAM_IMPORTANCE_5 , 1);
	data->SetInt32(PARAM_SEED , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlRandomMaterial(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLRANDOMMATERIAL, "Random Material"_s, 0, DlRandomMaterial::Alloc, "dlRandomMaterial"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

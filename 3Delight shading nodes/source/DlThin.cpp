//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlThin.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlThin : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlThin); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlThin::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_I_COLOR , Vector(0.8, 0.8, 0.8));
	data->SetFloat(PARAM_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_SPECULAR_LEVEL , 0.5);
	data->SetVector(PARAM_COLOR_BACK , Vector(0.8, 0.8, 0.8));
	data->SetFloat(PARAM_ROUGHNESS_BACK , 0.3);
	data->SetFloat(PARAM_SPECULAR_LEVEL_BACK , 0.5);
	data->SetFloat(PARAM_TRANSLUCENCY , 0.5);
	data->SetFloat(PARAM_OPACITY , 1);
	data->SetInt32(PARAM_DISP_NORMAL_BUMP_TYPE , PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP);
	data->SetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY , 1);
	data->SetFloat(PARAM_OCCLUSION_DISTANCE , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlThin(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLTHIN, "Thin"_s, 0, DlThin::Alloc, "dlThin"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlLayeredMaterial.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlLayeredMaterial : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlLayeredMaterial); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlLayeredMaterial::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetFloat(PARAM_TOP_MASK , 1);
	data->SetFloat(PARAM_MIDDLE_MASK , 1);
	data->SetFloat(PARAM_BOTTOM_MASK , 1);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlLayeredMaterial(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLLAYEREDMATERIAL, "Layered"_s, 0, DlLayeredMaterial::Alloc, "dlLayeredMaterial"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

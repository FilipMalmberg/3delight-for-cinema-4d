//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlColorBlend.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlColorBlend : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlColorBlend); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlColorBlend::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetInt32(PARAM_BLENDMODE , PARAM_BLENDMODE_OVER);
	data->SetVector(PARAM_FG , Vector(1, 1, 1));
	data->SetVector(PARAM_BG , Vector(0, 0, 0));
	data->SetFloat(PARAM_BLEND , 1);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlColorBlend(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLCOLORBLEND, "Color Blend"_s, 0, DlColorBlend::Alloc, "dlColorBlend"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

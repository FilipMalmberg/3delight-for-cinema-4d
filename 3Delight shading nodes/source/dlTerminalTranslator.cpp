#include "dlTerminalTranslator.h"
#include "dlTerminal.h"
#include "IDs.h"
#include "nsi.hpp"
#include "delightenvironment.h"
#include <c4d_graphview.h>

void dlTerminalTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());

	std::string shader_handle = (std::string) Handle;
	ctx.Create(shader_handle, "shader");

	const char* delightpath = DelightEnv::getDelightEnvironment();
	std::string  shaderpath = std::string(delightpath) + (std::string) "/osl/dlTerminal.oso";

	ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void dlTerminalTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser)
{
	GvNode* gvnode = (GvNode*)C4DNode;
	GvNodeMaster* node_master = gvnode->GetNodeMaster();
	if (!node_master) { return; }

	BaseList2D* node_material = node_master->GetOwner();


	NSI::Context& ctx(parser->GetContext());
	std::string shader_handle = (std::string) Handle;
	std::string attribute_handle = parser->GetHandleName(node_material);


	//TODO: Only create these connections if they have an incoming port! 
	//Otherwise, displacement does not work for e.g., the principled shader

	ctx.Connect(shader_handle, "", attribute_handle, "surfaceshader");
	//ctx.Connect(shader_handle, "", attribute_handle, "displacementshader");
	//ctx.Connect(shader_handle, "", attribute_handle, "volumeshader");
	

}

void dlTerminalTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser)
{
	//GePrint(String("Terminal  shader sample attributes"));
}

void dlTerminalTranslator::RegisterAttributeNames(DL_PluginManager* PM) {
	PM->RegisterAttributeName(DL_TERMINAL, SURFACE, "Surface");
	PM->RegisterAttributeName(DL_TERMINAL, VOLUME, "Volume");
	PM->RegisterAttributeName(DL_TERMINAL, DISPLACEMENT, "Displacement");
}

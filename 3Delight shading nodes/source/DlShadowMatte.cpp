//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlShadowMatte.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlShadowMatte : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlShadowMatte); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlShadowMatte::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetFloat(PARAM_SHADOW_OPACITY , 1);
	data->SetVector(PARAM_SHADOW_COLOR , Vector(0, 0, 0));
	data->SetVector(PARAM_DIFFUSE_COLOR , Vector(1, 1, 1));
	data->SetFloat(PARAM_DIFFUSE_INTENSITY , 0.8);
	data->SetFloat(PARAM_DIFFUSE_ROUGHNESS , 0.3);
	data->SetBool(PARAM_SHOW_BACKGROUND , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlShadowMatte(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLSHADOWMATTE, "Shadow Matte"_s, 0, DlShadowMatte::Alloc, "dlShadowMatte"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

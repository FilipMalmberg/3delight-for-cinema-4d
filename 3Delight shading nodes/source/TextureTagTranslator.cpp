#include "TextureTagTranslator.h"
#include <vector>
#include "DL_TypeConversions.h"
#include "c4d.h"
#include "nsi.hpp"
using namespace std;

void TextureTagTranslator::CreateNSINodes(const char* Handle, const char*
ParentTransformHandle, BaseList2D* C4DNode, BaseDocument* doc, DL_SceneParser*
parser) {
		//NSI::Context ctx(parser->GetContext());

		//transform_handle = std::string(ParentTransformHandle);
	NSI::Context& ctx(parser->GetContext());
	std::string axis_attributes_handle = Handle + std::string("texture_axis_attribute");
	ctx.Create(axis_attributes_handle, "attributes");

}

void TextureTagTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	TextureTag* tag = (TextureTag*)C4DNode;
	std::string transform_handle = std::string(ParentTransformHandle);
	BaseContainer* tagdata = tag->GetDataInstance();
	String restriction = tagdata->GetString(TEXTURETAG_RESTRICTION);

	if (restriction != "") {
		BaseObject* object = tag->GetObject();
		std::string object_handle = parser->GetHandleName(object);

		if (!object->IsInstanceOf(Opolygon)) {
			return;
		}

		SelectionTag* polygonSelectionTag =
			(SelectionTag*)object->GetTag(Tpolygonselection);
		std::string facehandle = "";

		while (polygonSelectionTag) {
			if (polygonSelectionTag->GetType() != Tpolygonselection) {
				polygonSelectionTag = (SelectionTag*)polygonSelectionTag->GetNext();
				continue;
			}

			if (restriction == polygonSelectionTag->GetName()) {
				facehandle =
					parser->GetHandleName(polygonSelectionTag) + std::string("faceset");
				ctx.Connect(facehandle, "", object_handle, "facesets");
				BaseMaterial* material = tag->GetMaterial();

				if (material) {
					// std::string attributes_handle =
					// std::string(parser->GetAssociatedHandle((BaseList2D*)material));
					std::string attributes_handle =
						parser->GetHandleName((BaseList2D*)material);

					if (attributes_handle != "") {
						ctx.Connect(
							attributes_handle, "", facehandle, "geometryattributes");
					}

					//Connect texture axis
					std::string axis_attributes_handle = Handle + std::string("texture_axis_attribute");
					ctx.Connect(axis_attributes_handle, "", facehandle, "shaderattributes");
				}

				return;
			}

			polygonSelectionTag = (SelectionTag*)polygonSelectionTag->GetNext();
		}

	}
	else {
		BaseMaterial* material = tag->GetMaterial();

		if (material) {
			// std::string attributes_handle =
			// std::string(parser->GetAssociatedHandle((BaseList2D*)material));
			std::string attributes_handle =
				parser->GetHandleName((BaseList2D*)material);

			if (attributes_handle != "") {
				ctx.Connect(attributes_handle, "", transform_handle, "geometryattributes");
			}

			//Connect texture axis
			std::string axis_attributes_handle = Handle + std::string("texture_axis_attribute");
			ctx.Connect(axis_attributes_handle, "", transform_handle, "shaderattributes");
		}
	}
}

void TextureTagTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser) 
{
	NSI::Context& ctx(parser->GetContext());
	std::string axis_attributes_handle = Handle + std::string("texture_axis_attribute");
	
	TextureTag* texturetag = (TextureTag*)C4DNode;

	Matrix texture_axis = texturetag->GetMl();
	texture_axis = ~texture_axis; //Invert matrix

	std::vector<float> v = MatrixToNSIMatrix(texture_axis);
	
	NSI::Argument xform("textureAxis");
	xform.SetType(NSITypeMatrix);
	xform.SetValuePointer((void*)&v[0]);
	ctx.SetAttributeAtTime(axis_attributes_handle, info->sample_time, (xform));


}
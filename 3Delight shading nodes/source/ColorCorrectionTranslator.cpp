#include "ColorCorrectionTranslator.h"
#include "ColorCorrection.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void ColorCorrectionTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/colorCorrection.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void ColorCorrectionTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void ColorCorrectionTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

	{
		v = toLinear(data->GetVector(PARAM_INPUT), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("input", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_MASK);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("mask", f));

		f = (float) data->GetFloat(PARAM_GAMMA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("gamma", f));

		f = (float) data->GetFloat(PARAM_HUESHIFT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("hueShift", f));

		f = (float) data->GetFloat(PARAM_SATURATION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("saturation", f));

		f = (float) data->GetFloat(PARAM_VIBRANCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("vibrance", f));

		f = (float) data->GetFloat(PARAM_CONTRAST);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("contrast", f));

		f = (float) data->GetFloat(PARAM_CONTRASTPIVOT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("contrastPivot", f));

		f = (float) data->GetFloat(PARAM_EXPOSURE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("exposure", f));

	{
		v = toLinear(data->GetVector(PARAM_GAIN), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("gain", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_OFFSET), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("offset", &v_data[0]));
	}

		{
			int value = (int)data->GetBool(PARAM_INVERT);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("invert", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTCOLORR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorR", f));

		f = (float) data->GetFloat(PARAM_OUTCOLORG);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorG", f));

		f = (float) data->GetFloat(PARAM_OUTCOLORB);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outColorB", f));

}

void  ColorCorrectionTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_INPUT, "input");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_MASK, "mask");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_GAMMA, "gamma");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_HUESHIFT, "hueShift");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_SATURATION, "saturation");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_VIBRANCE, "vibrance");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_CONTRAST, "contrast");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_CONTRASTPIVOT, "contrastPivot");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_EXPOSURE, "exposure");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_GAIN, "gain");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_OFFSET, "offset");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_INVERT, "invert");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_OUTCOLORR, "outColorR");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_OUTCOLORG, "outColorG");
	PM->RegisterAttributeName(ID_COLORCORRECTION, PARAM_OUTCOLORB, "outColorB");
}

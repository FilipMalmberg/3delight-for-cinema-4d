#include "c4d.h"
#include "IDs.h"

//A dummy data type for OSL closures. 
//This type is only used to make connections in the shader graph, and does not hold any data of its own

class ClosureDataType : public CustomDataTypeClass {
	INSTANCEOF(ClosureDataType, CustomDataTypeClass)

public:
	virtual Int32 GetId();
	virtual CustomDataType* AllocData();
	virtual void FreeData(CustomDataType* data);
	virtual Bool CopyData(const CustomDataType* src, CustomDataType* dst, AliasTrans* aliastrans);
	virtual Int32 Compare(const CustomDataType* d1, const CustomDataType* d2);
	virtual Bool WriteData(const CustomDataType* t_d, HyperFile* hf);
	virtual Bool ReadData(CustomDataType* t_d, HyperFile* hf, Int32 level);
	virtual const Char* GetResourceSym();
	virtual void GetDefaultProperties(BaseContainer& data);
};

Int32 ClosureDataType::GetId() {
	return 	DL_CLOSURE_TYPE;
}

CustomDataType* ClosureDataType::AllocData() { return 0; }

void ClosureDataType::FreeData(CustomDataType* data) {  }

Bool ClosureDataType::CopyData(const CustomDataType* src, CustomDataType* dst, AliasTrans* aliastrans) { return true; }

Int32 ClosureDataType::Compare(const CustomDataType* d1, const CustomDataType* d2){ return 0; }

Bool ClosureDataType::WriteData(const CustomDataType* t_d, HyperFile* hf) { return true; }

Bool ClosureDataType::ReadData(CustomDataType* t_d, HyperFile* hf, Int32 level) { return true;  }

const Char* ClosureDataType::GetResourceSym() { return "CLOSURE"; }

void ClosureDataType::GetDefaultProperties(BaseContainer& data){}


Bool RegisterClosureDataType() {
	return RegisterCustomDataTypePlugin("Closure"_s,0, NewObjClear(ClosureDataType),0);
}
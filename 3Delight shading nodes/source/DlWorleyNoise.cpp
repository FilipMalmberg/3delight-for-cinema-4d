//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlWorleyNoise.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlWorleyNoise : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlWorleyNoise); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlWorleyNoise::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetInt32(PARAM_OUTPUT_TYPE , PARAM_OUTPUT_TYPE_VORONOI_1__F1_);
	data->SetInt32(PARAM_DISTANCE_TYPE , PARAM_DISTANCE_TYPE_EUCLIDEAN);
	data->SetFloat(PARAM_MINKOWSKI_N , 3);
	data->SetFloat(PARAM_SCALE , 1);
	data->SetFloat(PARAM_DENSITY , 1);
	data->SetFloat(PARAM_RANDOM_POS , 1);
	data->SetFloat(PARAM_I_TIME , 0);
	data->SetInt32(PARAM_SPACE , PARAM_SPACE_WORLD);
	{
		AutoAlloc<Gradient> gradient;
		if (!gradient) return false;
		GradientKnot k;
		k.col = Vector(0, 0, 0);
		k.pos = 0;
		gradient->InsertKnot(k);
		k.col = Vector(1, 1, 1);
		k.pos = 1;
		gradient->InsertKnot(k);
		data->SetData(PARAM_COLOR_IN_CELL_COLORVALUE, GeData(CUSTOMDATATYPE_GRADIENT, *gradient));
	}
	{
		AutoAlloc<Gradient> gradient;
		if (!gradient) return false;
		GradientKnot k;
		k.col = Vector(0, 0, 0);
		k.pos = 0;
		gradient->InsertKnot(k);
		k.col = Vector(1, 1, 1);
		k.pos = 1;
		gradient->InsertKnot(k);
		data->SetData(PARAM_COLOR_ACROSS_CELL_COLORVALUE, GeData(CUSTOMDATATYPE_GRADIENT, *gradient));
	}
	data->SetVector(PARAM_COLOR_EDGE , Vector(0, 0, 0));
	data->SetVector(PARAM_COLOR_BG , Vector(0, 0, 0));
	data->SetInt32(PARAM_COLOR_BLEND_MODE , PARAM_COLOR_BLEND_MODE_OVER);
	data->SetVector(PARAM_DISTORTION , Vector(0, 0, 0));
	data->SetFloat(PARAM_DISTORTION_INTENSITY , 1);
	data->SetFloat(PARAM_BORDER_THICKNESS , 0);
	data->SetFloat(PARAM_BORDER_SMOOTHNESS , 0);
	data->SetInt32(PARAM_LAYERS , 1);
	data->SetFloat(PARAM_LAYER_PERSISTENCE , 0.7);
	data->SetFloat(PARAM_LAYER_SCALE , 0.45);
	data->SetFloat(PARAM_LAYER_DENSITY , 0.8);
	data->SetFloat(PARAM_AMPLITUDE , 1);
	data->SetFloat(PARAM_CONTRAST , 1);
	data->SetBool(PARAM_INVERT , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlWorleyNoise(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLWORLEYNOISE, "Worley Noise"_s, 0, DlWorleyNoise::Alloc, "dlWorleyNoise"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

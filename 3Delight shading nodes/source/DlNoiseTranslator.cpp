#include "DlNoiseTranslator.h"
#include "DlNoise.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlNoiseTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlNoise.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord" );
		ctx.Connect("3dlfc4d::default_placement_matrix", "out", shader_handle, "placementMatrix" );
}

void DlNoiseTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlNoiseTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			Int32 option = data->GetInt32(PARAM_NOISE_TYPE);
			if(option == PARAM_NOISE_TYPE_PERLIN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("noise_type", 0));
			}
			else if(option == PARAM_NOISE_TYPE_SIMPLEX){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("noise_type", 1));
			}
			else if(option == PARAM_NOISE_TYPE_GABOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("noise_type", 2));
			}
		}

		{
			Int32 option = data->GetInt32(PARAM_NOISE_DIMENSION);
			if(option == PARAM_NOISE_DIMENSION_3D){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("noise_dimension", 0));
			}
			else if(option == PARAM_NOISE_DIMENSION_2D){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("noise_dimension", 1));
			}
		}

		f = (float) data->GetFloat(PARAM_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("scale", f));

		f = (float) data->GetFloat(PARAM_I_TIME);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("i_time", f));

		{
			Int32 option = data->GetInt32(PARAM_SPACE);
			if(option == PARAM_SPACE_WORLD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 0));
			}
			else if(option == PARAM_SPACE_OBJECT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("space", 1));
			}
		}

		{
			Int32 option = data->GetInt32(PARAM_GABOR_TYPE);
			if(option == PARAM_GABOR_TYPE_ISOTROPIC){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("gabor_type", 0));
			}
			else if(option == PARAM_GABOR_TYPE_ANISOTROPIC){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("gabor_type", 1));
			}
			else if(option == PARAM_GABOR_TYPE_ANISOTROPIC_HYBRID){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("gabor_type", 2));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_GABOR_DIRECTION), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("gabor_direction", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_GABOR_STRIPES);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("gabor_stripes", f));

		{
			int value = (int)data->GetInt32(PARAM_LAYERS);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("layers", value));
		}

		f = (float) data->GetFloat(PARAM_LAYER_PERSISTENCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layer_persistence", f));

		f = (float) data->GetFloat(PARAM_LAYER_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layer_scale", f));

		f = (float) data->GetFloat(PARAM_LAYER_CONTRAST);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layer_contrast", f));

		{
			Int32 option = data->GetInt32(PARAM_LAYER_INFLECTION);
			if(option == PARAM_LAYER_INFLECTION_OFF){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("layer_inflection", 0));
			}
			else if(option == PARAM_LAYER_INFLECTION_ON__SHARP_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("layer_inflection", 1));
			}
			else if(option == PARAM_LAYER_INFLECTION_ON__SOFT_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("layer_inflection", 2));
			}
		}

		{
			Int32 option = data->GetInt32(PARAM_LAYER_BLEND_MODE);
			if(option == PARAM_LAYER_BLEND_MODE_ADD){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("layer_blend_mode", 0));
			}
			else if(option == PARAM_LAYER_BLEND_MODE_TERRAIN){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("layer_blend_mode", 1));
			}
			else if(option == PARAM_LAYER_BLEND_MODE_MAX__SHARP_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("layer_blend_mode", 2));
			}
			else if(option == PARAM_LAYER_BLEND_MODE_MAX__SOFT_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("layer_blend_mode", 3));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_DISTORTION), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("distortion", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DISTORTION_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("distortion_intensity", f));

		f = (float) data->GetFloat(PARAM_SELF_DISTORTION);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("self_distortion", f));

		f = (float) data->GetFloat(PARAM_SELF_DISTORTION_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("self_distortion_scale", f));

		f = (float) data->GetFloat(PARAM_SELF_DISTORTION_ACCUM);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("self_distortion_accum", f));

		{
			Int32 option = data->GetInt32(PARAM_NOISE_CHANNELS);
			if(option == PARAM_NOISE_CHANNELS_GRAY){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("noise_channels", 0));
			}
			else if(option == PARAM_NOISE_CHANNELS_RGB){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("noise_channels", 1));
			}
		}

		{
			Int32 option = data->GetInt32(PARAM_NOISE_RANGE);
			if(option == PARAM_NOISE_RANGE_0_TO_1){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("noise_range", 0));
			}
			else if(option == PARAM_NOISE_RANGE__1_TO_1){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("noise_range", 1));
			}
		}

		f = (float) data->GetFloat(PARAM_FILTERING);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("filtering", f));

		f = (float) data->GetFloat(PARAM_AMPLITUDE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("amplitude", f));

		f = (float) data->GetFloat(PARAM_OFFSET);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("offset", f));

		f = (float) data->GetFloat(PARAM_CLAMP_SOFTNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("clamp_softness", f));

		f = (float) data->GetFloat(PARAM_CONTRAST);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("contrast", f));

		{
			int value = (int)data->GetBool(PARAM_INVERT);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("invert", value));
		}

	{
		Matrix m = data->GetMatrix(PARAM_PLACEMENTMATRIX);
		std::vector<float> mdata = MatrixToNSIMatrix(m);
		NSI::Argument matrix_arg("placementMatrix");
		matrix_arg.SetType(NSITypeMatrix);
		matrix_arg.SetValuePointer((void*)&mdata[0]);
		ctx.SetAttribute(shader_handle, matrix_arg);
	}

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTALPHA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outAlpha", f));

	{
		v = toLinear(data->GetVector(PARAM_OUTMASK), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outMask", &v_data[0]));
	}

}

void  DlNoiseTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_NOISE_TYPE, "noise_type");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_NOISE_DIMENSION, "noise_dimension");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_SCALE, "scale");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_I_TIME, "i_time");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_SPACE, "space");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_GABOR_TYPE, "gabor_type");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_GABOR_DIRECTION, "gabor_direction");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_GABOR_STRIPES, "gabor_stripes");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_LAYERS, "layers");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_LAYER_PERSISTENCE, "layer_persistence");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_LAYER_SCALE, "layer_scale");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_LAYER_CONTRAST, "layer_contrast");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_LAYER_INFLECTION, "layer_inflection");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_LAYER_BLEND_MODE, "layer_blend_mode");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_DISTORTION, "distortion");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_DISTORTION_INTENSITY, "distortion_intensity");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_SELF_DISTORTION, "self_distortion");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_SELF_DISTORTION_SCALE, "self_distortion_scale");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_SELF_DISTORTION_ACCUM, "self_distortion_accum");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_NOISE_CHANNELS, "noise_channels");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_NOISE_RANGE, "noise_range");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_FILTERING, "filtering");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_AMPLITUDE, "amplitude");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_OFFSET, "offset");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_CLAMP_SOFTNESS, "clamp_softness");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_CONTRAST, "contrast");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_INVERT, "invert");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_UVCOORD, "uvCoord");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_PLACEMENTMATRIX, "placementMatrix");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_OUTALPHA, "outAlpha");
	PM->RegisterAttributeName(ID_DLNOISE, PARAM_OUTMASK, "outMask");
}

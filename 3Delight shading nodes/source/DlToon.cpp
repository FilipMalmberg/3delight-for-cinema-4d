//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlToon.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlToon : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlToon); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlToon::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_COLOR_TINT , Vector(1, 1, 1));
	{
		AutoAlloc<Gradient> gradient;
		if (!gradient) return false;
		GradientKnot k;
		k.col = Vector(0, 0, 0);
		k.pos = 0;
		gradient->InsertKnot(k);
		k.col = Vector(1, 1, 1);
		k.pos = 1;
		gradient->InsertKnot(k);
		data->SetData(PARAM_COLOR_CURVE_COLORS, GeData(CUSTOMDATATYPE_GRADIENT, *gradient));
	}
	data->SetFloat(PARAM_DIFFUSE_WEIGHT , 1);
	data->SetFloat(PARAM_OPACITY , 1);
	data->SetFloat(PARAM_SPECULAR_WEIGHT , 1);
	data->SetVector(PARAM_SPECULAR_COLOR , Vector(1, 1, 1));
	data->SetFloat(PARAM_SPECULAR_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_REFRACT_WEIGHT , 0);
	data->SetVector(PARAM_REFRACT_COLOR , Vector(1, 1, 1));
	data->SetFloat(PARAM_REFRACT_IOR , 1.3);
	data->SetBool(PARAM_OUTLINE_SILHOUETTES_ENABLE , 1);
	data->SetVector(PARAM_OUTLINE_SILHOUETTES_COLOR , Vector(0.2, 0.2, 0.2));
	data->SetBool(PARAM_OUTLINE_SILHOUETTES_TINT_COLOR , 0);
	data->SetFloat(PARAM_OUTLINE_SILHOUETTES_OPACITY , 1);
	data->SetFloat(PARAM_OUTLINE_SILHOUETTES_WIDTH , 0.01);
	data->SetFloat(PARAM_OUTLINE_SILHOUETTES_SENSITIVITY , 1);
	data->SetInt32(PARAM_OUTLINE_SILOUHETTES_PRIORITY , 0);
	data->SetBool(PARAM_OUTLINE_FOLDS_ENABLE , 0);
	data->SetVector(PARAM_OUTLINE_FOLDS_COLOR , Vector(0.5, 0.5, 0.5));
	data->SetBool(PARAM_OUTLINE_FOLDS_TINT_COLOR , 0);
	data->SetFloat(PARAM_OUTLINE_FOLDS_OPACITY , 1);
	data->SetFloat(PARAM_OUTLINE_FOLDS_WIDTH , 0.01);
	data->SetFloat(PARAM_OUTLINE_FOLDS_SENSITIVITY , 0.3);
	data->SetInt32(PARAM_OUTLINE_FOLDS_PRIORITY , 0);
	data->SetBool(PARAM_OUTLINE_CREASES_ENABLE , 0);
	data->SetVector(PARAM_OUTLINE_CREASES_COLOR , Vector(0.5, 0.5, 0.5));
	data->SetBool(PARAM_OUTLINE_CREASES_TINT_COLOR , 0);
	data->SetFloat(PARAM_OUTLINE_CREASES_OPACITY , 1);
	data->SetFloat(PARAM_OUTLINE_CREASES_WIDTH , 0.01);
	data->SetInt32(PARAM_OUTLINE_CREASES_PRIORITY , 0);
	data->SetBool(PARAM_OUTLINE_OBJECTS_ENABLE , 0);
	data->SetVector(PARAM_OUTLINE_OBJECTS_COLOR , Vector(0.5, 0.5, 0.5));
	data->SetBool(PARAM_OUTLINE_OBJECTS_TINT_COLOR , 0);
	data->SetFloat(PARAM_OUTLINE_OBJECTS_OPACITY , 1);
	data->SetFloat(PARAM_OUTLINE_OBJECTS_WIDTH , 0.01);
	data->SetInt32(PARAM_OUTLINE_OBJECTS_PRIORITY , 0);
	data->SetBool(PARAM_OUTLINE_TEXTURE_ENABLE , 0);
	data->SetVector(PARAM_OUTLINE_TEXTURE_SOURCE , Vector(0, 0, 0));
	data->SetVector(PARAM_OUTLINE_TEXTURE_COLOR , Vector(0.5, 0.5, 0.5));
	data->SetBool(PARAM_OUTLINE_TEXTURE_TINT_COLOR , 0);
	data->SetFloat(PARAM_OUTLINE_TEXTURE_OPACITY , 1);
	data->SetFloat(PARAM_OUTLINE_TEXTURE_WIDTH , 0.01);
	data->SetFloat(PARAM_OUTLINE_TEXTURE_SENSITIVITY , 0.1);
	data->SetInt32(PARAM_OUTLINE_TEXTURE_PRIORITY , 0);
	data->SetInt32(PARAM_OUTLINE_ALPHA_ENABLE , 0);
	data->SetVector(PARAM_OUTLINE_ALPHA_COLOR , Vector(0.5, 0.5, 0.5));
	data->SetInt32(PARAM_OUTLINE_ALPHA_TINT_COLOR , 0);
	data->SetFloat(PARAM_OUTLINE_ALPHA_OPACITY , 1);
	data->SetFloat(PARAM_OUTLINE_ALPHA_WIDTH , 0.01);
	data->SetFloat(PARAM_OUTLINE_ALPHA_SENSITIVITY , 0.1);
	data->SetInt32(PARAM_OUTLINE_ALPHA_PRIORITY , 0);
	data->SetFloat(PARAM_OUTLINE_FADE , 1);
	data->SetBool(PARAM_ENABLE_BACKFACING_OUTLINES , 1);
	data->SetVector(PARAM_INCANDESCENCE , Vector(0, 0, 0));
	data->SetFloat(PARAM_INCANDESCENCE_INTENSITY , 1);
	data->SetVector(PARAM_INCANDESCENCE_MULTIPLIER , Vector(1, 1, 1));
	data->SetInt32(PARAM_DISP_NORMAL_BUMP_TYPE , PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP);
	data->SetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY , 1);
	data->SetFloat(PARAM_OCCLUSION_DISTANCE , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlToon(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLTOON, "Toon"_s, 0, DlToon::Alloc, "dlToon"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

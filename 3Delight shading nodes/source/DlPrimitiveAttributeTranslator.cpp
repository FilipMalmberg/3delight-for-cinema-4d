#include "DlPrimitiveAttributeTranslator.h"
#include "DlPrimitiveAttribute.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlPrimitiveAttributeTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlPrimitiveAttribute.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void DlPrimitiveAttributeTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlPrimitiveAttributeTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			String s = data->GetString(PARAM_ATTRIBUTE_NAME);
			ctx.SetAttribute(shader_handle, NSI::StringArg("attribute_name", StringToStdString(s)));
		}

		{
			Int32 option = data->GetInt32(PARAM_ATTRIBUTE_TYPE);
			if(option == PARAM_ATTRIBUTE_TYPE_FLOAT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("attribute_type", 0));
			}
			else if(option == PARAM_ATTRIBUTE_TYPE_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("attribute_type", 1));
			}
			else if(option == PARAM_ATTRIBUTE_TYPE_POINT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("attribute_type", 2));
			}
			else if(option == PARAM_ATTRIBUTE_TYPE_UV){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("attribute_type", 3));
			}
			else if(option == PARAM_ATTRIBUTE_TYPE_INTEGER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("attribute_type", 4));
			}
			else if(option == PARAM_ATTRIBUTE_TYPE_AUTO){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("attribute_type", 5));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_FALLBACK_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("fallback_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_O_FLOAT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_float", f));

		{
			int value = (int)data->GetInt32(PARAM_OUTINTEGER);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("outInteger", value));
		}

		f = (float) data->GetFloat(PARAM_O_POINTX);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_pointX", f));

		f = (float) data->GetFloat(PARAM_O_POINTY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_pointY", f));

		f = (float) data->GetFloat(PARAM_O_POINTZ);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_pointZ", f));

		f = (float) data->GetFloat(PARAM_O_U);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_u", f));

		f = (float) data->GetFloat(PARAM_O_V);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_v", f));

	{
		v = toLinear(data->GetVector(PARAM_O_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("o_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_O_COLORR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_colorR", f));

		f = (float) data->GetFloat(PARAM_O_COLORG);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_colorG", f));

		f = (float) data->GetFloat(PARAM_O_COLORB);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("o_colorB", f));

		f = (float) data->GetFloat(PARAM_OUTALPHA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outAlpha", f));

}

void  DlPrimitiveAttributeTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_ATTRIBUTE_NAME, "attribute_name");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_ATTRIBUTE_TYPE, "attribute_type");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_FALLBACK_COLOR, "fallback_color");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_FALLBACK_VALUE, "fallback_value");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_FLOAT, "o_float");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_OUTINTEGER, "outInteger");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_POINT, "o_point");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_POINTX, "o_pointX");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_POINTY, "o_pointY");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_POINTZ, "o_pointZ");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_UV, "o_uv");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_U, "o_u");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_V, "o_v");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_COLOR, "o_color");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_COLORR, "o_colorR");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_COLORG, "o_colorG");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_O_COLORB, "o_colorB");
	PM->RegisterAttributeName(ID_DLPRIMITIVEATTRIBUTE, PARAM_OUTALPHA, "outAlpha");
}

#include "DlToonGlassTranslator.h"
#include "DlToonGlass.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlToonGlassTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlToonGlass.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
}

void DlToonGlassTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlToonGlassTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

	{
		v = toLinear(data->GetVector(PARAM_I_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("i_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_REFRACT_IOR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("refract_ior", f));

		{
			int value = (int)data->GetBool(PARAM_FG_OUTLINE_OBJECTS_ENABLE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("fg_outline_objects_enable", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_FG_OUTLINE_OBJECTS_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("fg_outline_objects_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_FG_OUTLINE_OBJECTS_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("fg_outline_objects_opacity", f));

		f = (float) data->GetFloat(PARAM_FG_OUTLINE_OBJECTS_WIDTH);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("fg_outline_objects_width", f));

		{
			int value = (int)data->GetInt32(PARAM_FG_OUTLINE_OBJECTS_PRIORITY);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("fg_outline_objects_priority", value));
		}

		f = (float) data->GetFloat(PARAM_OUTLINE_FADE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outline_fade", f));

}

void  DlToonGlassTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLTOONGLASS, PARAM_I_COLOR, "i_color");
	PM->RegisterAttributeName(ID_DLTOONGLASS, PARAM_REFRACT_IOR, "refract_ior");
	PM->RegisterAttributeName(ID_DLTOONGLASS, PARAM_FG_OUTLINE_OBJECTS_ENABLE, "fg_outline_objects_enable");
	PM->RegisterAttributeName(ID_DLTOONGLASS, PARAM_FG_OUTLINE_OBJECTS_COLOR, "fg_outline_objects_color");
	PM->RegisterAttributeName(ID_DLTOONGLASS, PARAM_FG_OUTLINE_OBJECTS_OPACITY, "fg_outline_objects_opacity");
	PM->RegisterAttributeName(ID_DLTOONGLASS, PARAM_FG_OUTLINE_OBJECTS_WIDTH, "fg_outline_objects_width");
	PM->RegisterAttributeName(ID_DLTOONGLASS, PARAM_FG_OUTLINE_OBJECTS_PRIORITY, "fg_outline_objects_priority");
	PM->RegisterAttributeName(ID_DLTOONGLASS, PARAM_OUTLINE_FADE, "outline_fade");
	PM->RegisterAttributeName(ID_DLTOONGLASS, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLTOONGLASS, PARAM_OUTCOLOR, "outColor");
}

#include "DlRampTranslator.h"
#include "DlRamp.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlRampTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlRamp.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord" );
}

void DlRampTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlRampTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		{
			Int32 option = data->GetInt32(PARAM_MODE);
			if(option == PARAM_MODE_HORIZONTAL__U_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 0));
			}
			else if(option == PARAM_MODE_VERTICAL__V_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 1));
			}
			else if(option == PARAM_MODE_DIAGONAL){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 2));
			}
			else if(option == PARAM_MODE_RADIAL){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 3));
			}
			else if(option == PARAM_MODE_CIRCULAR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 4));
			}
			else if(option == PARAM_MODE_BOX){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 5));
			}
			else if(option == PARAM_MODE_RANDOM_COLOR){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 6));
			}
			else if(option == PARAM_MODE_COLORIZE_INPUT){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("mode", 7));
			}
		}

		{
			Gradient* gradient = (Gradient*)data->GetCustomDataType(PARAM_COLORRAMP_COLORVALUE, CUSTOMDATATYPE_GRADIENT );

			int knot_count;
			std::vector<float> knots;
			std::vector<float> colors;
			std::vector<int> interpolations;
			FillColorRampNSIData(gradient, doc, knots, colors, interpolations, knot_count);

			NSI::Argument arg_knot_color("colorRamp_ColorValue");
			arg_knot_color.SetArrayType(NSITypeColor, knot_count);
			arg_knot_color.SetValuePointer((void*)&colors[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_color));

			NSI::Argument arg_knot_position("colorRamp_Position");
			arg_knot_position.SetArrayType(NSITypeFloat, knot_count);
			arg_knot_position.SetValuePointer((void*)&knots[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_position));

			NSI::Argument arg_knot_interpolation("colorRamp_Interp");
			arg_knot_interpolation.SetArrayType(NSITypeInteger, knot_count);
			arg_knot_interpolation.SetValuePointer((void*)&interpolations[0]);
			ctx.SetAttribute(shader_handle, (arg_knot_interpolation));

		}

		{
			Int32 option = data->GetInt32(PARAM_COLOR_MIX_MODE);
			if(option == PARAM_COLOR_MIX_MODE_RGB){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_mix_mode", 0));
			}
			else if(option == PARAM_COLOR_MIX_MODE_HSV){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("color_mix_mode", 1));
			}
		}

		f = (float) data->GetFloat(PARAM_INPUT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("input", f));

		f = (float) data->GetFloat(PARAM_REPEAT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("repeat", f));

		f = (float) data->GetFloat(PARAM_OFFSET);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("offset", f));

		{
			int value = (int)data->GetBool(PARAM_REVERSE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("reverse", value));
		}

		f = (float) data->GetFloat(PARAM_DISTORTION_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("distortion_intensity", f));

	{
		v = toLinear(data->GetVector(PARAM_DISTORTION), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("distortion", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_NOISE_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_intensity", f));

		f = (float) data->GetFloat(PARAM_NOISE_HUE_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_hue_intensity", f));

		f = (float) data->GetFloat(PARAM_NOISE_SATURATION_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_saturation_intensity", f));

		f = (float) data->GetFloat(PARAM_NOISE_VALUE_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_value_intensity", f));

		f = (float) data->GetFloat(PARAM_NOISE_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_scale", f));

		f = (float) data->GetFloat(PARAM_NOISE_OFFSET);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_offset", f));

		f = (float) data->GetFloat(PARAM_NOISE_STEPPING);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_stepping", f));

		f = (float) data->GetFloat(PARAM_NOISE_TIME);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("noise_time", f));

		{
			int value = (int)data->GetInt32(PARAM_LAYERS);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("layers", value));
		}

		f = (float) data->GetFloat(PARAM_LAYER_PERSISTENCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layer_persistence", f));

		f = (float) data->GetFloat(PARAM_LAYER_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("layer_scale", f));

		{
			int value = (int)data->GetBool(PARAM_INVERT);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("invert", value));
		}

	{
		v = toLinear(data->GetVector(PARAM_COLORGAIN), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("colorGain", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_COLOROFFSET), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("colorOffset", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_ALPHAGAIN);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("alphaGain", f));

		f = (float) data->GetFloat(PARAM_ALPHAOFFSET);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("alphaOffset", f));

	{
		v = toLinear(data->GetVector(PARAM_OUTCOLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("outColor", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_OUTALPHA);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outAlpha", f));

}

void  DlRampTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_MODE, "mode");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_COLORRAMP_POSITION, "colorRamp_Position");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_COLORRAMP_COLORVALUE, "colorRamp_ColorValue");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_COLORRAMP_INTERP, "colorRamp_Interp");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_COLOR_MIX_MODE, "color_mix_mode");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_INPUT, "input");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_REPEAT, "repeat");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_OFFSET, "offset");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_REVERSE, "reverse");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_DISTORTION_INTENSITY, "distortion_intensity");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_DISTORTION, "distortion");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_NOISE_INTENSITY, "noise_intensity");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_NOISE_HUE_INTENSITY, "noise_hue_intensity");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_NOISE_SATURATION_INTENSITY, "noise_saturation_intensity");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_NOISE_VALUE_INTENSITY, "noise_value_intensity");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_NOISE_SCALE, "noise_scale");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_NOISE_OFFSET, "noise_offset");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_NOISE_STEPPING, "noise_stepping");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_NOISE_TIME, "noise_time");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_LAYERS, "layers");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_LAYER_PERSISTENCE, "layer_persistence");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_LAYER_SCALE, "layer_scale");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_INVERT, "invert");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_COLORGAIN, "colorGain");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_COLOROFFSET, "colorOffset");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_ALPHAGAIN, "alphaGain");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_ALPHAOFFSET, "alphaOffset");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_UVCOORD, "uvCoord");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_OUTCOLOR, "outColor");
	PM->RegisterAttributeName(ID_DLRAMP, PARAM_OUTALPHA, "outAlpha");
}

#include "DlPrincipledTranslator.h"
#include "DlPrincipled.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlPrincipledTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlPrincipled.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("*uvCoord", "o_outUV", shader_handle, "uvCoord" );
}

void DlPrincipledTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlPrincipledTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

	{
		v = toLinear(data->GetVector(PARAM_I_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("i_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("roughness", f));

		f = (float) data->GetFloat(PARAM_SPECULAR_LEVEL);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("specular_level", f));

		f = (float) data->GetFloat(PARAM_METALLIC);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("metallic", f));

		{
			int value = (int)data->GetBool(PARAM_REFLECT_AOVS);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("reflect_aovs", value));
		}

		f = (float) data->GetFloat(PARAM_ANISOTROPY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("anisotropy", f));

	{
		v = data->GetVector(PARAM_ANISOTROPY_DIRECTION); //Don't linearize
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("anisotropy_direction", &v_data[0]));
	}
		f = (float) data->GetFloat(PARAM_OPACITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("opacity", f));

		f = (float) data->GetFloat(PARAM_COATING_THICKNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("coating_thickness", f));

	{
		v = toLinear(data->GetVector(PARAM_COATING_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("coating_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_COATING_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("coating_roughness", f));

		f = (float) data->GetFloat(PARAM_COATING_SPECULAR_LEVEL);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("coating_specular_level", f));

		f = (float) data->GetFloat(PARAM_SSS_WEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("sss_weight", f));

	{
		v = toLinear(data->GetVector(PARAM_SSS_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("sss_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_SSS_ANISOTROPY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("sss_anisotropy", f));

		f = (float) data->GetFloat(PARAM_SSS_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("sss_scale", f));

		{
			int value = (int)data->GetBool(PARAM_SSS_INTERNAL_LIGHTING);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("sss_internal_lighting", value));
		}

		{
			int value = (int)data->GetBool(PARAM_SSS_SIMPLE_COLOR_MODEL);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("sss_simple_color_model", value));
		}

		{
			String s = data->GetString(PARAM_SSS_MERGE_SET);
			ctx.SetAttribute(shader_handle, NSI::StringArg("sss_merge_set", StringToStdString(s)));
		}

		{
			int value = (int)data->GetBool(PARAM_SSS_DOMINANT_MATERIAL);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("sss_dominant_material", value));
		}

		{
			int value = (int)data->GetBool(PARAM_SSS_DOUBLE_SIDED);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("sss_double_sided", value));
		}

		f = (float) data->GetFloat(PARAM_SHEEN_LEVEL);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("sheen_level", f));

		f = (float) data->GetFloat(PARAM_SHEEN_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("sheen_roughness", f));

	{
		v = toLinear(data->GetVector(PARAM_SHEEN_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("sheen_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_REFRACT_WEIGHT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("refract_weight", f));

	{
		v = toLinear(data->GetVector(PARAM_REFRACT_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("refract_color", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_REFRACT_IOR);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("refract_ior", f));

		f = (float) data->GetFloat(PARAM_REFRACT_ROUGHNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("refract_roughness", f));

		{
			int value = (int)data->GetBool(PARAM_TRANSMIT_AOVS);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("transmit_aovs", value));
		}

		{
			int value = (int)data->GetInt32(PARAM_VOLUMETRIC_ENABLE);
			ctx.SetAttribute(shader_handle, NSI::IntegerArg("volumetric_enable", value));
		}

		f = (float) data->GetFloat(PARAM_VOLUMETRIC_DENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("volumetric_density", f));

	{
		v = toLinear(data->GetVector(PARAM_VOLUMETRIC_SCATTERING_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("volumetric_scattering_color", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_VOLUMETRIC_TRANSPARENCY_COLOR), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("volumetric_transparency_color", &v_data[0]));
	}

	{
		v = toLinear(data->GetVector(PARAM_INCANDESCENCE), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("incandescence", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_INCANDESCENCE_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("incandescence_intensity", f));

	{
		v = toLinear(data->GetVector(PARAM_INCANDESCENCE_MULTIPLIER), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("incandescence_multiplier", &v_data[0]));
	}

		{
			Int32 option = data->GetInt32(PARAM_DISP_NORMAL_BUMP_TYPE);
			if(option == PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 0));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_NORMAL_MAP__DIRECTX_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 1));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_NORMAL_MAP__OPENGL_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 2));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_DISPLACEMENT__0_0_CENTERED_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 3));
			}
			else if(option == PARAM_DISP_NORMAL_BUMP_TYPE_DISPLACEMENT__0_5_CENTERED_){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("disp_normal_bump_type", 4));
			}
		}

	{
		v = toLinear(data->GetVector(PARAM_DISP_NORMAL_BUMP_VALUE), doc);
		v_data[0] = (float)v.x;
		v_data[1] = (float)v.y;
		v_data[2] = (float)v.z;
		ctx.SetAttribute(shader_handle, NSI::ColorArg("disp_normal_bump_value", &v_data[0]));
	}

		f = (float) data->GetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("disp_normal_bump_intensity", f));

		{
			Int32 option = data->GetInt32(PARAM_NORMAL_BUMP_AFFECT_LAYER);
			if(option == PARAM_NORMAL_BUMP_AFFECT_LAYER_BOTH_LAYERS){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("normal_bump_affect_layer", 0));
			}
			else if(option == PARAM_NORMAL_BUMP_AFFECT_LAYER_COATING_LAYER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("normal_bump_affect_layer", 1));
			}
			else if(option == PARAM_NORMAL_BUMP_AFFECT_LAYER_BASE_LAYER){
				ctx.SetAttribute(shader_handle, NSI::IntegerArg("normal_bump_affect_layer", 2));
			}
		}

		f = (float) data->GetFloat(PARAM_OCCLUSION_DISTANCE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("occlusion_distance", f));

}

void  DlPrincipledTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_I_COLOR, "i_color");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_ROUGHNESS, "roughness");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SPECULAR_LEVEL, "specular_level");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_METALLIC, "metallic");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_REFLECT_AOVS, "reflect_aovs");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_ANISOTROPY, "anisotropy");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_ANISOTROPY_DIRECTION, "anisotropy_direction");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_OPACITY, "opacity");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_COATING_THICKNESS, "coating_thickness");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_COATING_COLOR, "coating_color");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_COATING_ROUGHNESS, "coating_roughness");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_COATING_SPECULAR_LEVEL, "coating_specular_level");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SSS_WEIGHT, "sss_weight");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SSS_COLOR, "sss_color");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SSS_ANISOTROPY, "sss_anisotropy");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SSS_SCALE, "sss_scale");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SSS_INTERNAL_LIGHTING, "sss_internal_lighting");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SSS_SIMPLE_COLOR_MODEL, "sss_simple_color_model");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SSS_MERGE_SET, "sss_merge_set");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SSS_DOMINANT_MATERIAL, "sss_dominant_material");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SSS_DOUBLE_SIDED, "sss_double_sided");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SHEEN_LEVEL, "sheen_level");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SHEEN_ROUGHNESS, "sheen_roughness");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_SHEEN_COLOR, "sheen_color");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_REFRACT_WEIGHT, "refract_weight");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_REFRACT_COLOR, "refract_color");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_REFRACT_IOR, "refract_ior");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_REFRACT_ROUGHNESS, "refract_roughness");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_TRANSMIT_AOVS, "transmit_aovs");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_VOLUMETRIC_ENABLE, "volumetric_enable");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_VOLUMETRIC_DENSITY, "volumetric_density");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_VOLUMETRIC_SCATTERING_COLOR, "volumetric_scattering_color");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_VOLUMETRIC_TRANSPARENCY_COLOR, "volumetric_transparency_color");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_INCANDESCENCE, "incandescence");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_INCANDESCENCE_INTENSITY, "incandescence_intensity");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_INCANDESCENCE_MULTIPLIER, "incandescence_multiplier");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_DISP_NORMAL_BUMP_TYPE, "disp_normal_bump_type");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_DISP_NORMAL_BUMP_VALUE, "disp_normal_bump_value");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_DISP_NORMAL_BUMP_INTENSITY, "disp_normal_bump_intensity");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_NORMAL_BUMP_AFFECT_LAYER, "normal_bump_affect_layer");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_OCCLUSION_DISTANCE, "occlusion_distance");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_UVCOORD, "uvCoord");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_AOVGROUP, "aovGroup");
	PM->RegisterAttributeName(ID_DLPRINCIPLED, PARAM_OUTCOLOR, "outColor");
}

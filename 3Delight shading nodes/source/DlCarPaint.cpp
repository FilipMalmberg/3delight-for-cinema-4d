//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlCarPaint.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlCarPaint : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlCarPaint); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlCarPaint::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetVector(PARAM_I_COLOR , Vector(0.9, 0.4, 0.1));
	data->SetFloat(PARAM_DIFFUSE_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_OPACITY , 1);
	data->SetVector(PARAM_SPECULAR_COLOR , Vector(1, 0.1, 0.1));
	data->SetFloat(PARAM_SPECULAR_LEVEL , 0.5);
	data->SetFloat(PARAM_SPECULAR_ROUGHNESS_1 , 0.05);
	data->SetFloat(PARAM_SPECULAR_ROUGHNESS_2 , 0.25);
	data->SetFloat(PARAM_SPECULAR_BALANCE , 0.5);
	data->SetFloat(PARAM_METALLIC , 0.8);
	data->SetFloat(PARAM_FLAKE_WEIGHT , 0.7);
	data->SetFloat(PARAM_FLAKE_DENSITY , 0.7);
	data->SetVector(PARAM_FLAKE_COLOR , Vector(1, 0.3, 0.2));
	data->SetFloat(PARAM_FLAKE_ROUGHNESS , 0.3);
	data->SetFloat(PARAM_FLAKE_SCALE , 0.25);
	data->SetInt32(PARAM_FLAKE_LAYERS , 3);
	data->SetFloat(PARAM_FLAKE_RANDOMNESS , 1);
	data->SetFloat(PARAM_THIN_FILM_THICKNESS , 0);
	data->SetFloat(PARAM_THIN_FILM_IOR , 1.5);
	data->SetFloat(PARAM_COATING_THICKNESS , 0.1);
	data->SetVector(PARAM_COATING_COLOR , Vector(0.8, 0.7, 0.2));
	data->SetFloat(PARAM_COATING_ROUGHNESS , 0.02);
	data->SetFloat(PARAM_COATING_SPECULAR_LEVEL , 0.75);
	data->SetInt32(PARAM_DISP_NORMAL_BUMP_TYPE , PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP);
	data->SetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY , 1);
	data->SetInt32(PARAM_NORMAL_BUMP_AFFECT_LAYER , PARAM_NORMAL_BUMP_AFFECT_LAYER_BOTH_LAYERS);
	data->SetFloat(PARAM_OCCLUSION_DISTANCE , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlCarPaint(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLCARPAINT, "Car Paint"_s, 0, DlCarPaint::Alloc, "dlCarPaint"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

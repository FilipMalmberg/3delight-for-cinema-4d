#include "DlDisplacementBlendTranslator.h"
#include "DlDisplacementBlend.h"
#include <c4d_graphview.h>
#include "IDs.h"
#include "DL_utilities.h"
#include "DL_TypeConversions.h"
#include "nsi.hpp"
#include "delightenvironment.h"

void DlDisplacementBlendTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){
		NSI::Context& ctx(parser->GetContext());
		std::string shader_handle = (std::string) Handle;
		ctx.Create(shader_handle, "shader");
		const char* delightpath = DelightEnv::getDelightEnvironment();
		std::string  shaderpath = std::string(delightpath) + (std::string)"/osl/dlDisplacementBlend.oso";
		ctx.SetAttribute(shader_handle, NSI::StringArg("shaderfilename", shaderpath));
		ctx.Connect("3dlfc4d::default_placement_matrix", "out", shader_handle, "placementMatrix" );
}

void DlDisplacementBlendTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	std::string source_handle = std::string(Handle);

	//Clear any previous connections
	ctx.Disconnect(source_handle, ".all", ".all", ".all");

	GvPort* port;
	GvDestination* destination;
	for (int i = 0; i < gvnode->GetOutPortCount(); i++) {
		port = gvnode->GetOutPort(i);
		std::string source_attribute_name = parser->GetAttributeName(gvnode->GetOperatorID(), port->GetMainID());
		for (int j = 0; j < port->GetNrOfConnections(); j++) {
			destination = port->GetOutgoing(j);
			if (destination) {
				std::string dest_handle = parser->GetHandleName(destination->node);
				long destID = destination->node->GetOperatorID();
				long destAttributeID = destination->port->GetMainID();
				std::string dest_attribute_name = parser->GetAttributeName(destID, destAttributeID);
				ctx.Disconnect(".all", ".all", dest_handle, dest_attribute_name);
				ctx.Connect(source_handle, source_attribute_name, dest_handle, dest_attribute_name);
			}
		}
	}
}

void DlDisplacementBlendTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser){

	NSI::Context& ctx(parser->GetContext());
	GvNode* gvnode = (GvNode*)C4DNode;
	BaseContainer * data = gvnode->GetOpContainerInstance();
	std::string shader_handle = (std::string) Handle;

	float f;
	Vector v;
	float v_data[3];

		f = (float) data->GetFloat(PARAM_DISPLACEMENT_1);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("displacement_1", f));

		f = (float) data->GetFloat(PARAM_DISPLACEMENT_GAIN_1);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("displacement_gain_1", f));

		f = (float) data->GetFloat(PARAM_DISPLACEMENT_OFFSET_1);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("displacement_offset_1", f));

		f = (float) data->GetFloat(PARAM_DISPLACEMENT_2);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("displacement_2", f));

		f = (float) data->GetFloat(PARAM_DISPLACEMENT_GAIN_2);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("displacement_gain_2", f));

		f = (float) data->GetFloat(PARAM_DISPLACEMENT_OFFSET_2);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("displacement_offset_2", f));

		f = (float) data->GetFloat(PARAM_DISPLACEMENT_3);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("displacement_3", f));

		f = (float) data->GetFloat(PARAM_DISPLACEMENT_GAIN_3);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("displacement_gain_3", f));

		f = (float) data->GetFloat(PARAM_DISPLACEMENT_OFFSET_3);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("displacement_offset_3", f));

		f = (float) data->GetFloat(PARAM_BLEND_SOFTNESS);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_softness", f));

		f = (float) data->GetFloat(PARAM_BLEND_NOISE_INTENSITY);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_noise_intensity", f));

		f = (float) data->GetFloat(PARAM_BLEND_NOISE_SCALE);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("blend_noise_scale", f));

	{
		Matrix m = data->GetMatrix(PARAM_PLACEMENTMATRIX);
		std::vector<float> mdata = MatrixToNSIMatrix(m);
		NSI::Argument matrix_arg("placementMatrix");
		matrix_arg.SetType(NSITypeMatrix);
		matrix_arg.SetValuePointer((void*)&mdata[0]);
		ctx.SetAttribute(shader_handle, matrix_arg);
	}

		f = (float) data->GetFloat(PARAM_OUTDISPLACEMENT);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outDisplacement", f));

		f = (float) data->GetFloat(PARAM_OUTMASK1);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outMask1", f));

		f = (float) data->GetFloat(PARAM_OUTMASK2);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outMask2", f));

		f = (float) data->GetFloat(PARAM_OUTMASK3);
		ctx.SetAttribute(shader_handle, NSI::FloatArg("outMask3", f));

}

void  DlDisplacementBlendTranslator::RegisterAttributeNames(DL_PluginManager* PM){
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_DISPLACEMENT_1, "displacement_1");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_DISPLACEMENT_GAIN_1, "displacement_gain_1");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_DISPLACEMENT_OFFSET_1, "displacement_offset_1");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_DISPLACEMENT_2, "displacement_2");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_DISPLACEMENT_GAIN_2, "displacement_gain_2");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_DISPLACEMENT_OFFSET_2, "displacement_offset_2");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_DISPLACEMENT_3, "displacement_3");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_DISPLACEMENT_GAIN_3, "displacement_gain_3");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_DISPLACEMENT_OFFSET_3, "displacement_offset_3");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_BLEND_SOFTNESS, "blend_softness");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_BLEND_NOISE_INTENSITY, "blend_noise_intensity");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_BLEND_NOISE_SCALE, "blend_noise_scale");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_PLACEMENTMATRIX, "placementMatrix");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_OUTDISPLACEMENT, "outDisplacement");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_OUTMASK1, "outMask1");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_OUTMASK2, "outMask2");
	PM->RegisterAttributeName(ID_DLDISPLACEMENTBLEND, PARAM_OUTMASK3, "outMask3");
}

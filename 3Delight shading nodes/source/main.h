#pragma once

#include "c4d.h"
#include <string.h>

#include "DL_API.h"
#include "ShaderSettingsHook.h"
#include "DelightUVCoord.h"
#include "TextureTagTranslator.h"

//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlDisplacementBlend.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlDisplacementBlend : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlDisplacementBlend); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlDisplacementBlend::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetFloat(PARAM_DISPLACEMENT_1 , 0);
	data->SetFloat(PARAM_DISPLACEMENT_GAIN_1 , 1);
	data->SetFloat(PARAM_DISPLACEMENT_OFFSET_1 , 0);
	data->SetFloat(PARAM_DISPLACEMENT_2 , 0);
	data->SetFloat(PARAM_DISPLACEMENT_GAIN_2 , 1);
	data->SetFloat(PARAM_DISPLACEMENT_OFFSET_2 , 0);
	data->SetFloat(PARAM_DISPLACEMENT_3 , 0);
	data->SetFloat(PARAM_DISPLACEMENT_GAIN_3 , 1);
	data->SetFloat(PARAM_DISPLACEMENT_OFFSET_3 , 0);
	data->SetFloat(PARAM_BLEND_SOFTNESS , 0.5);
	data->SetFloat(PARAM_BLEND_NOISE_INTENSITY , 0);
	data->SetFloat(PARAM_BLEND_NOISE_SCALE , 1);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlDisplacementBlend(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLDISPLACEMENTBLEND, "Displacement Blend"_s, 0, DlDisplacementBlend::Alloc, "dlDisplacementBlend"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

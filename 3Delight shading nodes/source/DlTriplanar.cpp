//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlTriplanar.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlTriplanar : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlTriplanar); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlTriplanar::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetInt32(PARAM_MODE , PARAM_MODE_TRIPLANAR);
	data->SetString(PARAM_COLOR_TEXTURE, String(""));
	data->SetInt32(PARAM_COLOR_TEXTURE_META_COLORSPACE, PARAM_COLOR_TEXTURE_META_COLORSPACE_AUTO);
	data->SetString(PARAM_FLOAT_TEXTURE, String(""));
	data->SetInt32(PARAM_FLOAT_TEXTURE_META_COLORSPACE, PARAM_FLOAT_TEXTURE_META_COLORSPACE_LINEAR);
	data->SetString(PARAM_HEIGHT_TEXTURE, String(""));
	data->SetInt32(PARAM_HEIGHT_TEXTURE_META_COLORSPACE, PARAM_HEIGHT_TEXTURE_META_COLORSPACE_LINEAR);
	data->SetFloat(PARAM_SCALE , 1);
	data->SetInt32(PARAM_SPACE , PARAM_SPACE_WORLD);
	data->SetFloat(PARAM_BLEND_SOFTNESS , 0.5);
	data->SetFloat(PARAM_BLEND_NOISE_INTENSITY , 0);
	data->SetFloat(PARAM_BLEND_NOISE_SCALE , 1);
	data->SetBool(PARAM_USE_HEIGHT_BLENDING , 1);
	data->SetFloat(PARAM_VARIATION_OFFSET , 0);
	data->SetFloat(PARAM_VARIATION_AXIS_ROTATION , 0);
	data->SetFloat(PARAM_VARIATION_SCALE , 0);
	data->SetBool(PARAM_TILE_REMOVAL , 0);
	data->SetFloat(PARAM_TILE_SOFTNESS , 0.75);
	data->SetFloat(PARAM_TILE_OFFSET , 1);
	data->SetFloat(PARAM_TILE_ROTATION , 1);
	data->SetFloat(PARAM_TILE_SCALE , 0.5);
	data->SetBool(PARAM_USEIMAGESEQUENCE , 0);
	data->SetInt32(PARAM_FRAME , 1);
	data->SetInt32(PARAM_FRAMEOFFSET , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlTriplanar(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLTRIPLANAR, "Triplanar"_s, 0, DlTriplanar::Alloc, "dlTriplanar"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

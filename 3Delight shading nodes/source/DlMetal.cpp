//C++ code automatically generated from OSL shader by ShaderGen.exe. Do not edit.

#include "c4d.h"
#include "DlMetal.h"
#include "gvbase.h"
#include "c4d_graphview.h"
#include <c4d_operatordata.h>
#include "IDs.h"

class DlMetal : public  GvOperatorData {
public:
	static NodeData * Alloc(void) {
		return NewObjClear(DlMetal); 
	}

	virtual Bool iCreateOperator(GvNode * 	bn);
};

Bool DlMetal::iCreateOperator(GvNode * 	bn){
	BaseContainer* data = bn->GetOpContainerInstance();

	data->SetFloat(PARAM_COATING_THICKNESS , 0);
	data->SetVector(PARAM_COATING_COLOR , Vector(1, 0.5, 0.1));
	data->SetFloat(PARAM_COATING_ROUGHNESS , 0);
	data->SetFloat(PARAM_COATING_SPECULAR_LEVEL , 0.5);
	data->SetVector(PARAM_I_COLOR , Vector(0.946699, 0.600009, 0.538258));
	data->SetVector(PARAM_EDGE_COLOR , Vector(1, 0.850332, 0.796304));
	data->SetFloat(PARAM_ROUGHNESS , 0.2);
	data->SetFloat(PARAM_ANISOTROPY , 0);
	data->SetVector(PARAM_ANISOTROPY_DIRECTION , Vector(0.5, 1, 0));
	data->SetFloat(PARAM_OPACITY , 1);
	data->SetFloat(PARAM_THIN_FILM_THICKNESS , 0);
	data->SetFloat(PARAM_THIN_FILM_IOR , 2.5);
	data->SetInt32(PARAM_DISP_NORMAL_BUMP_TYPE , PARAM_DISP_NORMAL_BUMP_TYPE_BUMP_MAP);
	data->SetFloat(PARAM_DISP_NORMAL_BUMP_INTENSITY , 1);
	data->SetInt32(PARAM_NORMAL_BUMP_AFFECT_LAYER , PARAM_NORMAL_BUMP_AFFECT_LAYER_BOTH_LAYERS);
	data->SetFloat(PARAM_OCCLUSION_DISTANCE , 0);

	return  GvOperatorData::iCreateOperator(bn);
}

Bool RegisterDlMetal(const char* icon="dl_200.tif", int node_group=ID_GV_OPGROUP_TYPE_GENERAL){
	return GvRegisterOperatorPlugin(ID_DLMETAL, "Metal"_s, 0, DlMetal::Alloc, "dlMetal"_s, 0, DL_NODES_CLASS,node_group, 0, AutoBitmap(String(icon))); 
}

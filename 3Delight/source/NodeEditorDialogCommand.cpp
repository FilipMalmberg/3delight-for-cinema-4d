#pragma once
#include "c4d.h"
#include "NodeEditorDialog.h"
#include "IDs.h"

// A CommandData plugin that hosts and opens a GeDialog based window (the 3delight shader node editor).

class NodeEditorDialogCommand : public CommandData
{
	INSTANCEOF(NodeEditorDialogCommand, CommandData)

private:
	NodeEditorDialog _dialog;

public:
	Bool Execute(BaseDocument* doc, GeDialog* parentManager)
	{
		if (_dialog.IsOpen() == false)
		{
			// open dialog window
			_dialog.Open(DLG_TYPE::ASYNC, ID_NODE_EDITOR_DIALOG, -1, -1, 1200, 800);
		}
		else
		{
			// close dialog window
			_dialog.Close();
		}

		return true;
	};

	Bool RestoreLayout(void* secret)
	{
		return _dialog.RestoreLayout(ID_NODE_EDITOR_DIALOG, 0, secret);
	};

	static NodeEditorDialogCommand* Alloc() { return NewObjClear(NodeEditorDialogCommand); }
};

Bool RegisterNodeEditorDialogCommand()
{
	return RegisterCommandPlugin(ID_NODE_EDITOR_DIALOG_COMMAND, String("3delight Shader Graph Editor"), PLUGINFLAG_HIDEPLUGINMENU, AutoBitmap(String("Lightcard.tif")), String("Opens the 3delight Shader Graph editor"), NewObjClear(NodeEditorDialogCommand));
}


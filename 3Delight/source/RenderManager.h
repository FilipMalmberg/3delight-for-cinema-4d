#pragma once
#include "c4d.h"
#include "SceneParser.h"
#include "nsi.hpp"
#include <queue>
#include <mutex>
#include <thread>

class RenderManager : public MessageData
{
public:
	RenderManager();
	~RenderManager();

	void FillFrames();

	void Render(NSI::Context& i_nsi);

	void StopRender(NSI::Context& i_nsi);

	std::string GetNSIExportFilename(BaseContainer* data, long i_frame) const;

	virtual Bool CoreMessage(Int32 id, const BaseContainer& bc);

	/// Returns true if a single frame is to be rendered
	bool SingleFrame()const { return m_start_time == m_end_time; }

	/// Returns true if rendering should be done in a background thread
	bool BackgroundThreadRendering()const
	{
		return SingleFrame() && !m_output_nsi && !m_batch;
	}

	/**
		\brief Returns true if rendering should be done in a background process.

		We use a renderdl process in order to start rendering as soon as the
		first frame is exported.
	*/
	bool BackgroundProcessRendering()const
	{
		return !SingleFrame() && !m_output_nsi;
	}

	/*
		A thread that waits for the sequence process to finish so we could be
		notified of it.
	*/
	std::thread m_sequence_waiter;
public:
	Int32 m_start_time =  0 , m_end_time = 0, m_frame_step = 1;
private:
	SceneParser* parser;

	/** The context which controls rendering sequences. */
	std::shared_ptr<NSI::Context> m_sequence_context;

	bool m_batch;

	bool m_output_nsi = false;

	bool m_ipr = false;

	/*
		Mutex controlling access to parser, specifically
		when handling various end-of-render situations.
	*/
	std::mutex m_render_end_mutex;
};

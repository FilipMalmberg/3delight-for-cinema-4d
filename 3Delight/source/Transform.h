#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <string>
#include "DL_SampleInfo.h"
#include "DL_SceneParser.h"
#include "c4d.h"

class Transform
{
private:
	BaseObject* obj;
	std::string handle;
	std::string null_handle;
	std::string parent;

public:
	int motionSamples;
	Transform(std::string object_handle, BaseObject* object, std::string parent);
	void CreateNodes(BaseDocument* document, DL_SceneParser* parser);
	void SampleAttributes(DL_SampleInfo* info,
						  BaseDocument* document,
						  DL_SceneParser* parser);
	std::string GetHandle();
	std::string GetNullHandle();
};

#endif
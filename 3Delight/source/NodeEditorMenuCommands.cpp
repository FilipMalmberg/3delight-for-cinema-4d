#pragma once
#include "c4d.h"
#include "NodeEditorDialog.h"
#include "DL_NodeMaterial.h"
#include "IDs.h"

GvCopyBuffer* copybuffer = NULL;

void CopySelectedNodes(GvNode* node) {
	GvNode* next_node = 0;
	while (node) {
		if (node->IsGroupNode()) {
			CopySelectedNodes(node->GetDown());
		}
		next_node = node->GetNext();
		if (node->GetSelectState()) {
			GvNodeMaster* master = node->GetNodeMaster();
			//GvNode* nodecopy = node->GetClone();

			//ApplicationOutput("Copy: "_s);
			//ApplicationOutput(node->GetTitle());
			
			//node->Remove();
			//master->FreeNode(node);
		}
		node = next_node;
	}
}

void RemoveSelectedNodes(GvNode* node) {
	while (node) {
		if (node->IsGroupNode()) {
			RemoveSelectedNodes(node->GetDown());
		}
		GvNode* next_node = node->GetNext();
		if (node->GetSelectState()) {
			//ApplicationOutput("Remove: "_s);
			//ApplicationOutput(node->GetTitle());
			GvNodeMaster* master = node->GetNodeMaster();
			node->Remove();
			master->FreeNode(node);
		}
		node = next_node;
	}
}

class NodeEditorDeleteCommand : public CommandData
{
	INSTANCEOF(NodeEditorDeleteCommand, CommandData)

public:
	Bool Execute(BaseDocument* doc, GeDialog* parentManager)
	{
		//ApplicationOutput("Delete selected nodes"_s);
		//NodeEditorDialog* editor = (NodeEditorDialog*)parentManager;
		
		BaseMaterial* activeMaterial = GetActiveDocument()->GetActiveMaterial();

		if (activeMaterial != nullptr && activeMaterial->GetType() == ID_DL_NODEMATERIAL) {
			DL_NodeMaterial* node_material = (DL_NodeMaterial*)(activeMaterial->GetNodeData<DL_NodeMaterial>());
			GvNodeMaster* nodeMaster = node_material->GetNodeMaster();

			if (nodeMaster != nullptr) {
				doc->StartUndo();
				doc->AddUndo(UNDOTYPE::CHANGE, nodeMaster);
				RemoveSelectedNodes(nodeMaster->GetRoot());
				doc->EndUndo();
				
				EventAdd();
				activeMaterial->Update(true, false);
			}
		}
		
		return true;
	};

	Bool Message(Int32 type, void *t_data)
	{
		CommandInformationData *cid = (CommandInformationData*)t_data;
		if (type == MSG_COMMANDINFORMATION && cid)
		{
			cid->managergroup = ID_NODE_EDITOR_DIALOG;
			if (cid->command_id == ID_DL_DELETE_NODES)
				cid->parentid = IDM_DELETE;
		}
		return true;
	}

};

class NodeEditorCopyCommand : public CommandData
{
	INSTANCEOF(NodeEditorCopyCommand, CommandData)

public:
	Bool Execute(BaseDocument* doc, GeDialog* parentManager)
	{
		BaseMaterial* activeMaterial = GetActiveDocument()->GetActiveMaterial();

		if (activeMaterial != nullptr && activeMaterial->GetType() == ID_DL_NODEMATERIAL) {
			DL_NodeMaterial* node_material = (DL_NodeMaterial*)(activeMaterial->GetNodeData<DL_NodeMaterial>());
			GvNodeMaster* nodeMaster = node_material->GetNodeMaster();

			if (nodeMaster != nullptr) {
				//doc->StartUndo();
				//doc->AddUndo(UNDOTYPE::CHANGE, nodeMaster);
				//CopySelectedNodes(nodeMaster->GetRoot());
				//CallCommand(300001010); // Copy
				//CallCommand(300001011); // Paste
				if (copybuffer!=NULL) {
					nodeMaster->FreeCopyBuffer(copybuffer); 
				}
				copybuffer=nodeMaster->GetCopyBuffer();

				//nodeMaster->PasteFromBuffer(*buffer);

				//doc->EndUndo();

				EventAdd();
				activeMaterial->Update(true, false);
			}
		}

		return true;

	};

	Bool Message(Int32 type, void *t_data)
	{
		CommandInformationData *cid = (CommandInformationData*)t_data;
		if (type == MSG_COMMANDINFORMATION && cid)
		{
			cid->managergroup = ID_NODE_EDITOR_DIALOG;
			if (cid->command_id == ID_DL_COPY_NODES)
				cid->parentid = IDM_COPY;
		}
		return true;
	}

};

class NodeEditorPasteCommand : public CommandData
{
	INSTANCEOF(NodeEditorPasteCommand, CommandData)

public:
	Bool Execute(BaseDocument* doc, GeDialog* parentManager)
	{
		//ApplicationOutput("Paste selected nodes"_s);
		BaseMaterial* activeMaterial = GetActiveDocument()->GetActiveMaterial();

		if (activeMaterial != nullptr && activeMaterial->GetType() == ID_DL_NODEMATERIAL) {
			DL_NodeMaterial* node_material = (DL_NodeMaterial*)(activeMaterial->GetNodeData<DL_NodeMaterial>());
			GvNodeMaster* nodeMaster = node_material->GetNodeMaster();

			if (nodeMaster != nullptr) {
				//doc->StartUndo();
				//doc->AddUndo(UNDOTYPE::CHANGE, nodeMaster);
				
				if (copybuffer != NULL) {
					nodeMaster->PasteFromBuffer(*copybuffer, GV_INSERT_AFTER);
				}
				
				//

				//doc->EndUndo();

				EventAdd();
				activeMaterial->Update(true, false);
			}
		}

		return true;

	};

	Bool Message(Int32 type, void *t_data)
	{
		CommandInformationData *cid = (CommandInformationData*)t_data;
		if (type == MSG_COMMANDINFORMATION && cid)
		{
			cid->managergroup = ID_NODE_EDITOR_DIALOG;
			if (cid->command_id == ID_DL_PASTE_NODES)
				cid->parentid = IDM_PASTE;
		}
		return true;
	}

};

Bool RegisterNodeEditorMenuCommands()
{
	bool OK = true;
	OK= RegisterCommandPlugin(ID_DL_DELETE_NODES, String("Delete"), PLUGINFLAG_HIDEPLUGINMENU, 0, String("Delete selected nodes"), NewObjClear(NodeEditorDeleteCommand));
	OK &= RegisterCommandPlugin(ID_DL_COPY_NODES, String("Copy"), PLUGINFLAG_HIDEPLUGINMENU, 0, String("Copy selected nodes"), NewObjClear(NodeEditorCopyCommand));
	OK &= RegisterCommandPlugin(ID_DL_PASTE_NODES, String("Paste"), PLUGINFLAG_HIDEPLUGINMENU, 0, String("Paste nodes"), NewObjClear(NodeEditorPasteCommand));
	return OK;
}


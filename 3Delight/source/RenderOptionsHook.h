#ifndef RENDER_OPTIODL_HOOK_H
#define RENDER_OPTIODL_HOOK_H

#include <vector>
#include "DL_HookPlugin.h"
#include "DL_Multi_Light.h"
#include "aov_layers.h"
#include "c4d.h"

class RenderOptionsHook : public DL_HookPlugin
{
public:
	virtual void CreateNSINodes(BaseDocument* doc, DL_SceneParser* parser);
	virtual void ConnectNSINodes(BaseDocument* doc, DL_SceneParser* parser);

	void ExportOneOutputLayer(
		DL_SceneParser* parser,
		const std::string& i_layer_handle,
		const std::string& i_variable_source,
		const std::string& i_scalar_format,
		const std::string& i_layer_type,
		const std::string& i_filter,
		const std::string& i_aov,
		double i_filter_width,
		int i_alpha,
		const std::string& i_screen_handle,
		const std::string& i_driver_handle,
		const std::string& i_driver_name,
		unsigned& io_sort_key);

	void ExportMultiLights(
		DL_SceneParser* parser,
		std::vector<BaseObject*> i_lights,
		const std::string& i_variable_source,
		const std::string& i_scalar_format,
		const std::string& i_layer_type,
		const std::string& i_aov,
		const std::string& i_screen_handle,
		const std::string& i_driver_handle,
		const std::string& i_driver_name,
		int i_index,
		unsigned& io_sort_key);

private:
	std::string m_layer_handle;
	std::string m_layer_jpg;
	std::string m_layer_file;
	std::string m_display_driver_handle;
	std::string m_file_driver_handle;
	std::string m_bitmapdriver_handle;
	std::string m_aov;
	std::string m_aovname;
	std::string m_layertype;
	std::string m_format;
	std::string m_variable_source;
	std::string m_variable_type;
	std::string m_output_driver_handle;
	std::string m_scalar_format;
	std::string m_extension;
	std::string m_output_format;
};

#endif
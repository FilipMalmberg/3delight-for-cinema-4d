#include "NodeEditorDialog.h"
#include "DL_NodeMaterial.h"

#define MAIN_GROUP 1500
#define NODE_GADGET 1501
#define ATTRIBUTE_MANAGER_GADGET 1502
#define SHADERGRAPH_GROUP 1503
#define AM_GROUP 1504

NodeEditorDialog::NodeEditorDialog() {
	_nodeGUI = nullptr;

	
}

NodeEditorDialog::~NodeEditorDialog() {
	GvGetWorld()->FreeNodeGUI(_nodeGUI);

}

Bool NodeEditorDialog::InitValues() {
	_weights.SetInt32(GROUPWEIGHTS_PERCENT_W_CNT, 2);
	_weights.SetFloat(GROUPWEIGHTS_PERCENT_W_VAL + 0, 75.0);  // weight for graphview
	_weights.SetFloat(GROUPWEIGHTS_PERCENT_W_VAL + 1, 25.0);  // weight for attribute manager
	GroupWeightsLoad(MAIN_GROUP, _weights);
	return true;
}

Bool NodeEditorDialog::CreateLayout()
{
	SetTitle("3Delight Shader Graph Editor"_s);

	MenuFlushAll();
	MenuSubBegin("Edit"_s);
	MenuAddCommand(12105); //Undo
	MenuAddCommand(12297); //Redo
	MenuAddSeparator();
	MenuAddCommand(ID_DL_COPY_NODES); 
	MenuAddCommand(ID_DL_PASTE_NODES);
	MenuAddCommand(ID_DL_DELETE_NODES);
	MenuAddSeparator();
	MenuAddCommand(12112); //Select all
	MenuAddCommand(12113); //Deselect all
	MenuFinished();

	if (_nodeGUI == nullptr)
	{
		_shape = GvGetWorld()->AllocShape();
		_group = GvGetWorld()->AllocGroupShape();

		_nodeGUI = GvGetWorld()->AllocNodeGUI(_shape, _group, NODE_GADGET);
	}

	//GePrint("Create layout"_s);

	GroupBegin(MAIN_GROUP, BFH_RIGHT|BFH_SCALEFIT|BFV_SCALEFIT, 2, 1, ""_s, BFV_GRIDGROUP_ALLOW_WEIGHTS);
	GroupBegin(SHADERGRAPH_GROUP, BFH_RIGHT | BFH_SCALEFIT | BFV_SCALEFIT, 1, 1, ""_s, 0);
	GroupEnd();
	GroupBegin(AM_GROUP, BFH_RIGHT | BFH_SCALEFIT | BFV_SCALEFIT, 1, 1, ""_s, 0);

	//Graph view
	//_nodeGUI->Attach(this, 0);
	//C4DGadget* gadget = AddUserArea(NODE_GADGET, BFH_SCALEFIT | BFV_SCALEFIT, 0, 0);
	//AttachUserArea(*_nodeGUI->GetUserArea(), gadget, USERAREAFLAGS::TABSTOP | USERAREAFLAGS::HANDLEFOCUS);

	//Attribute manager
	BaseContainer customgui;
	customgui.SetBool(DESCRIPTION_ALLOWFOLDING, true);
	customgui.SetBool(DESCRIPTION_SHOWTITLE, true);
	am = (DescriptionCustomGui*)AddCustomGui(ATTRIBUTE_MANAGER_GADGET, CUSTOMGUI_DESCRIPTION, String(), BFH_SCALEFIT | BFV_SCALEFIT, 0, 0, customgui);

	GroupEnd();
	//UpdateDialog();
	GroupEnd();

	/*MenuFlushAll();
	// This example adds a group to the menu bar.
	// This menu group contains a button.

	GroupBeginInMenuLine();

	GroupBegin(1000, BFH_RIGHT, 2, 1, ""_s, 0);
	AddButton(100, BFH_SCALEFIT, 0, 10, "Button"_s);
	GroupEnd();

	GroupEnd();*/

	/*C4DGadget* const userAreaGadget = this->AddUserArea(ID_UV_USER_AREA, BFH_SCALEFIT | BFV_SCALEFIT | BFH_LEFT | BFV_BOTTOM, 800, 800);
	if (userAreaGadget) {
		this->AttachUserArea(_UserArea, userAreaGadget);
	}*/

	UpdateDialog();

	return true;
}

GvNode* GetFirstSelected(GvNode* node) {
	while (node) {
		if (node->GetBit(BIT_ACTIVE)) {
			return node;
		}
		GvNode* tmp = GetFirstSelected(node->GetDown());
		if (tmp != nullptr) {
			return tmp;
		}

		node = node->GetNext();
	}
	return nullptr;
}

void NodeEditorDialog::UpdateDialog() {


	// clear the group
	LayoutFlushGroup(SHADERGRAPH_GROUP);
	Bool found_material = false;

	BaseMaterial* activeMaterial = GetActiveDocument()->GetActiveMaterial();


	if (activeMaterial != nullptr && activeMaterial->GetType() == ID_DL_NODEMATERIAL) {
		DL_NodeMaterial* node_material = (DL_NodeMaterial*) (activeMaterial->GetNodeData<DL_NodeMaterial>());
		activeMaterial->Update(true, false); //Update the material preview swatch for the active material
		GvNodeMaster* nodeMaster = node_material->GetNodeMaster();
	
		if (nodeMaster != nullptr) {
	
			nodeMaster->GetRoot()->SetName(activeMaterial->GetName());
			found_material = true;
			
			//GroupBegin(71254, BFH_SCALEFIT | BFV_SCALEFIT, 2, 1, String("My group"), BFV_GRIDGROUP_ALLOW_WEIGHTS);

			//Graph view
			_nodeGUI->Attach(this, nodeMaster);
			C4DGadget* gadget = AddUserArea(NODE_GADGET, BFH_SCALEFIT | BFV_SCALEFIT, 0, 0);
			AttachUserArea(*_nodeGUI->GetUserArea(), gadget, USERAREAFLAGS::TABSTOP | USERAREAFLAGS::HANDLEFOCUS);

			//Attribute manager
			/*BaseContainer customgui;
			customgui.SetBool(DESCRIPTION_ALLOWFOLDING, true);
			customgui.SetBool(DESCRIPTION_SHOWTITLE, true);
			am = (DescriptionCustomGui*)AddCustomGui(ATTRIBUTE_MANAGER_GADGET, CUSTOMGUI_DESCRIPTION, String(), BFH_SCALEFIT | BFV_SCALEFIT, 0, 0, customgui);*/

			GvNode* active_node = GetFirstSelected(nodeMaster->GetRoot());
			
			am->SetObject(active_node); 
			//GroupEnd();
		}
	

	}


	
	LayoutChanged(SHADERGRAPH_GROUP);
	


}

Int32 NodeEditorDialog::Message(const BaseContainer & 	msg, BaseContainer & 	result) {
	//if (msg.GetId() == GV_MESSAGE_NODE_SELECTED) {
	//	BaseDocument* doc = GetActiveDocument();
		//if (!doc) { return false; }
	//	GePrint("Node selected"_s);
		//BaseList2D* node = msg.GetLink(GV_MESSAGE_FIRST_DATA_ID + 0, doc);
		//GePrint(String("Node selected: ")+node->GetName());
	//}
	//ActiveObjectManager_SetMode(ACTIVEOBJECTMODE(DL_GRAPHVIEW_MODE), false);

	
	
	if (msg.GetId() == BFM_WEIGHTS_CHANGED){    // save the weights
		GroupWeightsSave(MAIN_GROUP, _weights);
	}
	else if (msg.GetId() == BFM_LAYOUT_GETDATA) {     // store the weights
		result = BaseContainer(0);
		result.SetContainer(10000, _weights);
		return true;
	}
	else if(msg.GetId() == BFM_LAYOUT_SETDATA) {   // restore the weights
		const BaseContainer* bc = msg.GetContainerInstance(BFM_LAYOUT_SETDATA);
		if (bc)
		{
			// access the sub-container with the ID 10000
			const BaseContainer* const weightsContainer = bc->GetContainerInstance(10000);

			if (weightsContainer)
				_weights = *weightsContainer;

			GroupWeightsLoad(MAIN_GROUP, _weights);

			return true;
		}
	}
	/*else if (msg.GetId() == BFM_INPUT) {

		if (msg.GetInt32(BFM_INPUT_DEVICE) == BFM_INPUT_KEYBOARD) {
			const String key = msg.GetString(BFM_INPUT_ASC);
			const Int32 qualifier = msg.GetInt32(BFM_INPUT_QUALIFIER);
			if (qualifier & QCTRL) {
				ApplicationOutput("Ctrl is down"_s);
			}
			ApplicationOutput(key);

			if (msg.GetInt32(BFM_INPUT_CHANNEL) == KEY_DELETE || msg.GetInt32(BFM_INPUT_CHANNEL) == KEY_BACKSPACE)
			{
				ApplicationOutput("Delete selected nodes"_s);
				return true;
			}
			else if (key == String("c")) {
				ApplicationOutput("Copy selected nodes"_s);
				return true;

			}
		}
	}*/



	return GeDialog::Message(msg, result);
}

Bool NodeEditorDialog::Command(Int32 	id, const BaseContainer & 	msg) {
	//GePrint("Command"_s);
	//ActiveObjectManager_SetMode(ACTIVEOBJECTMODE(DL_GRAPHVIEW_MODE), false);
	return true;
}

Bool NodeEditorDialog::CoreMessage(Int32 	id, const BaseContainer & 	msg) {

	if (id == EVMSG_MATERIALSELECTION) {
		//GePrint("Material selection event"_s);
	}
	if (id == EVMSG_GRAPHVIEWCHANGED) {
		//GePrint("Graph view change!"_s);
	}
	if (id == EVMSG_CHANGE) {
		UpdateDialog();
		//GePrint("Change event"_s);
		
	}

	/*if (id == EVMSG_CHANGE|| id==EVMSG_DOCUMENTRECALCULATED){
		if (_nodeGUI != nullptr) {
			//GePrint("Redraw node GUI"_s);
			_nodeGUI->Redraw();
		}
	}*/

	

	return GeDialog::CoreMessage(id, msg);

}

#include "DL_NodeMaterial.h"
#include "mnodematerial.h"
#include "customgui_matpreview.h"
#include "IDs.h"

GvNodeMaster* DL_NodeMaterial::GetNodeMaster() {
	return node_master;
}


static void DL_SetDefaultMatpreview(BaseMaterial* material)
{
	GeData d;

	if (material->GetParameter(MATERIAL_PREVIEW, d, DESCFLAGS_GET::NONE)) {
		MaterialPreviewData* mpd =
			(MaterialPreviewData*)d.GetCustomDataType(CUSTOMDATATYPE_MATPREVIEW);

		if (mpd) {
			mpd->SetPreviewType(MatPreviewUser);
			mpd->SetUserPreviewSceneName(String("DL_Sphere"));
			material->SetParameter(MATERIAL_PREVIEW, d, DESCFLAGS_SET::NONE);
		}
	}
}

Bool DL_NodeMaterial::Init(GeListNode* node) {
	node_master = GvGetWorld()->AllocNodeMaster((BaseList2D*)(this->Get()));

	//Create a terminal node for this material
	node_master->CreateNode(node_master->GetRoot(), DL_TERMINAL, nullptr, 200,200); 

	DL_SetDefaultMatpreview((BaseMaterial*)node);

	return node_master != nullptr;

}

void DL_NodeMaterial::Free(GeListNode* node) {
	GvGetWorld()->FreeNodeMaster(node_master);
}

Bool DL_NodeMaterial::Read(GeListNode *node, HyperFile *hf, Int32 level) {
	return node_master->ReadObject(hf,true);
}

Bool DL_NodeMaterial::Write(GeListNode *node, HyperFile *hf) {
	return node_master->WriteObject(hf);
}

Bool DL_NodeMaterial::CopyTo(NodeData *dest, GeListNode *snode, GeListNode *dnode, COPYFLAGS flags, AliasTrans *trn) {
	DL_NodeMaterial* dst = (DL_NodeMaterial*)dest;
	return node_master->CopyTo(dst->GetNodeMaster(), flags, trn);
}


Bool DL_NodeMaterial::Message(GeListNode *node, Int32 type, void *data){
	if (type == MSG_DESCRIPTION_COMMAND) {
		DescriptionCommand* dc = (DescriptionCommand*)data;
		if (dc->_descId[0].id == OPEN_GRAPH_EDITOR)
		{
			CallCommand(ID_NODE_EDITOR_DIALOG_COMMAND);
		}
	}
	

	return MaterialData::Message(node, type, data);
}

//Initializes resources for rendering.
INITRENDERRESULT DL_NodeMaterial::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}


void DL_NodeMaterial::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{

	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);

	vd->col = 0.8*diff;
}

Bool RegisterDLNodeMaterial(void)
{
	return RegisterMaterialPlugin(ID_DL_NODEMATERIAL, "DL_NodeMaterial"_s, 0, DL_NodeMaterial::Alloc, "Mnodematerial"_s, 0);
}

#include "c4d.h"
#include "DL_NodeMaterial.h"

//Get the terminal node of a 3delight shader graph, if one exists
GvNode* GetTerminal(GvNode* node){
	while (node) {
		if (node->GetOperatorID() == DL_TERMINAL) {
			return node;
		}
		GvNode* tmp = GetTerminal(node->GetDown());
		if (tmp) {
			return tmp;
		}
		node = node->GetNext();
	}
	return nullptr;

}

class CreateNodeMaterial : public CommandData
{
public:
	int shelf_used = 0;
	int node_id=-1;
	String material_name;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

#define ID_DL_NODEMATERIAL 	1062101

Bool CreateNodeMaterial::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	if (node_id == -1) {
		return false;
	}

	doc->StartUndo();

	BaseMaterial* material = BaseMaterial::Alloc(ID_DL_NODEMATERIAL);
	material->SetName(material_name);

	DL_NodeMaterial* nodematerial= (DL_NodeMaterial*)(material->GetNodeData<DL_NodeMaterial>());

	GvNodeMaster* nodeMaster = nodematerial->GetNodeMaster();

	if (nodeMaster != nullptr) {
		GvNode* material_node= nodeMaster->CreateNode(nodeMaster->GetRoot(), node_id, nullptr, 100, 200);
		int nports = material_node->GetOutPortCount();
		GvPort* outport = 0;
		for (int i = 0; i < nports; i++) {
			GvPort* tmp = material_node->GetOutPort(i);
			if (tmp->GetName(material_node) == "outColor"){
				outport = tmp;
			}
		}
	
		GvNode* terminal = GetTerminal(nodeMaster->GetRoot());
		if (terminal != nullptr) {
			nports = terminal->GetInPortCount();
			GvPort* inport=0;
			for (int i = 0; i < nports; i++) {
				GvPort* tmp = terminal->GetInPort(i);
				if (tmp->GetName(terminal) == "Surface") {
					inport = tmp;
				}
			}
			if (nodeMaster->IsConnectionValid(material_node, outport, terminal, inport, material_node, outport, terminal, inport)) {
				terminal->AddConnection(material_node, outport, terminal, inport);
			}
			
		}
		
	}

	doc->AddUndo(UNDOTYPE::NEWOBJ, material);

	doc->InsertMaterial(material);

	EventAdd();
	doc->EndUndo();
	return true;
}

Bool RegisterCreateNodeMaterial(Int32 ID, Int32 shader_ID, String name, String icon)
{
	CreateNodeMaterial* instance = NewObjClear(CreateNodeMaterial);
	instance->node_id = shader_ID;
	instance->material_name = name;
	return RegisterCommandPlugin(ID, name, PLUGINFLAG_HIDEPLUGINMENU, AutoBitmap(icon), String("Create a new " + name + " material"), instance);
}
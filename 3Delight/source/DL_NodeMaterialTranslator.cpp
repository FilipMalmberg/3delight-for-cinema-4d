#include "DL_NodeMaterialTranslator.h"
#include "DL_NodeMaterial.h"
#include "IDs.h"
#include "nsi.hpp"
//#include "delightenvironment.h"
#include <c4d_graphview.h>

void DL_NodeMaterialTranslator::CreateNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());

	std::string attributes_handle = (std::string) Handle;
	ctx.Create(attributes_handle, "attributes");
	
}

/*void DL_NodeMaterialTranslator::ConnectNSINodes(const char* Handle,
	const char* ParentTransformHandle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser)
{
	

}

void DL_NodeMaterialTranslator::SampleAttributes(DL_SampleInfo* info,
	const char* Handle,
	BaseList2D* C4DNode,
	BaseDocument* doc,
	DL_SceneParser* parser)
{
	
}*/

//void DL_NodeMaterialTranslator::RegisterAttributeNames(DL_PluginManager* PM) {
	//PM->RegisterAttributeName(DUMMY_SHADER, SURFACE_COLOR, "Color");
	//PM->RegisterAttributeName(DUMMY_SHADER, OUTPUT, "Output");
//}
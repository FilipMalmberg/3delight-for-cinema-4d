#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <map>
#include <memory>
#include <set>
#include <vector>
#include "DL_API.h"
#include "c4d.h"

typedef std::map<long, TranslatorAllocator> TranslatorMap;
typedef std::shared_ptr<DL_Hook> DL_HookPtr;
typedef std::map< std::pair<long, long>, std::string> AttributeNameMap;

class PluginManager : public DL_PluginManager
{
private:
	TranslatorMap Allocators;
	std::vector<HookAllocator> HookAllocators;
	std::set<long> LightTypes;
	AttributeNameMap attribute_names;

public:
	virtual ~PluginManager();

	virtual long GetAPIVersion();

	virtual void RegisterHook(HookAllocator allocator);
	virtual void RegisterTranslator(long id,
									TranslatorAllocator allocator,
									bool IsLight = false);
	virtual void RegisterAttributeName(long nodeID, long attributeID, const char* name);

	bool IsLight(BaseList2D* item);

	DL_Translator* GetTranslator(long id);
	std::vector<DL_HookPtr> GetHooks();
	std::string GetAttributeName(long nodeID, long attributeID);

};

#endif

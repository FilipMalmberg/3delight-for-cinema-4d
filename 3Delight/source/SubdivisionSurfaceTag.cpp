#include "IDs.h"
#include "c4d.h"
#include "RegisterPrototypes.h"
#include "tsubdiv.h"

class DL_SubdivisionSurfaceTag : public TagData
{
public:
	virtual Bool Init(GeListNode* node);

	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_SubdivisionSurfaceTag);
	}
};

Bool DL_SubdivisionSurfaceTag::Init(GeListNode* node)
{
	BaseTag* tag = (BaseTag*) node;
	BaseContainer* data = tag->GetDataInstance();
	data->SetBool(RENDER_AS_SDS, true);
	return TRUE;
}

Bool RegisterDL_SubdivisionSurfaceTag(void)
{
	return RegisterTagPlugin(ID_DL_SUBDIVISIONSURFACETAG,
							 "Subdivision Surface"_s,
							 TAG_VISIBLE,
							 DL_SubdivisionSurfaceTag::Alloc,
							 "tsubdiv"_s,
							 AutoBitmap("subdivide_surface.png"_s),
							 0);
}

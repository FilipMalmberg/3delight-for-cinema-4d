#pragma once
#include "c4d.h"
#include "IDs.h"
#include <c4d_graphview.h>

class DL_NodeMaterial : public MaterialData
{
	INSTANCEOF(DL_NodeMaterial, MaterialData)

private:
	GvNodeMaster* node_master;
	
public:


	GvNodeMaster* GetNodeMaster();

	virtual Bool Init(GeListNode* node);
	virtual void Free(GeListNode* node);

	virtual Bool Read(GeListNode *node, HyperFile *hf, Int32 level);
	virtual Bool Write(GeListNode *node, HyperFile *hf);
	virtual Bool CopyTo(NodeData *dest, GeListNode *snode, GeListNode *dnode, COPYFLAGS flags, AliasTrans *trn);
	//Int32 GetBranchInfo(GeListNode *node, BranchInfo *info, Int32 max, GETBRANCHINFO flags);

	virtual Bool Message(GeListNode *node, Int32 type, void *data);

	virtual	INITRENDERRESULT InitRender(BaseMaterial* mat, const InitRenderStruct& irs);
	virtual	void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc() {
		return NewObjClear(DL_NodeMaterial);
	}


};
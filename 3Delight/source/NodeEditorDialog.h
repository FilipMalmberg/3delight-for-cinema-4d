#pragma once
#include "c4d.h"
#include <c4d_graphview.h>
#include "IDs.h"

class NodeEditorDialog : public GeDialog
{
	INSTANCEOF(NodeEditorDialog, GeDialog)

private:

	// Xpresso Node GUI data
	GvShape* _shape;
	GvShape* _group;
	GvNodeGUI* _nodeGUI;

	//Attribute manager
	DescriptionCustomGui* am;

	//Layout weights
	BaseContainer _weights;

public:
	NodeEditorDialog();
	~NodeEditorDialog();

	Bool InitValues();
	Bool CreateLayout();
	Bool CoreMessage(Int32 	id, const BaseContainer & 	msg);
	Int32 Message(const BaseContainer & 	msg, BaseContainer & 	result);
	Bool Command(Int32 	id, const BaseContainer & 	msg);
	void UpdateDialog();
}; 

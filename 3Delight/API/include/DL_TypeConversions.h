#pragma once

#include <string>
#include <vector>
#include "c4d.h"


inline std::vector<float>
MatrixToNSIMatrix(Matrix m)
{
	std::vector<float> v(16);
	v[0] = m.sqmat.v1.x;
	v[1] = m.sqmat.v1.y;
	v[2] = m.sqmat.v1.z;
	v[3] = 0.0;
	v[4] = m.sqmat.v2.x;
	v[5] = m.sqmat.v2.y;
	v[6] = m.sqmat.v2.z;
	v[7] = 0.0;
	v[8] = m.sqmat.v3.x;
	v[9] = m.sqmat.v3.y;
	v[10] = m.sqmat.v3.z;
	v[11] = 0.0;
	v[12] = m.off.x;
	v[13] = m.off.y;
	v[14] = m.off.z;
	v[15] = 1.0;
	return v;
}

inline std::vector<double>
MatrixToNSIDoubleMatrix(Matrix m)
{
	std::vector<double> v(16);
	v[0] = m.sqmat.v1.x;
	v[1] = m.sqmat.v1.y;
	v[2] = m.sqmat.v1.z;
	v[3] = 0.0;
	v[4] = m.sqmat.v2.x;
	v[5] = m.sqmat.v2.y;
	v[6] = m.sqmat.v2.z;
	v[7] = 0.0;
	v[8] = m.sqmat.v3.x;
	v[9] = m.sqmat.v3.y;
	v[10] = m.sqmat.v3.z;
	v[11] = 0.0;
	v[12] = m.off.x;
	v[13] = m.off.y;
	v[14] = m.off.z;
	v[15] = 1.0;
	return v;
}



inline std::vector<char>
StringToChars(String s)
{
	long max = s.GetCStringLen();
	std::vector<char> cstr(max + 1);
	s.GetCString(&cstr[0], max + 1);
	return cstr;
}

inline std::string
StringToStdString(String s)
{
	maxon::String maxon_str = (maxon::String) s;
	std::string result;
	maxon::Result<maxon::BaseArray<maxon::Char>> str = maxon_str.GetCString();

	if (str.GetValue().GetCount() > 0) {
		result.resize(str.GetValue().GetCount());

		for (int i = 0; i < str.GetValue().GetCount(); i++) {
			result[i] = str.GetValue()[i];
		}
	}

	return result;
}

#include <string.h>
#include "DL_API.h"
#include "HairObjectTranslator.h"
#include "InstanceObjectTranslator.h"
#include "PolygonObjectTranslator.h"
#include "C4DSelectionTagTranslator.h"
#include "DLAtmosphere_Translator.h"
#include "DLOpenVDBNode_Translator.h"

#include "IDs.h"

#include "c4d.h"

Bool RegisterDLAtmosphere(void);
Bool RegisterOpenVDB(void);

Bool PluginStart(void)
{
	if (!RegisterDLAtmosphere()) {
		return FALSE;
	}

	if (!RegisterOpenVDB()) {
		return FALSE;
	}

	return true;
}

void PluginEnd(void) {}

Bool PluginMessage(Int32 id, void* data)
{
	switch (id) {
	case C4DPL_INIT_SYS:
		if (!g_resource.Init()) {
			return FALSE; // don't start plugin without resource
		}

		return TRUE;
		break;

	case DL_LOAD_PLUGINS:
		DL_PluginManager* pm = (DL_PluginManager*) data;
		//Object types
		pm->RegisterTranslator(Opolygon, AllocateTranslator<PolygonObjectTranslator>);
		pm->RegisterTranslator(Ohair, AllocateTranslator<HairObjectTranslator>);
		pm->RegisterTranslator(Oinstance, AllocateTranslator<InstanceObjectTranslator>);
		
		//Volume
		pm->RegisterTranslator(DL_ATMOSPHERE_OBJECT, AllocateTranslator<Atmosphere_Translator>);
		pm->RegisterTranslator(DL_OPENVDB, AllocateTranslator<Delight_OpenVDBTranslator>);

		//Tags
		pm->RegisterTranslator(Tpolygonselection, AllocateTranslator<C4DSelectionTagTranslator>);
		break;
	}

	return FALSE;
}

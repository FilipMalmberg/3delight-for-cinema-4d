#pragma once

enum {
	DL_ATMOSPHERE = 100,
	DL_ATMOSPHERE_SHADER,
	DL_ATMOSPHERE_COLOR,
	DL_ATMOSPHERE_DENSITY,
	DL_ATMOSPHERE_SUPER_REFLECTIVE,
	DUMMY

};
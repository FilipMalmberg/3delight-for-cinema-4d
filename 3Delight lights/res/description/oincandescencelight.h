#pragma once

enum {
	INCANDESCENCE_LIGHT_COLOR = 1000,
	INCANDESCENCE_LIGHT_INTENSITY,
	INCANDESCENCE_LIGHT_EXPOSURE,
	INCANDESCENCE_LIGHT_GEOMETRY,
	INCANDESCENCE_LIGHT_ILLUMINATES,
	INCANDESCENCE_LIGHT_GROUP,
	DUMMY_VALUE
};

#pragma once

#include "c4d.h"

class LightFiltersCustomDataTypeClass; // forward declaration

/**
        In this class we have stored the information that we need for the light
   filters. We have inherited from iCustomDataType so we can override its
   funtcions to store this data and also write them on a hyperfile so we can
   access them even after we have closed c4d.
*/
class iCustomDataTypeFilters : public iCustomDataType<iCustomDataTypeFilters>
{
	friend class LightFiltersCustomDataTypeClass;

public:
	maxon::BaseArray<maxon::String> m_light_filters_selected_layers;
	maxon::BaseArray<Int32> m_selected_light_filters_itemID;
	maxon::BaseArray<maxon::String> m_selected_light_filters_GUID;
	maxon::BaseArray<maxon::String> m_all_light_filters;
	iCustomDataTypeFilters() {}
};

class LightFitersDialog : public GeDialog
{
private:
	SimpleListView m_filtersview;
	AutoAlloc<BaseSelect> m_selection;

public:
	LightFitersDialog();
	virtual ~LightFitersDialog();
	maxon::BaseArray<String> m_aov_selector_selected;
	maxon::BaseArray<Int32> m_aov_selector_selectedID;
	iCustomDataTypeFilters* m_data;
	virtual Bool CreateLayout();
	virtual Bool InitValues();
	virtual Bool Command(Int32 i_id, const BaseContainer& i_msg);
	virtual Int32 Message(const BaseContainer& i_msg, BaseContainer& i_result);
};

/**
iCustomDataTypeLightFilters is the class used to buid the Filters View and
it inherits from iCustomGui because the tree view for the LighFilters
can not be build directly on a descriprion UI so we use Custom Gui
DataType to build on a parameter description. Basically we are putting
a GeDialog in a parameter description UI using the iCustomGui class
*/

class iCustomDataTypeLightFilters : public iCustomGui

{
	INSTANCEOF(iCustomDataTypeLightFilters, iCustomGui)

private:
	iCustomDataTypeFilters m_data;
	LightFitersDialog m_dialog;
	SimpleListView m_lightfiters;
	AutoAlloc<BaseSelect> m_light_filter_selection;
	TreeViewCustomGui* m_filter_view;

public:
	iCustomDataTypeLightFilters(const BaseContainer& i_settings,
								CUSTOMGUIPLUGIN* i_plugin);
	virtual Bool CreateLayout();
	virtual Bool InitValues();
	virtual Int32 Message(const BaseContainer& msg, BaseContainer& result);
	virtual Bool CoreMessage(Int32 id, const BaseContainer& msg);
	virtual Bool Command(Int32 i_id, const BaseContainer& i_msg);
	virtual Bool SetData(const TriState<GeData>& i_tristate);
	virtual TriState<GeData> GetData();
};

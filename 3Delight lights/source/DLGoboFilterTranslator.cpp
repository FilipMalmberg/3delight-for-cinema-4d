#include "DLGoboFilterTranslator.h"
#include "delightenvironment.h"
#include "DL_Utilities.h"
#include "IDs.h"
#include "dlgobofilter.h"
#include "nsi.hpp"

void GoboFilterTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseShader* shader = (BaseShader*) C4DNode;
	BaseContainer* data = shader->GetDataInstance();
	Bool is_filter_selected = data->GetBool(IS_SELECTED);

	if (!is_filter_selected) {
		return;
	}

	std::string filter_transform = (std::string) ParentTransformHandle;
	std::string filter_shader = (std::string) Handle + "shader";
	ctx.Create(filter_transform, "transform");
	ctx.Create(filter_shader, "shader");
}

void GoboFilterTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	return;
}

void GoboFilterTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	Bool is_filter_selected = data->GetInt32(IS_SELECTED);

	if (!is_filter_selected) {
		return;
	}

	std::string filter_shader = (std::string) Handle + "shader";
	std::string c_shaderpath_gobo = (GeGetPluginPath().GetDirectory() + Filename("osl")
							+ Filename("Bitmap.oso")).GetString().GetCStringCopy();
	Filename file = data->GetFilename(GOBO_MAP);
	std::string map_dir = "";

	if (file.IsPopulated()) {
		map_dir = file.GetString().GetCStringCopy();
	}

	float gobo_density = (float) data->GetFloat(GOBO_DENSITY);
	int gobo_invert = data->GetBool(GOBO_INVERT);
	float scale[2];
	scale[0] = float(data->GetFloat(GOBO_SCALE_START));
	scale[1] = float(data->GetFloat(GOBO_SCALE_END));
	float offset[2];
	offset[0] = float(data->GetFloat(GOBO_OFFSET_START));
	offset[1] = float(data->GetFloat(GOBO_OFFSET_END));
	NSI::ArgumentList argsfilter;
	argsfilter.Add(NSI::Argument::New("scale")
				   ->SetArrayType(NSITypeFloat, 2)
				   ->CopyValue((float*) &scale[0], 2 * sizeof(float)));
	argsfilter.Add(NSI::Argument::New("offset")
				   ->SetArrayType(NSITypeFloat, 2)
				   ->CopyValue((float*) &offset[0], 2 * sizeof(float)));
	int swrap = data->GetInt32(S_WRAP_MODE);
	int twrap = data->GetInt32(T_WRAP_MODE);
	std::string swrap_val = "";
	std::string twrap_val = "";

	switch (swrap) {
	case S_WRAP_BLACK:
		swrap_val = "black";
		break;

	case S_WRAP_CLAMP:
		swrap_val = "clamp";
		break;

	case S_WRAP_PERIODIC:
		swrap_val = "periodic";
		break;

	case S_WRAP_MIRROR:
		swrap_val = "mirror";
		break;

	default:
		break;
	}

	switch (twrap) {
	case T_WRAP_BLACK:
		twrap_val = "black";
		break;

	case T_WRAP_CLAMP:
		twrap_val = "clamp";
		break;

	case T_WRAP_PERIODIC:
		twrap_val = "periodic";
		break;

	case T_WRAP_MIRROR:
		twrap_val = "mirror";
		break;

	default:
		break;
	}

	ctx.SetAttributeAtTime(filter_shader,
						   info->sample_time,
						   (NSI::StringArg("shaderfilename", c_shaderpath_gobo),
							NSI::StringArg("textureName.meta.colorspace", ""),
							NSI::StringArg("textureName", map_dir.c_str()),
							NSI::FloatArg("density", gobo_density),
							NSI::IntegerArg("invert", gobo_invert),
							NSI::StringArg("swrap", swrap_val.c_str()),
							NSI::StringArg("twrap", twrap_val.c_str())));
	ctx.SetAttributeAtTime(filter_shader, info->sample_time, (argsfilter));
}

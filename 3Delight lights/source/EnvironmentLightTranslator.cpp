#include "EnvironmentLightTranslator.h"
#include "delightenvironment.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "IDs.h"
#include "c4d.h"
#include "nsi.hpp"
#include "oenvironmentlight.h"

#ifdef __APPLE__
#	pragma clang diagnostic ignored "-Wimplicit-float-conversion"
#endif

void EnvironmentLightTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());

	m_handle = (std::string) Handle;
	m_transform_handle = std::string(ParentTransformHandle);
	std::string transform_handle2 =(std::string) Handle + (std::string) "transform";

	ctx.Create(transform_handle2, "transform");
	std::vector<double> change_direction{ 0, 0, -1, 0, 0, 1, 0, 0,
										  -1, 0, 0, 0, 0, 0, 0, 1 };
	NSI::Argument xform("transformationmatrix");
	xform.SetType(NSITypeDoubleMatrix);
	xform.SetValuePointer((void*) &change_direction[0]);
	ctx.SetAttribute(transform_handle2, xform);

	ctx.Connect(transform_handle2, "", m_transform_handle, "objects");
	
	//Create an environment shape NSI node
	ctx.Create(m_handle, "environment");
	ctx.Connect(m_handle, "", transform_handle2, "objects");

	//Create an attribute node and connect it to geometry attributes
	std::string attributes_handle =(std::string) Handle + std::string("attributes");
	ctx.Create(attributes_handle, "attributes");
	ctx.Connect(attributes_handle, "", m_handle, "geometryattributes");

	// Create a shader for the enviroment node and connect it to the geometry attributes of the environment node
	m_shader_handle = (std::string) Handle + std::string("shader");
	ctx.Create(m_shader_handle, "shader");
	
	const char* delightpath = DelightEnv::getDelightEnvironment();
	//std::string shaderpath = delightpath + (std::string) "/osl" + (std::string) "/dlEnvironment.oso";
	std::string shaderpath = delightpath + (std::string) "/osl" + (std::string) "/environmentLight.oso";
	NSI::ArgumentList shader_args;
	shader_args.Add(new NSI::StringArg("shaderfilename", std::string(&shaderpath[0])));
	ctx.SetAttribute(m_shader_handle, shader_args);

	ctx.Connect(m_shader_handle, "", attributes_handle, "surfaceshader");

	//Create a shader for the environment shader
	std::string envtexture_handle = (std::string) Handle + std::string("texture");
	ctx.Create(envtexture_handle, "shader");
	std::string envshaderpath = delightpath + (std::string) "/cinema4d//osl" + (std::string) "/c4d_environmentTexture.oso";

	NSI::ArgumentList envtexture_shader_args;
	envtexture_shader_args.Add(new NSI::StringArg("shaderfilename", std::string(&envshaderpath[0])));
	ctx.SetAttribute(envtexture_handle, envtexture_shader_args);

	//ctx.Connect(envtexture_handle, "outColor", m_shader_handle, "i_texture");

}

void EnvironmentLightTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	
}

void EnvironmentLightTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	
	
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	

	float intensity = data->GetFloat(ENVIRONMENT_INTENSITY);
	float exposure = data->GetFloat(ENVIRONMENT_EXPOSURE);
	
	float texturecolor[3] { (float) 1,
							(float) 1,
							(float) 1
						  };
	
	Vector tint_c4d = toLinear(data->GetVector(ENVIRONMENT_TINT),doc);
	float tint[3] { (float) tint_c4d.x, (float) tint_c4d.y, (float) tint_c4d.z };
	
	int mapping = data->GetInt32(ENVIRONMENT_MAPPING);
	int camera_visibility = data->GetBool(ENVIRONMENT_SEEN_BY_CAMERA);
	int prelit = data->GetBool(ENVIRONMENT_PRELIT);
	bool illumination = data->GetBool(ENVIRONMENT_ILLUMINATES_BY_DEFAULT);
	Filename texturefile = data->GetFilename(ENVIRONMENT_TEXTURE_NAME);

	std::string mapname = StringToStdString(texturefile.GetString());
	
	NSI::ArgumentList attributes_args;
	attributes_args.Add(new NSI::IntegerArg("visibility.camera", camera_visibility));
	attributes_args.Add(new NSI::IntegerArg("prelit", prelit));
	attributes_args.Add(new NSI::IntegerArg("visibility", illumination));

	std::string attributes_handle =(std::string) Handle + std::string("attributes");
	ctx.SetAttribute(attributes_handle, attributes_args);
	
	NSI::ArgumentList shader_args;
	shader_args.Add(new NSI::FloatArg("intensity", intensity));
	shader_args.Add(new NSI::FloatArg("exposure", exposure));
	shader_args.Add(new NSI::ColorArg("i_texture", texturecolor));
	shader_args.Add(new NSI::ColorArg("i_color", tint));
	shader_args.Add(new NSI::IntegerArg("mapping", mapping));
	shader_args.Add(new NSI::StringArg("image", mapname));

	m_shader_handle = (std::string) Handle + std::string("shader");
	ctx.SetAttribute(m_shader_handle, shader_args);
}

#include "LightGroupTranslator.h"
#include "customgui_inexclude.h"
#include "nsi.hpp"
#include "olightgroup.h"

using namespace std;

void LightGroupTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	NSI::Context& ctx(parser->GetContext());
	std::string handle = std::string(Handle);
	ctx.Create(handle, "set");
}

void LightGroupTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	BaseList2D* obj = (BaseList2D*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	std::string handle = std::string(Handle);
	NSI::Context& ctx(parser->GetContext());
	InExcludeData* light_list = NULL;
	light_list = (InExcludeData*) data->GetCustomDataType(
					 LIGHTGROUP_LIGHTS, CUSTOMDATATYPE_INEXCLUDE_LIST);

	if (light_list) {
		Int32 nlights = light_list->GetObjectCount();

		for (int i = 0; i < nlights; i++) {
			BaseList2D* light = light_list->ObjectFromIndex(doc, i);
			std::string lighthandle =
				std::string("X_") + parser->GetHandleName(light); // The handle of the parent transform of the light
			ctx.Connect(lighthandle, "", handle, "objects");
		}
	}
}

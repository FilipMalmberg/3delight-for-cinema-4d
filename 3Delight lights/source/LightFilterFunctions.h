#pragma once

#include <vector>
#include "IDs.h"
#include "c4d.h"
#include "dlgobofilter.h"

// With this function we get the corresponding filter shader selected from the
// view of light filters.

inline std::vector<BaseShader*>
getSelectedFilters(BaseObject* object,
				   std::vector<Int32> objectID,
				   maxon::Int all_light_filters)
{
	std::vector<BaseShader*> light_list;
	BaseShader* shader = object->GetFirstShader();

	if (!shader) {
		return light_list;
	}

	bool process = true;
	Int32 Position = (Int32) all_light_filters - 1;
	int lights_size = (int) objectID.size();

	while (shader) {
		if (process && (shader->GetType() == DL_GOBOFILTER || shader->GetType() == DL_DECAYFILTER)) {
			BaseContainer* shader_data = shader->GetDataInstance();
			bool selected = false;

			for (int i = 0; i < lights_size; i++) {
				/*
				                        Finding the objects that are selected in
				   the LightFilter UI objectID contains the ID of the selected
				   objects in the UI(row number) and now we are getting the
				   corresponing object for that ID
				                */
				if (objectID[i] == Position) {
					light_list.push_back(shader);
					shader_data->SetBool(IS_SELECTED, TRUE);
					selected = true;
				}
			}

			if (!selected) {
				shader_data->SetBool(IS_SELECTED, FALSE);
			}

			Position--;
		}

		if (shader->GetDown() && process) {
			shader = shader->GetDown();
			process = true;

		} else if (shader->GetNext()) {
			shader = shader->GetNext();
			process = true;

		} else if (shader->GetUp()) {
			shader = shader->GetUp();
			process = false;

		} else {
			shader = NULL;
		}
	}

	return light_list;
}

#include "main.h"

Bool PluginStart(void)
{
	if (!RegisterAreaLight()) {
		return false;
	}

	if (!RegisterDirectionalLight()) {
		return false;
	}

	if (!RegisterPointLight()) {
		return false;
	}

	if (!RegisterIncandescenceLight()) {
		return false;
	}

	if (!RegisterSpotLight()) {
		return false;
	}

	if (!RegisterEnvironmentLight()) {
		return FALSE;
	}

	if (!RegisterCustomLightFilters()) {
		return false;
	}

	if (!RegisterGoboFilter()) {
		return false;
	}

	if (!RegisterDecayFilter()) {
		return false;
	}

	if (!RegisterLightGroup()) {
		return false;
	}

	if (!Register_LightShape()) {
		return false;
	}

	return true;
}

void PluginEnd(void) {}

Bool PluginMessage(Int32 id, void* data)
{
	switch (id) {
	case C4DPL_INIT_SYS:
		if (!g_resource.Init()) {
			return FALSE; // don't start plugin without resource
		}

		return TRUE;
		break;

	case DL_LOAD_PLUGINS:
		DL_PluginManager* pm = (DL_PluginManager*) data;
		/*
			                We are going to have our own 3Delight lights and thus we
			   will not support Cinema4D lights.
			        */
		// pm->RegisterTranslator(Olight,
		// AllocateTranslator<c4dLightTranslator>,true);
		pm->RegisterTranslator(
			ID_AREALIGHT, AllocateTranslator<AreaLightTranslator>, true);
		pm->RegisterTranslator(
			DL_GOBOFILTER, AllocateTranslator<GoboFilterTranslator>, true);
		pm->RegisterTranslator(
			DL_DECAYFILTER, AllocateTranslator<DlDecayFilterTranslator>, true);
		pm->RegisterTranslator(
			ID_POINTLIGHT, AllocateTranslator<PointLightTranslator>, true);
		pm->RegisterTranslator(ID_DIRECTIONAL_LIGHT,
							   AllocateTranslator<DirectionalLightTranslator>,
							   true);
		pm->RegisterTranslator(ID_INCANDESCENCELIGHT,
							   AllocateTranslator<IncandescenceLightTranslator>,
							   true);
		pm->RegisterTranslator(
			ID_SPOTLIGHT, AllocateTranslator<SpotLightTranslator>, true);
		pm->RegisterTranslator(ID_ENVIRONMENTLIGHT,
			AllocateTranslator<EnvironmentLightTranslator>,
			true);
		pm->RegisterTranslator(
			ID_LIGHTGROUP, AllocateTranslator<LightGroupTranslator>, false);
		break;
	}

	return FALSE;
}
